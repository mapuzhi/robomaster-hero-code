#include "chassis_task.h"
void Chassis_Speed_Calc(float vx,float vy,float vw);
void Chassis_Follow_Control();
void Chassis_Stop_Control();
void Chassis_Rotate_Control();
extern void Encoder_Data_Process(CAN_Data_TypeDef *encoder_data,short init_angle);
#define IS_SUPER_OFF (DBUS.RC.ch4 > -600 && !(DBUS.PC.Keyboard & KEY_SHIFT))
#define SUPPER_OFF 1
#define SUPPER_ON  0
TaskHandle_t CHASSIS_Task_Handler;
void chassis_task(void *p_arg);

Chassis_Speed_Typedef Chassis_Speed;
Chassis_Mode_Enum Chassis_Mode;
Chassis_Control_Speed_Typedef rc;
Chassis_Control_Speed_Typedef keyboard;	
int Super_Allow_Flag = 0;
int time_delay = 0;
float Power_Max=60.0f;
int Power_Chassis_Max=50;
int xishu;

/*
*@title：底盘任务创建
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/
void chassis_task_create()
{
		xTaskCreate((TaskFunction_t )chassis_task,          //任务函数
							(const char*    )"chassis_task",          //任务名称
							(uint16_t       )CHASSIS_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                //传递给任务函数的参数
							(UBaseType_t    )CHASSIS_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&CHASSIS_Task_Handler);  //任务句柄  
}

float power_limit = 1.0;

/*
1.随动模式
2.自瞄模式
3.陀螺模式
4.无力模式
*/
void Chassis_Power_Limit();

/*
*@title：底盘任务
*@description：周期3ms
*@param 1：	
*@param 2：	
*@return:：	
*/
void chassis_task(void *p_arg)
{
	
	const TickType_t TimeIncrement=pdMS_TO_TICKS(4);
	TickType_t	PreviousWakeTime;
	PreviousWakeTime=xTaskGetTickCount();	
	
	while(1)
	{
		switch(Chassis_Mode)
		{
			//跟随模式
			case CHASSIS_MODE_FOLLOW:
				Chassis_Follow_Control();
				break;
			//陀螺模式
			case CHASSIS_MODE_ROTATE:
				Chassis_Rotate_Control();
				break;
			//无力模式
			case CHASSIS_MODE_STOP:
				Chassis_Stop_Control();
				break;
			default:
				break;
		}
		
		//底盘速度解算
		Chassis_Speed_Calc(Chassis_Speed.vx,Chassis_Speed.vy,Chassis_Speed.vw);
		
				//底盘电容控制
		Chassis_SuperCap_Control();
		
		//电机目标电流赋值
		for(int i = 0;i<4;i++)
		{
			CAN_Chassis[i].Target_Current = Pid_Calc(&PID_Chassis[i], CAN_Chassis[i].Current_Speed, Chassis_Speed.wheel_speed[i]);		
		}
		
		//CAN数据更新发送
		xEventGroupSetBits(EventGroupHandler,CHASSIS_SIGNAL);	//底盘事件组置位		
		vTaskDelayUntil(&PreviousWakeTime,TimeIncrement);	
	}

}
/**
 *@Function:	底盘电容策略控制	
 *@Description:	
 *@Param:       形参
 *@Return:	  	返回值
 */
void Chassis_SuperCap_Control(void)
{
	SuperCap_Info.id = 0x300;
	SuperCap_Signal = 1000;
			//安装了电容
		if(SuperCap_Signal > 0)
		{
			//雾列控制电容
			if(SuperCap_Info.id == 0x211)
			{
				Chassis_SuperCap_Wulie_Control();			
			}
			//自制超级电容
			else if(SuperCap_Info.id == 0x300)
			{
				Chassis_SuperCap_HomeMade_Control();
			}
		}
		//未接入电容，使用裁判系统数据
		else
		{
			//开启超速模式，未使用电容情况下慎用
			//关闭电容模式下
			if(IS_SUPER_OFF)
			{
				Chassis_Power_Limit();
			}			
		}
}


void Chassis_Power_Limit()
{
	if(power_heat_data.chassis_power_buffer>Power_Max)
	{
		Power_Max=250.0f;
	}
		//底盘功率控制
		if(power_heat_data.chassis_power_buffer < Power_Max)
		{
			for(int i =0;i<4;i++)
			{
				Chassis_Speed.wheel_speed[i] *= power_heat_data.chassis_power_buffer/Power_Max;
			}
		}	
}

/**
 *@Function:	底盘雾列电容控制策略
 *@Description:	
 *@Param:       形参
 *@Return:	  	返回值
 */
//SuperCap_Info.CapVot
void Chassis_SuperCap_Wulie_Control()
{
	static float   power_limit_value = 1.0; //功率限制系数
	static uint8_t power_flag  = 0; 	//超速标志
	
	
	//电容关闭
	if(IS_SUPER_OFF) 
	{
		power_flag = 0;
		Super_Allow_Flag = 1;
	}
	//开启超速模式
	else if(Super_Allow_Flag)
	{
		power_flag = 1;
	}
	else
	{
		power_flag = 0;			
	}
	
	//若电容电压低于16v强制限速
	if(SuperCap_Info.Low_Filter_Vot < 18)
	{
		Super_Allow_Flag = 0;
		power_flag = 0;
	}
	
	//限速状态
	if(power_flag == 0)
	{
		//底盘功率控制系数
		power_limit_value = (SuperCap_Info.Low_Filter_Vot - 11.0) / (SuperCap_Info.InputVot - 10.0);
		//系数限幅
		if(power_limit_value >= 1.0)
		{
			power_limit_value = 1.0;
		}
		else if(power_limit_value < 0)
		{
			power_limit_value = 0;
		}
		//速度限制
		for(int i =0;i<4;i++)
		{
			Chassis_Speed.wheel_speed[i] *= power_limit_value;
		}
	}
	//超速状态，不限制速度上限（也可以对速度进行提升）
	else if(power_flag == 1)
	{
		//修改power_limit对速度上限进行提升，参考Chassis_Speed_Calc(float vx,float vy,float vw)
		power_limit_value = 1;		
			for(int i =0;i<4;i++)
		{
			Chassis_Speed.wheel_speed[i] *= power_limit_value;
		}
	}
}
/**
 *@Function:	底盘自制电容控制策略
 *@Description:	
 *@Param:       形参
 *@Return:	  	返回值
 */
void Chassis_SuperCap_HomeMade_Control()
{
	if(IS_SUPER_OFF==SUPPER_ON)//遥控器控制打开电容
	{
		Super_Allow_Flag = 1;
		if(SuperCap_Info.CapVot < 16.0)
		{			
			power_relay = 0;
			Super_Allow_Flag = 0;	//不允许开
		}
		
	  if(Super_Allow_Flag == 1)
		{
			power_relay = 1;
			if(SuperCap_Info.InputCurrent!=1 || SuperCap_Info.CapVot <= 19.0 )
			{
				Chassis_Power_Limit();
			}
		}
		else
		{
				Chassis_Power_Limit();
				power_relay = 0;
				time_delay = 0 ;
		}
	}
	else
	{
			Chassis_Power_Limit();
			power_relay = 0;
			time_delay = 0 ;
	}
}

//上一中值角度
extern short Angle_Last;
//初始角度
extern short Init_Angle;

//返回Critical值
int Encoder_Data_Cal(CAN_Data_TypeDef *encoder_data,short init_angle)
{
	if(init_angle + 4096 >8191)
	{
		return init_angle + 4096 - 8192;
	}
	else
	{
		return init_angle + 4096;
	}
}
//返回当前真实角度值
int Angle_Correct(int angle_to_resize)
{	
		if(angle_to_resize < CAN_Gimbal[0].Critical_MechAngle)
		{
			return angle_to_resize + 8192;
		}
		else
		{
			return angle_to_resize;
		}	
}
extern short Origin_Init_Yaw_Angle;


//速度方向标志位
int Init_Dir = 1;
/**
 *@Function:		Angle_Reverse_Handle()
 *@Description:	角度反转处理
 *@Param:       形参
 *@Return:	  	返回值
 */
void Angle_Reverse_Handle()
{
		/*
		实现跟随状态时最小旋转时间，即头尾合用
	*/

	//角度跳变
	if(abs(CAN_Gimbal[0].Current_MechAngle - Angle_Last) > 4000)
	{
		
		if(CAN_Gimbal[0].Critical_MechAngle == Encoder_Data_Cal(&CAN_Gimbal[0],Origin_Init_Yaw_Angle) )
		{
			//将反面改为正前方
			Encoder_Data_Process(&CAN_Gimbal[0],Origin_Init_Yaw_Angle+4096);
			if(CAN_Gimbal[0].Origin_MechAngle < CAN_Gimbal[0].Critical_MechAngle)
			{
				CAN_Gimbal[0].Current_MechAngle = CAN_Gimbal[0].Origin_MechAngle + 8192;
			}
			else
			{
				CAN_Gimbal[0].Current_MechAngle = CAN_Gimbal[0].Origin_MechAngle;
			}		
			
			Init_Angle = Angle_Correct(Origin_Init_Yaw_Angle+4096);
			
		}
		else
		{
			//将反面改为正前方
			Encoder_Data_Process(&CAN_Gimbal[0],Origin_Init_Yaw_Angle);
			if(CAN_Gimbal[0].Origin_MechAngle < CAN_Gimbal[0].Critical_MechAngle)
			{
				CAN_Gimbal[0].Current_MechAngle = CAN_Gimbal[0].Origin_MechAngle + 8192;
			}
			else
			{
				CAN_Gimbal[0].Current_MechAngle = CAN_Gimbal[0].Origin_MechAngle;
			}
			
			Init_Angle = Angle_Correct(Origin_Init_Yaw_Angle);
		}
		
		Init_Dir =-Init_Dir;
	}
	
	Angle_Last = CAN_Gimbal[0].Current_MechAngle;

}

#define ANGLE_TO_RAD 0.01745329251994329576923690768489f
/*
*@Description：底盘跟随控制
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Chassis_Follow_Control()
{
	static double Follow_Angle = 0;
	Follow_Angle = (CAN_Gimbal[0].Current_MechAngle - Origin_Init_Yaw_Angle)/8192.0*360.0f;
	short Follow_Angle_Different_Value=0;
	Follow_Angle_Different_Value=(CAN_Gimbal[0].Current_MechAngle - Init_Angle);

	//战车速度分量赋值
//	Chassis_Speed.vx = rc.vx + keyboard.vx;
//	Chassis_Speed.vy = rc.vy + keyboard.vy;
	Chassis_Speed.vx = (rc.vy + keyboard.vy) * sin(Follow_Angle * ANGLE_TO_RAD)+ (rc.vx + keyboard.vx) * cos(Follow_Angle * ANGLE_TO_RAD);
	Chassis_Speed.vy = (rc.vy + keyboard.vy) * cos(Follow_Angle * ANGLE_TO_RAD) -(rc.vx + keyboard.vx) * sin(Follow_Angle * ANGLE_TO_RAD);
	Chassis_Speed.vw = -Pid_Calc(&PID_Chassis_Omega,Follow_Angle_Different_Value/8192.0*360.0f, 0); //云台Yaw轴相对角PID 输出旋转速度分量

	//首尾互换处理
//	{
//	Angle_Reverse_Handle();
//	Chassis_Speed.vx *= Init_Dir;
//	Chassis_Speed.vy *= Init_Dir;
//	}
	
//	if(DBUS.PC.Keyboard & KEY_Q)
//	{
//		Chassis_Speed.vw = -Pid_Calc(&PID_Chassis_Omega,Follow_Angle_Different_Value/8192.0*360.0f, 20); //云台Yaw轴相对角PID 输出旋转速度分量
//	}
//	else if(DBUS.PC.Keyboard & KEY_E)
//	{
//		Chassis_Speed.vw = -Pid_Calc(&PID_Chassis_Omega,Follow_Angle_Different_Value/8192.0*360.0f, -20); //云台Yaw轴相对角PID 输出旋转速度分量
//	}
//	
	//旋转速度死区，减少静止底盘轮系抖动
	if(fabs(Chassis_Speed.vw) < 200)
	Chassis_Speed.vw = 0;
}

int Chassis_Rotate_Base_Speed = 7000;
extern short Origin_Init_Yaw_Angle;

/*
*@Description：底盘陀螺控制
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Chassis_Rotate_Control()
{
	static double Rotate_Angle = 0;
	
	Rotate_Angle = (CAN_Gimbal[0].Current_MechAngle - Origin_Init_Yaw_Angle)/8192.0*360.0f;
	if(Rotate_Angle <= 0)
	{
		Rotate_Angle = Rotate_Angle + 360.0f;
	}
	
	Chassis_Speed.vx = (rc.vy + keyboard.vy) * sin(Rotate_Angle * ANGLE_TO_RAD)+ (rc.vx + keyboard.vx) * cos(Rotate_Angle * ANGLE_TO_RAD);
	Chassis_Speed.vy = (rc.vy + keyboard.vy) * cos(Rotate_Angle * ANGLE_TO_RAD) -(rc.vx + keyboard.vx) * sin(Rotate_Angle * ANGLE_TO_RAD);
	Chassis_Speed.vw = Chassis_Rotate_Base_Speed;
	
//	Angle_Reverse_Handle();

}
/*
*@Description：底盘无力控制
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Chassis_Stop_Control()
{
	Chassis_Speed.vx = 0;
	Chassis_Speed.vy = 0;
	Chassis_Speed.vw = 0;
}

/*
*@Description：底盘速度解算（麦轮）
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Chassis_Speed_Calc(float vx,float vy,float vw)
{
	float Speed_Max = 0;
	
	//轮速解算
	Chassis_Speed.wheel_speed[0] = +vx - vy + vw;
	Chassis_Speed.wheel_speed[1] = -vx - vy + vw;
	Chassis_Speed.wheel_speed[2] = -vx + vy + vw;
	Chassis_Speed.wheel_speed[3] = +vx + vy + vw;

	
	//速度限幅调整
	//最大值寻找
	for(int i = 0;i<4;i++)
	{
		if(abs(Chassis_Speed.wheel_speed[i]) > Speed_Max)
		{
			Speed_Max = abs(Chassis_Speed.wheel_speed[i]);
		}
	}

	//最大轮速限制
	if(Speed_Max > MAX_WHEEL_SPEED)
	{
		for(int i = 0;i<4;i++)
		{
			Chassis_Speed.wheel_speed[i] *= MAX_WHEEL_SPEED/Speed_Max;
		}
	}
	
}