#include "gimbal.h"
Init_Angle_Data Angle_Init;

/*
*@Description：电机编码器数据处理
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Encoder_Data_Process(CAN_Data_TypeDef *encoder_data,short init_angle)
{
	//init_angle代表云台初始角度的电机原始值（范围0-8191）
	//判断初始角度是否超过编码器中间值
	if(init_angle + 4096 >8191)
	{ 
		encoder_data->Critical_MechAngle = init_angle - 4096;
	}
	else
	{
		encoder_data->Critical_MechAngle = init_angle + 4096;
	}
}


//Flash信息储存地址
#define CONFIG_PARAM_SIZE	(1020*1024)
#define CONFIG_PARAM_ADDR 	(FLASH_BASE + CONFIG_PARAM_SIZE)	
//云台角度标定（标定的值为电机编码器原始值）
void Gimbal_Angle_Calibrate()
{
	if(DBUS.RC.Switch_Left == RC_SW_DOWN && DBUS.RC.Switch_Right == RC_SW_DOWN)
	{
	//内八状态下进入标定程序
	if(DBUS.RC.ch0 < -600 && DBUS.RC.ch1 <-600 && DBUS.RC.ch2 > 600 && DBUS.RC.ch3 < -600)
	{
		//蜂鸣器响两声
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(800,100);
		delay_ms(500);		
		Beep_Ctrl(800,0);
		//蜂鸣器响两声
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(800,100);
		delay_ms(500);		
		Beep_Ctrl(800,0);
		
		//等待左拨杆置中
		while(DBUS.RC.Switch_Left != RC_SW_MID)
		{
			Angle_Init.Yaw_Angle_Init = CAN_Gimbal[0].Origin_MechAngle;
		}
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(1000,0);
		//等待左拨杆置上
		while(DBUS.RC.Switch_Left != RC_SW_UP)
		{
			Angle_Init.Pitch_Angle_Init = CAN_Gimbal[1].Origin_MechAngle;
		}
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(1000,0);
		//等待右拨杆置中
		while(DBUS.RC.Switch_Right != RC_SW_MID)
		{
			Angle_Init.Pitch_Angle_Max = CAN_Gimbal[1].Origin_MechAngle;			
		}
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(1000,0);
		//等待右拨杆置上
		while(DBUS.RC.Switch_Right != RC_SW_UP)
		{
			Angle_Init.Pitch_Angle_Min = CAN_Gimbal[1].Origin_MechAngle;			
		}		
		Beep_Ctrl(1000,100);
		delay_ms(500);
		Beep_Ctrl(800,100);
		delay_ms(500);		
		Beep_Ctrl(800,0);
		
		STMFLASH_Write(CONFIG_PARAM_ADDR,(u32 *)&Angle_Init, sizeof(Angle_Init));	/*写入stm32 flash*/

	}		
	}

}


//机器人Flash角度值读取
void Gimbal_Angle_Read()
{
	//先清空角度值
	memset(&Angle_Init,0,sizeof(Angle_Init));
	//读取Flash值
	STMFLASH_Read(CONFIG_PARAM_ADDR, (u32 *)&Angle_Init, sizeof(Angle_Init));
	//判断是否为有效值
	if(Angle_Init.Pitch_Angle_Init == 0 && Angle_Init.Pitch_Angle_Max == 0 && Angle_Init.Pitch_Angle_Min == 0 && Angle_Init.Yaw_Angle_Init == 0 )
	{
			Beep_Ctrl(1000,100);
			delay_ms(500);
			Beep_Ctrl(800,100);
			delay_ms(500);	
			Beep_Ctrl(1000,100);
			delay_ms(500);
			Beep_Ctrl(800,0);
			//卡死等待
			while(1){}
	}
	
}
//返回当前真实角度值 
static int Angle_Correct_Get(int angle_to_resize , int angle_critical)
{	
		if(angle_to_resize < angle_critical)
		{
			return angle_to_resize + 8192;
		}
		else
		{
			return angle_to_resize;
		}	
}

short Angle_Last ;
//初始角度
short Init_Angle ;

short Origin_Init_Yaw_Angle;
short Origin_Init_Pitch_Angle;


extern float Yaw_Aim_Angle;
extern float Pitch_Aim_Angle;
extern float Pitch_Angle_Max;
extern float Pitch_Angle_Min;
/*
*@Description：云台初始化
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
 void Gimbal_Init()
{
		//云台角度标定
		Gimbal_Angle_Calibrate();
	
	  //云台PID初始化
		Pid_Reset(&PID_Gimbal_Angle[0]);
		Pid_Reset(&PID_Gimbal_Angle[1]);
		Pid_Reset(&PID_Gimbal_Speed[0]);
		Pid_Reset(&PID_Gimbal_Speed[1]);
		Pid_Reset(&PID_Gimbal_Angle_Stop[0]);//Yaw
		Pid_Reset(&PID_Gimbal_Angle_Stop[1]);//Pitch
		Pid_Reset(&PID_Gimbal_Speed_Stop[0]);//Yaw
		Pid_Reset(&PID_Gimbal_Speed_Stop[1]);//Pitch
		Pid_Reset(&PID_Yaw_Angle_Left);
		Pid_Reset(&PID_Yaw_Angle_Right);
		Pid_Reset(&PID_Yaw_Speed_Left);
		Pid_Reset(&PID_Yaw_Speed_Right);
		Pid_Reset(&PID_Pitch_Angle);
		Pid_Reset(&PID_Pitch_Speed);
	
		Pid_Set(&PID_Gimbal_Angle[0],20,0,0,10000,2000,30000,10000,1,30000,0,2);	//Yaw15
		Pid_Set(&PID_Gimbal_Speed[0],300,0,0,30000,2000,200,30000,1,30000,0,2);	  //Yaw180  给i
		Pid_Set(&PID_Gimbal_Angle[1],2.2,0,0,10000,0,30000,10000,1,30000,0,2);		  //Pitch
		Pid_Set(&PID_Gimbal_Speed[1],200,0,0,30000,5000,30000,30000,1,30000,0,2);  //Pitch
		Pid_Set(&PID_Gimbal_Angle_Stop[0],50,0,0,10000,200,30000,10000,1,30000,0,2);//Yaw轴电机急停
		Pid_Set(&PID_Gimbal_Speed_Stop[0],200,0,0,30000,200,200,30000,1,30000,0,2);	  //Yaw180 
		Pid_Set(&PID_Gimbal_Angle_Stop[1],18,0,5,10000,200,30000,10000,1,30000,0,2);//Pitch轴电机急停
		Pid_Set(&PID_Gimbal_Speed_Stop[1],25,0,5,30000,200,200,30000,1,30000,0,2);	  //Pitch180 
		Pid_Set(&PID_Yaw_Angle_Left,20,0.008,0,10000,50,30000,10000,1,30000,0,2); //Yaw轴往左转
		Pid_Set(&PID_Yaw_Speed_Left,550,0,0,30000,50,200,30000,1,30000,0,2);  //Yaw轴往左转
		Pid_Set(&PID_Yaw_Angle_Right,20,0.008,0,10000,50,30000,10000,1,30000,0,2); //Yaw轴往右转
		Pid_Set(&PID_Yaw_Speed_Right,550,0,0,30000,50,200,30000,1,30000,0,2);  //Yaw轴往右转
		Pid_Set(&PID_Pitch_Angle,2.2,0,0,10000,2000,30000,10000,1,30000,0,2); //Pitch轴自瞄
		Pid_Set(&PID_Pitch_Speed,200,0,0,30000,2000,200,30000,1,30000,0,2);  //Pitch轴自瞄

		
		//读取机器人Flash参数值
		Gimbal_Angle_Read();
	
		//云台初始值解码
		Encoder_Data_Process(&CAN_Gimbal[0],Angle_Init.Yaw_Angle_Init);
		Encoder_Data_Process(&CAN_Gimbal[1],Angle_Init.Pitch_Angle_Init);
		
		//云台真实角度处理
		Pitch_Aim_Angle = Angle_Correct_Get(Angle_Init.Pitch_Angle_Init,CAN_Gimbal[1].Critical_MechAngle);
		Pitch_Angle_Max = Angle_Correct_Get(Angle_Init.Pitch_Angle_Max ,CAN_Gimbal[1].Critical_MechAngle);
		Pitch_Angle_Min = Angle_Correct_Get(Angle_Init.Pitch_Angle_Min ,CAN_Gimbal[1].Critical_MechAngle);
		
		if(Pitch_Angle_Max < Pitch_Angle_Min)
		{
			Pitch_Angle_Min = Pitch_Angle_Max;
			Pitch_Angle_Max = Angle_Correct_Get(Angle_Init.Pitch_Angle_Min,CAN_Gimbal[1].Critical_MechAngle);
		}
		Angle_Last = Init_Angle = Angle_Correct_Get(Angle_Init.Yaw_Angle_Init,CAN_Gimbal[0].Critical_MechAngle);
		//底盘YAW初始角
		Origin_Init_Yaw_Angle = Init_Angle;
		//Pitch初始角
		Origin_Init_Pitch_Angle = Pitch_Aim_Angle;

	  //等待一会儿CAN数据接收后同步角度
		delay_ms(10);
}



