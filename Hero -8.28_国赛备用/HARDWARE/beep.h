#ifndef __BEEP_H
#define __BEEP_H
/*********************************************
Copyright(C) 水木皆Ming
All rights reserved
File name:	beep.h
Author:		shuimujieming
Version:		1.0
Description:	蜂鸣器头文件
Others:		
Log:	
*********************************************/
#include "main.h"

//蜂鸣器接口对应时钟
#define BEEP_TUNE        TIM4->ARR
#define BEEP_SOUND        TIM4->CCR3

void Beep_Ctrl(uint16_t tune, uint16_t sound);
void Beep_Init();


#endif
