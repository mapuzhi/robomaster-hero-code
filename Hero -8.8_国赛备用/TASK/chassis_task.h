#ifndef __CHASSIS_TASK_H
#define __CHASSIS_TASK_H

#define MAX_WHEEL_SPEED 8500


typedef struct
{
	float vx;
	float vy;
	float vw;
	short wheel_speed[4];
}Chassis_Speed_Typedef;
typedef struct
{
	float vx;
	float vy;
	float vw;
} Chassis_Control_Speed_Typedef;
typedef enum
{
	CHASSIS_MODE_STOP = 0,
	CHASSIS_MODE_ROTATE,
	CHASSIS_MODE_FOLLOW,
	CHASSIS_MODE_AUTOAIM,
	CHASSIS_MODE_SUPERCAP,
}Chassis_Mode_Enum;
void chassis_task_create();
void Chassis_SuperCap_Control(void);
void Chassis_SuperCap_HomeMade_Control();
void Chassis_SuperCap_Wulie_Control();
#include "main.h"



#endif
