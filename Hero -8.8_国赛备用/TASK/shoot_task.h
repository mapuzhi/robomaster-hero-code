#ifndef __SHOOT_TASK_H
#define __SHOOT_TASK_H


typedef enum
{
	SHOOT_MODE_STOP = 0,
	SHOOT_MODE_RUN,
}Shoot_Mode_Enum;

extern Shoot_Mode_Enum Shoot_Mode;

//为1则是单发，为2则是连发
typedef enum
{
	SHOOT_CMD_STOP = 0,
	SHOOT_CMD_ONCE,
	SHOOT_CMD_LONG,
	SHOOT_CMD_TRIG,
}Shoot_Cmd_Enum;

extern Shoot_Cmd_Enum Shoot_Cmd;
extern int friction_wheel;
#include "main.h"

void shoot_task_create();

#endif
