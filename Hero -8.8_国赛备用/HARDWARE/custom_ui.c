	/**
  ****************************(C) COPYRIGHT 2021 SHUIMUJIEMING****************************
  * @file       custom_ui.c/h
  * @brief      自定义UI界面显示相关函数
  * @note       
  * @history
  *  Version    Date            Author          Modification
  *  V1.0.0     7-10-2021     shuimujieming              1. 完成
  *
  ****************************(C) COPYRIGHT 2021 SHUIMUJIEMING****************************
  */
#include "custom_ui.h"


//定义发送者和接收者ID，默认为蓝方
int Sendor_ID = 101;
int Receiver_ID = 0x0165;


//定义整数型数据
ext_client_custom_graphic_int_t interger_one;

//定义浮点型数据
ext_client_custom_graphic_float_t float_one;

//定义字符数据
ext_client_custom_char_int_t character_one;
ext_client_custom_char_int_t character_two;
ext_client_custom_char_int_t character_three;
ext_client_custom_char_int_t character_four;
ext_client_custom_char_int_t character_five;
ext_client_custom_char_int_t character_six;
ext_client_custom_char_int_t character_seven;
ext_client_custom_char_int_t character_eight;
ext_client_custom_char_int_t character_nine;
ext_client_custom_char_int_t character_ten;
ext_client_custom_char_int_t character_eleven;
ext_client_custom_char_int_t character_twelve;
ext_client_custom_char_int_t character_thirteen;
ext_client_custom_char_int_t character_fourteen;
//定义直线数据
ext_student_custom_graphic_t line_one;
ext_student_custom_graphic_t line_two;
ext_student_custom_graphic_t line_three;
ext_student_custom_graphic_t line_four;
ext_student_custom_graphic_t line_five;
ext_student_custom_graphic_t line_six;
ext_student_custom_graphic_t line_seven;
ext_student_custom_graphic_t line_eight;
ext_student_custom_graphic_t line_nine;

ext_student_custom_graphic_t line_ten;
ext_student_custom_graphic_t line_eleven;
ext_student_custom_graphic_t line_twelve;
ext_student_custom_graphic_t line_thirteen;
ext_student_custom_graphic_t line_fourteen;
ext_student_custom_graphic_t line_fifteen;
ext_student_custom_graphic_t line_sixteen;
ext_student_custom_graphic_t line_seventeen;
ext_student_custom_graphic_t line_eighteen;
ext_student_custom_graphic_t line_nineteen;
ext_student_custom_graphic_t line_twenty;
//定义圆数据
ext_student_custom_graphic_t circle_one;
ext_student_custom_graphic_t circle_two;
ext_student_custom_graphic_t circle_three;
ext_student_custom_graphic_t circle_four;
ext_student_custom_graphic_t circle_five;
ext_student_custom_graphic_t circle_six;
//定义字符串变量
char char_one[30];	//超级电容

char char_two[30];	//云台模式

char char_three[30];	//射击允许限位

char char_four[30];	//Pitch

char char_five[30];	//摩擦轮开关

char char_six[30];	//自瞄模式

char char_seven[30];	

char char_eight[30];	

char char_nine[30];	

char char_ten[30];	

char char_eleven[30];	

char char_twelve[30];	

char char_thirteen[30];	


/**
  * @brief 	绘制直线
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 开始坐标
						End_x，End_y 结束坐标
  * @retval 
  * @notes  
  */
void Draw_Line(ext_student_custom_graphic_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,u32 Graphic_Width,u32 Start_x,u32 Start_y,u32 End_x,u32 End_y)
{
	int i;
	Graphic->data_cmd_id = 0x0101;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->grapic_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->grapic_data_struct.graphic_tpye = Graphic_Type_LINE;
	Graphic->grapic_data_struct.operate_tpye = Operate_Type;	
	Graphic->grapic_data_struct.layer = Graphic_Layer;
	Graphic->grapic_data_struct.color = Graphic_Color;

	Graphic->grapic_data_struct.width = Graphic_Width;
	Graphic->grapic_data_struct.start_x = Start_x;
	Graphic->grapic_data_struct.start_y = Start_y;
	
	Graphic->grapic_data_struct.end_x = End_x;
	Graphic->grapic_data_struct.end_y = End_y;

}
	
/**
  * @brief 	绘制圆
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 圆心坐标
						Radius 半径
  * @retval 
  * @notes  
  */
void Draw_Circle(ext_student_custom_graphic_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,u32 Graphic_Width,u32 Start_x,u32 Start_y,u32 Radius)
{
	int i;
	Graphic->data_cmd_id = 0x0101;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->grapic_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->grapic_data_struct.graphic_tpye = Graphic_Type_CIRCLE;
	Graphic->grapic_data_struct.operate_tpye = Operate_Type;	
	Graphic->grapic_data_struct.layer = Graphic_Layer;
	Graphic->grapic_data_struct.color = Graphic_Color;

	Graphic->grapic_data_struct.width = Graphic_Width;
	Graphic->grapic_data_struct.start_x = Start_x;
	Graphic->grapic_data_struct.start_y = Start_y;
	
	Graphic->grapic_data_struct.radius = Radius;


}

	/**
  * @brief 	绘制直线
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 开始坐标
						End_x，End_y 对角顶点坐标
  * @retval 
  * @notes  
  */
void Draw_Rect(ext_student_custom_graphic_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,u32 Graphic_Width,u32 Start_x,u32 Start_y,u32 End_x,u32 End_y)
{
	int i;
	Graphic->data_cmd_id = 0x0101;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->grapic_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->grapic_data_struct.graphic_tpye = Graphic_Type_RECT;
	Graphic->grapic_data_struct.operate_tpye = Operate_Type;	
	Graphic->grapic_data_struct.layer = Graphic_Layer;
	Graphic->grapic_data_struct.color = Graphic_Color;

	Graphic->grapic_data_struct.width = Graphic_Width;
	Graphic->grapic_data_struct.start_x = Start_x;
	Graphic->grapic_data_struct.start_y = Start_y;
	
	Graphic->grapic_data_struct.end_x = End_x;
	Graphic->grapic_data_struct.end_y = End_y;

}
	/**
  * @brief 	绘制字符
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 开始坐标
						End_x，End_y 对角顶点坐标
						Font_Size 字体大小
						Graphic_Char 字符串
  * @retval 
  * @notes  
  */
void Draw_Char(ext_client_custom_char_int_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,char* Graphic_Char,u32 Font_Size,u32 Graphic_Width,u32 Start_x,u32 Start_y)
{
	int i;
	Graphic->data_cmd_id = 0x0110;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->character_custom.grapic_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->character_custom.grapic_data_struct.graphic_tpye = Graphic_Type_CHAR;
	Graphic->character_custom.grapic_data_struct.operate_tpye = Operate_Type;	
	Graphic->character_custom.grapic_data_struct.layer = Graphic_Layer;
	Graphic->character_custom.grapic_data_struct.color = Graphic_Color;

	Graphic->character_custom.grapic_data_struct.width = Graphic_Width;
	Graphic->character_custom.grapic_data_struct.start_x = Start_x;
	Graphic->character_custom.grapic_data_struct.start_y = Start_y;
	
	Graphic->character_custom.grapic_data_struct.start_angle = Font_Size;
	Graphic->character_custom.grapic_data_struct.end_angle = 30;

	for(int j=0;j<30;j++)
	{
	 Graphic->character_custom.data[j] = *Graphic_Char;		
	 Graphic_Char++;
	}

}


	/**
  * @brief 	绘制整数
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 开始坐标
						int_number 32位整型数
  * @retval 
  * @notes  
  */
void Draw_Int(ext_client_custom_graphic_int_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,int int_number,u32 Font_Size,u32 Graphic_Width,u32 Start_x,u32 Start_y)
{
	int i;
	Graphic->data_cmd_id = 0x0101;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->graphic_int_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->graphic_int_data_struct.graphic_tpye = Graphic_Type_INT;
	Graphic->graphic_int_data_struct.operate_tpye = Operate_Type;	
	Graphic->graphic_int_data_struct.layer = Graphic_Layer;
	Graphic->graphic_int_data_struct.color = Graphic_Color;

	Graphic->graphic_int_data_struct.width = Graphic_Width;
	Graphic->graphic_int_data_struct.start_x = Start_x;
	Graphic->graphic_int_data_struct.start_y = Start_y;
	
	Graphic->graphic_int_data_struct.start_angle = Font_Size;
	Graphic->graphic_int_data_struct.int_number = int_number;
}


	/**
  * @brief 	绘制浮点数
  * @param  Graphic 图形数据
						Graphic_Name 图形名
						Operate_Type	操作类型
						Graphic_Layer 图层
						Graphic_Color 图形颜色
						Graphic_Width	线宽
						Start_x，Start_y 开始坐标
						float_number	32位浮点数
						useful_digits 小数位有效个数
  * @retval 
  * @notes  
  */
void Draw_Float(ext_client_custom_graphic_float_t *Graphic,char Graphic_Name[3],u32 Operate_Type,u32 Graphic_Layer,u32 Graphic_Color,float float_number,int useful_digits,u32 Font_Size,u32 Graphic_Width,u32 Start_x,u32 Start_y)
{
	int i;
	Graphic->data_cmd_id = 0x0101;
	Graphic->sender_ID = Sendor_ID;
	Graphic->receiver_ID = Receiver_ID;
	//图形名
	for(i=0;i<3&&Graphic_Name[i]!='\0';i++)
	Graphic->graphic_float_data_struct.graphic_name[i]=Graphic_Name[i];
	
	Graphic->graphic_float_data_struct.graphic_tpye = Graphic_Type_FLOAT;
	Graphic->graphic_float_data_struct.operate_tpye = Operate_Type;	
	Graphic->graphic_float_data_struct.layer = Graphic_Layer;
	Graphic->graphic_float_data_struct.color = Graphic_Color;

	Graphic->graphic_float_data_struct.width = Graphic_Width;
	Graphic->graphic_float_data_struct.start_x = Start_x;
	Graphic->graphic_float_data_struct.start_y = Start_y;
	
	Graphic->graphic_float_data_struct.start_angle = Font_Size;
	Graphic->graphic_float_data_struct.end_angle = useful_digits;
	Graphic->graphic_float_data_struct.float_number = float_number;
}





char Send_Once_Flag = 0;//初始化标志

//显示UI界面图形
void Show_UI()
{
	//机器人ID设置
	if(game_robot_state.robot_id < 100)
	{
		Sendor_ID = game_robot_state.robot_id;
		Receiver_ID = 0x100 + game_robot_state.robot_id;
	}
	else if(game_robot_state.robot_id > 100)
	{
		Sendor_ID = game_robot_state.robot_id;
		Receiver_ID = 0x0164 + (game_robot_state.robot_id - 100);		
	}
	
	//内存清除
	memset(char_one,'\0',sizeof(char_one));
	memset(char_two,'\0',sizeof(char_two));
	memset(char_three,'\0',sizeof(char_three));
	memset(char_four,'\0',sizeof(char_four));
	memset(char_five,'\0',sizeof(char_five));
	memset(char_six,'\0',sizeof(char_six));
	memset(char_seven,'\0',sizeof(char_seven));
	memset(char_eight,'\0',sizeof(char_eight));
	memset(char_nine,'\0',sizeof(char_nine));
	//只需要发送一次的数据，即添加图形操作
	if(Send_Once_Flag == 0)
	{
		
	Send_Once_Flag = 1;//标志位置1,不再进行添加操作
		
	//内存数据置空
	//将数据写入字符串变量（同printf操作）
	sprintf(char_one,"Super Cap %.2f",100.0);
	//将字符图形数据初始化
	Draw_Char(&character_one,"001",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_one,12,1,80,760);
	
	//同上
	sprintf(char_eight,"Friction Speed      %d ",friction_wheel);
	Draw_Char(&character_eight,"008",Graphic_Operate_ADD,9,Graphic_Color_Cyan,char_eight,12,1,80,780);
		
	sprintf(char_two,"Gimbal Mode is Rotate %d",1);
	Draw_Char(&character_two,"100",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_two,12,1,80,840);
	
	sprintf(char_three,"Shoot Limit Mode");
	Draw_Char(&character_three	,"101",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_three,12,1,80,820);		

	sprintf(char_four,"Pitch %.2f",imu.pit / 100.0f);
	Draw_Char(&character_four,"102",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_three,12,1,80,880);		
			
	sprintf(char_five,"Fric status off");
	Draw_Char(&character_five,"103",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_five,12,1,80,800);		
	
	sprintf(char_six,"AUTO MODE NORMAL");
	Draw_Char(&character_six,"104",Graphic_Operate_ADD,9,Graphic_Color_Yellow,char_six,12,1,80,860);		
	
		//初始化整数数据
	Draw_Int(&interger_one,"301",Graphic_Operate_ADD,9,Graphic_Color_Yellow,power_heat_data.shooter_id1_42mm_cooling_heat,12,1,80,820);
	
	//初始化浮点数数据
	Draw_Float(&float_one,"401",Graphic_Operate_ADD,9,Graphic_Color_Yellow,real_shoot_data.bullet_speed,3,12,1,80,840);
	
	//初始化直线数据

	Draw_Line(&line_two,"002",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-40,SCREEN_WIDTH/2,SCREEN_LENGTH/2-40,SCREEN_WIDTH/2-320);
	Draw_Line(&line_nine,"009",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2+40-24,SCREEN_WIDTH/2,SCREEN_LENGTH/2+40-24,SCREEN_WIDTH/2-320);
	//中心竖线
	Draw_Line(&line_thirteen,"013",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-12,SCREEN_WIDTH/2,SCREEN_LENGTH/2-12,SCREEN_WIDTH/2-320);
	
	Draw_Line(&line_ten,"010",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-12,SCREEN_WIDTH/2,SCREEN_LENGTH/2-5,SCREEN_WIDTH/2);
	Draw_Line(&line_four,"004",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-12,SCREEN_WIDTH/2-320,SCREEN_LENGTH/2-5,SCREEN_WIDTH/2-320);
	
	Draw_Line(&line_seventeen,"017",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-5,SCREEN_WIDTH/2,SCREEN_LENGTH/2-5,SCREEN_WIDTH/2-320);

	//距离射表
//打车全用黄色
	Draw_Line(&line_twenty,"020",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-40-8.5,SCREEN_WIDTH/2-120,SCREEN_LENGTH/2+40-8.5,SCREEN_WIDTH/2-120);
	Draw_Line(&line_three,"003",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-100,SCREEN_WIDTH/2-90,SCREEN_LENGTH/2+60,SCREEN_WIDTH/2-90); //-90
	Draw_Line(&line_five,"005",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-120,SCREEN_WIDTH/2-143,SCREEN_LENGTH/2+80,SCREEN_WIDTH/2-143); //143
	
	Draw_Line(&line_fourteen,"014",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-80,SCREEN_WIDTH/2-154,SCREEN_LENGTH/2+40,SCREEN_WIDTH/2-154); //154
	
	Draw_Line(&line_six,"006",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-40-8.5,SCREEN_WIDTH/2-168,SCREEN_LENGTH/2+40-8.5,SCREEN_WIDTH/2-168);
	Draw_Line(&line_fifteen,"015",Graphic_Operate_ADD,9,Graphic_Color_Yellow,1,SCREEN_LENGTH/2-60-8.5,SCREEN_WIDTH/2-220,SCREEN_LENGTH/2+60-8.5,SCREEN_WIDTH/2-220);//176  280
	Draw_Line(&line_eight,"008",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-80,SCREEN_WIDTH/2-316,SCREEN_LENGTH/2+40,SCREEN_WIDTH/2-316);//176  280
//	
	Draw_Circle(&circle_one,"a",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-12,SCREEN_WIDTH/2-120,15);
	Draw_Circle(&circle_two,"b",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-8.5,SCREEN_WIDTH/2-148.5,7);
	Draw_Circle(&circle_three,"c",Graphic_Operate_ADD,9,Graphic_Color_Cyan,1,SCREEN_LENGTH/2-8.5,SCREEN_WIDTH/2-220,5);
	Draw_Circle(&circle_four,"d",Graphic_Operate_ADD,9,Graphic_Color_Green,1,SCREEN_LENGTH/2-8.5,SCREEN_WIDTH/2-226,5);
	Draw_Circle(&circle_five,"e",Graphic_Operate_ADD,9,Graphic_Color_Green,1,SCREEN_LENGTH/2-8.5,SCREEN_WIDTH/2-152,7);
	Draw_Circle(&circle_six,"f",Graphic_Operate_ADD,9,Graphic_Color_Green,1,SCREEN_LENGTH/2-8.5,SCREEN_WIDTH/2-168-7,7);
	
	//前哨战
	//向裁判系统串口发送图形数据
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_one,sizeof(character_one));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_two,sizeof(character_two));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_three,sizeof(character_three));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_four,sizeof(character_four));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_five,sizeof(character_five));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_six,sizeof(character_six));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_seven,sizeof(character_seven));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_eight,sizeof(character_eight));

	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_one,sizeof(line_one));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_two,sizeof(line_two));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_three,sizeof(line_three));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_four,sizeof(line_four));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_five,sizeof(line_five));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_six,sizeof(line_six));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_seven,sizeof(line_seven));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_eight,sizeof(line_eight));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_nine,sizeof(line_nine));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_ten,sizeof(line_ten));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_eleven,sizeof(line_eleven));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_twelve,sizeof(line_twelve));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_thirteen,sizeof(line_thirteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_fourteen,sizeof(line_fourteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_fifteen,sizeof(line_fifteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_sixteen,sizeof(line_sixteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_seventeen,sizeof(line_seventeen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_eighteen,sizeof(line_eighteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_nineteen,sizeof(line_nineteen));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&line_twenty,sizeof(line_twenty));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_one,sizeof(circle_one));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_two,sizeof(circle_two));	
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_three,sizeof(circle_three));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_four,sizeof(circle_four));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_five,sizeof(circle_five));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&circle_six,sizeof(circle_six));
	}	
	

	//需要不断更新的数据发送
	else
	{
	{sprintf(char_one,"SuperCap %.2f Mode ON",SuperCap_Info.CapVot);}
	
	{sprintf(char_one,"SuperCap %.2f Mode OFF",SuperCap_Info.CapVot);}

	if(SuperCap_Info.CapVot < 16.0)
	{
		{sprintf(char_one,"SuperCap %.2f Mode ATTETION!!!",SuperCap_Info.CapVot);}
		Draw_Char(&character_one,"001",Graphic_Operate_CHANGE,9,Graphic_Color_Purplish_red,char_one,12,1,80,760);		
	}
	else if(SuperCap_Info.CapVot > 19.0 && SuperCap_Info.InputCurrent==1)
	{
		{sprintf(char_one,"SuperCap %.2f Mode RUN!",SuperCap_Info.CapVot);}
		Draw_Char(&character_one,"001",Graphic_Operate_CHANGE,9,Graphic_Color_Cyan,char_one,12,1,80,760);				
	}
	else 
	{
			{sprintf(char_one,"SuperCap %.2f Mode Middle!",SuperCap_Info.CapVot);}
		Draw_Char(&character_one,"001",Graphic_Operate_CHANGE,9,Graphic_Color_Green,char_one,12,1,80,760);		
	}
	
	
	sprintf(char_eight,"Friction Speed      %d ",friction_wheel);
	if(real_shoot_data.bullet_speed<14.6)
	{
		Draw_Char(&character_eight,"008",Graphic_Operate_CHANGE,9,Graphic_Color_Orange,char_eight,12,1,80,780);
	}
	else if(real_shoot_data.bullet_speed>15.5)
	{
		Draw_Char(&character_eight,"008",Graphic_Operate_CHANGE,9,Graphic_Color_Purplish_red,char_eight,12,1,80,780);
	}
	else if(real_shoot_data.bullet_speed>14.6 && real_shoot_data.bullet_speed<15.5)
	{
		Draw_Char(&character_eight,"008",Graphic_Operate_CHANGE,9,Graphic_Color_Cyan,char_eight,12,1,80,780);
	}
	
	if(Chassis_Status == CHASSIS_STATUS_FOLLOW)
	{
		sprintf(char_two,"CHASSIS        Mode Follow");
		Draw_Char(&character_two,"100",Graphic_Operate_CHANGE,9,Graphic_Color_Green,char_two,12,1,80,840);
	}
	else if(Chassis_Status == CHASSIS_STATUS_ROTATE)
	{
		sprintf(char_two,"CHASSIS        Mode Rotate");
		Draw_Char(&character_two,"100",Graphic_Operate_CHANGE,9,Graphic_Color_Yellow,char_two,12,1,80,840);
	}

	sprintf(char_four,"Pitch ANGLE    %.2f",Pitch_Angle_NUC / 100.0f);
	Draw_Char(&character_four,"102",Graphic_Operate_CHANGE,9,Graphic_Color_Green,char_four,12,1,80,880);		
	
	if(Shoot_Mode == SHOOT_MODE_RUN)//摩擦轮开启模式下
	{
	sprintf(char_five,"Friction wheel Mode ON");
	Draw_Char(&character_five,"103",Graphic_Operate_CHANGE,9,Graphic_Color_Cyan,char_five,12,1,80,800);		
	}		
	else if(abs(CAN_Shoot[0].Current_Speed) < 10 && Shoot_Mode != SHOOT_MODE_STOP)//摩擦轮堵住了
	{
	sprintf(char_five,"Friction wheel Mode Block");	
	Draw_Char(&character_five,"103",Graphic_Operate_CHANGE,9,Graphic_Color_Main,char_five,12,1,80,800);	
	}
	else if(Shoot_Mode == SHOOT_MODE_STOP)//摩擦轮关闭
	{
	sprintf(char_five,"Friction Wheel Mode OFF");
	Draw_Char(&character_five,"103",Graphic_Operate_CHANGE,9,Graphic_Color_Orange,char_five,12,1,80,800);						
	}
	
{	if(Aim_Mode==0)
	{
		sprintf(char_six,"AUTO AIM       MODE NORMAL");
		Draw_Char(&character_six,"104",Graphic_Operate_CHANGE,9,Graphic_Color_Green,char_six,12,1,80,860);
	}
	else if(Aim_Mode==1)
	{
		sprintf(char_six,"AUTO AIM       MODE SENTRY");
		Draw_Char(&character_six,"104",Graphic_Operate_CHANGE,9,Graphic_Color_Cyan,char_six,12,1,80,860);
	}
	else if(Aim_Mode==3)
	{
		sprintf(char_six,"AUTO AIM       MODE ROTATE");
		Draw_Char(&character_six,"104",Graphic_Operate_CHANGE,9,Graphic_Color_Yellow,char_six,12,1,80,860);
	}
}	
	if(LIMIT_CHECK_FULL_RIGHT == LIMIT_ON || LIMIT_CHECK_FULL_LEFT == LIMIT_ON)
	{
		sprintf(char_three,"Shoot Limit    Mode ON");	
		Draw_Char(&character_three,"101",Graphic_Operate_CHANGE,9,Graphic_Color_Cyan,char_three,12,1,80,820);	
	}
	else
	{
		sprintf(char_three,"Shoot Limit    Mode OFF");
		Draw_Char(&character_three,"101",Graphic_Operate_CHANGE,9,Graphic_Color_Orange,char_three,12,1,80,820);	
	}
	
	
	int TypeSize=0;
	TypeSize=game_robot_state.max_HP-game_robot_state.remain_HP;
	if(game_robot_state.remain_HP<=game_robot_state.max_HP/2)
	{
		sprintf(char_two,"   RUN   ");
		Draw_Char(&character_nine,"109",Graphic_Operate_CHANGE,9,Graphic_Color_Main,char_nine,TypeSize,4,1440,810);
	}
	else
	{
		sprintf(char_two,"         ");
		Draw_Char(&character_nine,"109",Graphic_Operate_CHANGE,9,Graphic_Color_Main,char_nine,TypeSize,4,1440,810);
	}
	
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_one,sizeof(character_one));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_two,sizeof(character_two));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_three,sizeof(character_three));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_four,sizeof(character_four));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_five,sizeof(character_five));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_six,sizeof(character_six));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_seven,sizeof(character_seven));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_eight,sizeof(character_eight));
	referee_data_pack_handle(0xA5,0x0301,(uint8_t *)&character_nine,sizeof(character_nine));

	}
}
