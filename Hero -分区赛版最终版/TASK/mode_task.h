#ifndef __MODE_TASK_H
#define __MODE_TASK_H

#include "main.h"
void mode_task_create();

typedef enum
{
	CHASSIS_STATUS_STOP = 0,
	CHASSIS_STATUS_ROTATE,
	CHASSIS_STATUS_FOLLOW,

}Chassis_Status_Enum;
extern int Friction_Wheel_Time;
extern int Friction_Wheel_Flag;
extern Chassis_Status_Enum Chassis_Status;
extern int Friction_Up_Flag;
#endif