#include "shoot_task.h"

Shoot_Mode_Enum Shoot_Mode;
Shoot_Cmd_Enum Shoot_Cmd = SHOOT_CMD_STOP;

void Shoot_Stop_Control();
void Shoot_Run_Control();
void Bullet_Block_Control();

TaskHandle_t SHOOT_Task_Handler;
void shoot_task(void *p_arg);
int friction_wheel=6300;

/*
*@title：发射任务创建
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/
void shoot_task_create()
{
		xTaskCreate((TaskFunction_t )shoot_task,          //任务函数
							(const char*    )"shoot_task",          //任务名称
							(uint16_t       )SHOOT_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                //传递给任务函数的参数
							(UBaseType_t    )SHOOT_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&SHOOT_Task_Handler);  //任务句柄  
}

/*
*@title：发射任务
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/
void shoot_task(void *p_arg)
{
		EventBits_t  EventValue;

	while(1)
	{
		EventValue = xEventGroupWaitBits((EventGroupHandle_t)EventGroupHandler,
																			(EventBits_t      )SHOOT_EXE_SIGNAL,
																			(BaseType_t       )pdTRUE,
																			(BaseType_t       )pdFALSE,
																			(TickType_t       )portMAX_DELAY);
		switch(Shoot_Mode)
		{
			//发射停止
			case SHOOT_MODE_STOP:
				Shoot_Stop_Control();
				break;
			//发射运行
			case SHOOT_MODE_RUN:
				Shoot_Run_Control();
				break;

			default:
				break;
		}

		xEventGroupSetBits(EventGroupHandler,SHOOT_SIGNAL);	//发射事件组置位

	}

}

/*
*@Description：发射无力控制
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Shoot_Stop_Control() 
{
	Shoot_Cmd = SHOOT_CMD_STOP;//停止发射

	CAN_Trigger.Target_Current = Pid_Calc(&PID_Trigger,CAN_Trigger.Current_Speed,0);
	CAN_Shoot[0].Target_Current = Pid_Calc(&PID_Shoot[0],CAN_Shoot[0].Current_Speed,0);
	CAN_Shoot[1].Target_Current = Pid_Calc(&PID_Shoot[1],CAN_Shoot[1].Current_Speed,0);		
}


extern int Trig_Time ;
volatile float shoot_speed_get = 0;



//拨弹盘的转速
int Shoot_Speed = 0;
/*
*@Description：发射运行控制
*@param 1：	  参数1
*@param 2：	  参数2
*@return:：	  返回值
*/
void Shoot_Run_Control()
{
	//单发模式
	if(Shoot_Cmd == SHOOT_CMD_ONCE)
	{
		//通过判断裁判系统两次弹丸射速确定是否发射一颗弹丸
		if(real_shoot_data.bullet_speed !=shoot_speed_get )
		{
			Shoot_Speed = 0;
			Shoot_Cmd = SHOOT_CMD_STOP;//停止发射
		}
		else
		{
			//单发要求快速出弹
			Shoot_Speed = -2700;
		}
	}
	//连发模式
	else if(Shoot_Cmd == SHOOT_CMD_LONG)
	{
		//判断拨杆是否位于下方或者鼠标左键一直按住
		if(Trig_Time == 0)
		{
			Shoot_Speed = 0;
			Shoot_Cmd = SHOOT_CMD_STOP;
		}
		else
		{
			//连发要求发弹稳定
			Shoot_Speed = -2400;
		}
	}
	
	else if(Shoot_Cmd == SHOOT_CMD_TRIG)
	{
		Shoot_Speed = -1200;
		if(LIMIT_CHECK_FULL_RIGHT == LIMIT_ON)
		{
			Shoot_Cmd=SHOOT_CMD_STOP;
		}
	}
	else
	{
			Shoot_Speed = 0;
	}
	
	//发射卡弹控制
	Bullet_Block_Control();
	
	//热量限制（防止超热量）留出三颗弹丸余量，保证高射频下不超热量
	if(game_robot_state.shooter_id1_42mm_cooling_limit-power_heat_data.shooter_id1_42mm_cooling_heat<99)
	{
		Shoot_Speed = 0;
	}
	
	//射速获取
	shoot_speed_get = real_shoot_data.bullet_speed;

	//拨弹盘转速处理
	CAN_Trigger.Target_Current = Pid_Calc(&PID_Trigger,CAN_Trigger.Current_Speed,Shoot_Speed);			
	
	
	if(game_robot_state.shooter_id1_42mm_speed_limit == 10)
	{
	//摩擦轮转速处理
	CAN_Shoot[0].Target_Current = Pid_Calc(&PID_Shoot[0],CAN_Shoot[0].Current_Speed,-4200);
	CAN_Shoot[1].Target_Current = Pid_Calc(&PID_Shoot[1],CAN_Shoot[1].Current_Speed,4200);		
	}
	else if(game_robot_state.shooter_id1_42mm_speed_limit == 16)
	{
	//摩擦轮转速处理
		//降速
	if(((DBUS.PC.Keyboard & KEY_Z && DBUS.PC.Keyboard & KEY_CTRL) || (DBUS.PC.Keyboard & KEY_B && DBUS.PC.Keyboard & KEY_CTRL)) && Friction_Wheel_Flag==0)
	{
		Friction_Wheel_Time++;
		if(Friction_Wheel_Time>100 && (DBUS.PC.Keyboard & KEY_Z && DBUS.PC.Keyboard & KEY_CTRL))
		{
			friction_wheel-=100;
			Friction_Wheel_Flag=1;
			Friction_Wheel_Time=0;
			Friction_Up_Flag++;
		}
		//升速
		else if(Friction_Wheel_Time>100 &&(DBUS.PC.Keyboard & KEY_B && DBUS.PC.Keyboard & KEY_CTRL))
		{
			friction_wheel+=100;
			Friction_Wheel_Flag=1;
			Friction_Wheel_Time=0;
			Friction_Up_Flag++;
		}
	}
	
	CAN_Shoot[0].Target_Current = Pid_Calc(&PID_Shoot[0],CAN_Shoot[0].Current_Speed,-friction_wheel);//5785
	CAN_Shoot[1].Target_Current = Pid_Calc(&PID_Shoot[1],CAN_Shoot[1].Current_Speed,friction_wheel);		
		
	}
}
/**
 *@Function:		卡弹反转控制
 *@Description:	
 *@Param:       形参
 *@Return:	  	返回值
 */
void Bullet_Block_Control()
{
		//堵转周期计时
	static int Block_Time = 0;
	//反转时间计时
	static int Block_Reverse_Time = 0;
	
	//电机转速过大时检测反转
	if(CAN_Trigger.Current_Speed>-100 && CAN_Trigger.Current < -3500 && CAN_Trigger.Current_Speed<0)
	{
		Block_Time ++;
	}
		
	//反转
	if(Block_Reverse_Time >= 1)
	{
		Block_Reverse_Time++;
		
		//反转时间大于40ms后恢复正转
		if(Block_Reverse_Time > 20)
		{
			Block_Reverse_Time = 0;
			Block_Time = 0;
		}
		else
		{
			//反转时电流较小时才允许反转，防止反转堵转
			if(CAN_Trigger.Current < 9000)
			{Shoot_Speed = 4000;}
			//电流较大恢复正转
			else
			{
				Block_Reverse_Time = 0;
				Block_Time = 0;
			}
		}
	}
	//未反转时
	else
	{
		//判断堵转时间
		//堵转时间10*发射任务周期（2ms）= 20ms
		if(Block_Time == 10)
		{
			Block_Reverse_Time = 1;
		}		
	}
	
}