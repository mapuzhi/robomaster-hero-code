#ifndef __LIMIT_H
#define __LIMIT_H

#include "main.h"
#define LIMIT_ON 1
#define LIMIT_OFF 0
#define LIMIT_CHECK_FULL_RIGHT   GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_13)
#define LIMIT_CHECK_FULL_LEFT    GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_14)
void limit_Init();

#endif 