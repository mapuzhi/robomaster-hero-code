#include "super_task.h"


TaskHandle_t SUPER_Task_Handler;
void super_task(void *p_arg);

extern EventGroupHandle_t EventGroupHandler;


void super_task_create()
{
		xTaskCreate((TaskFunction_t )super_task,            //任务函数
							(const char*    )"super_task",          //任务名称
							(uint16_t       )SUPER_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )SUPER_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&SUPER_Task_Handler);   //任务句柄  
}
u16 super_delay_time = 0;
void super_task(void *p_arg)
{
		static portTickType xLastWakeTime;  
    const portTickType xFrequency = pdMS_TO_TICKS(35);  
    // 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
    xLastWakeTime = xTaskGetTickCount(); 
	
	while(1)
	{
		//按住CTRL键并且电容电压大于19V时开启电容
		if(DBUS.PC.Keyboard & KEY_CTRL && !(DBUS.PC.Keyboard & KEY_C) && Supper_Cup_Remain_Voltage > 19.0)
		{
			super_delay_time ++;
			if(super_delay_time >= 20)
			{Chassis_Sta = SUPPER;}
			TX_Cap = 1;
			
		}
		//超级电容电压低于16V或者松开CTRL键则关闭电容
		else if(Supper_Cup_Remain_Voltage < 16.0 || (!(DBUS.PC.Keyboard & KEY_CTRL) ))
		{
			super_delay_time = 0;
			
			{Chassis_Sta = NORMAL;}
			
			TX_Cap = 0;
		}
		
		xEventGroupSetBits(EventGroupHandler,SUPER_SIGNAL);	//超级电容事件组置位	
		vTaskDelayUntil( &xLastWakeTime,xFrequency );	//任务切换
	}
}