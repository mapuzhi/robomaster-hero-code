#include "graphic_task.h"
TaskHandle_t GRAPHIC_Task_Handler;
void graphic_task(void *p_arg);


void graphic_task_create()
{
		xTaskCreate((TaskFunction_t )graphic_task,            //任务函数
							(const char*    )"graphic_task",          //任务名称
							(uint16_t       )GRAPHIC_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )GRAPHIC_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&GRAPHIC_Task_Handler);   //任务句柄  
}

void graphic_task(void *p_arg)
{
	while(1)
	{
		//图形界面画图
		Show_UI();
		vTaskDelay(20);//任务切换

	}
}
