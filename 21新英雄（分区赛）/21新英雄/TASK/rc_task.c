#include "rc_task.h"

TaskHandle_t RC_Task_Handler;
void rc_task(void *p_arg);




void rc_task_create()
{
		xTaskCreate((TaskFunction_t )rc_task,            //任务函数
							(const char*    )"rc_task",          //任务名称
							(uint16_t       )RC_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )RC_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&RC_Task_Handler);   //任务句柄  
}

void rc_task(void *p_arg)
{
	while(1)
	{
		
			{//死区限制
				if(abs(DBUS.RC.ch0)<5)   DBUS.RC.ch0 = 0;
				if(abs(DBUS.RC.ch1)<5)   DBUS.RC.ch1 = 0;
				if(abs(DBUS.RC.ch2)<5)   DBUS.RC.ch2 = 0;
				if(abs(DBUS.RC.ch3)<5)   DBUS.RC.ch3 = 0;
			}
			
			if(!((DBUS.RC.Switch_Left == RC_SW_DOWN) && (DBUS.RC.Switch_Right == RC_SW_DOWN)))
			{IWDG_Feed();}
			
			
			Key_Control();	//按键控制
			Mouse_Control();	//鼠标控制
			
			
	    Control_Info.Chassis.Derection_X = - DBUS.RC.ch0 / 3.0f + Chassis_Speed_X;	//速度X分量赋值
		  Control_Info.Chassis.Derection_Y = - DBUS.RC.ch1 / 3.0f + Chassis_Speed_Y;	//速度Y分量赋值
		
			Control_Info.PTZ.Yaw = - DBUS.RC.ch2/300.0f - DBUS.PC.X / 20.0f + Yaw_Quick_Val;	//Yaw赋值
			Control_Info.PTZ.Pitch = - DBUS.RC.ch3/300.0f + DBUS.PC.Y / 10.0f;	//Pitch赋值

	
		vTaskDelay(2);	//任务切换
	
	}
}