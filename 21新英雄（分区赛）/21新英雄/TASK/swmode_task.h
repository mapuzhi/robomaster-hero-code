#ifndef __SWMODE_TASK_H
#define __SWMODE_TASK_H

#include "main.h"
void swmode_task_create();

typedef enum
{
  CHASSIS_LOW_SPEED_MODE,//低速
  CHASSIS_MID_SPEED_MODE,//中速
  CHASSIS_HIGH_SPEED_MODE,//高速
	CHASSIS_ZERO_SPEED_MODE	//零速
} chassis_speed_mode_e;	//底盘速度模式


typedef enum
{
  GIMBAL_FOLLOW_MODE,//跟随
  GIMBAL_ROTATE_MODE,//陀螺
} gimbal_mode_e;	//云台模式


typedef enum
{
  CONTROL_MANUAL_MODE,//手动
  CONTROL_AUTO_MODE,//自瞄
} control_mode_e;	//控制模式

typedef enum
{
  SHOOT_OPEN_MODE,//开启摩擦轮
  SHOOT_CLOSE_MODE,//关闭摩擦轮
} shoot_mode_e;	//发射模式

typedef enum
{
	ONLINE_REFEREE_MODE,	//裁判系统在线模式
	OFFLINE_REFEREE_MODE	//裁判系统离线模式
} referee_mode_e;

extern chassis_speed_mode_e chassis_speed_mode;
extern gimbal_mode_e gimbal_mode;
extern control_mode_e control_mode;
extern shoot_mode_e shoot_mode;
extern referee_mode_e referee_mode;
#endif