#include "gimbal_task.h"



TaskHandle_t GIMBAL_Task_Handler;
void gimbal_task(void *p_arg);

float Yaw_NUC_Angle,Pitch206_NUC_Angle,Pitch207_NUC_Angle;
extern EventGroupHandle_t EventGroupHandler;


void gimbal_task_create()
{
		xTaskCreate((TaskFunction_t )gimbal_task,            //任务函数
							(const char*    )"gimbal_task",          //任务名称
							(uint16_t       )GIMBAL_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )GIMBAL_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&GIMBAL_Task_Handler);   //任务句柄  
}

u8 NUC_MODE_FLAG = 0;//自瞄开启标志
void gimbal_task(void *p_arg)
{
		static portTickType xLastWakeTime;  
    const portTickType xFrequency = pdMS_TO_TICKS(3);  
    // 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
    xLastWakeTime = xTaskGetTickCount(); 
	
	PTZ_Init();																		 //初始化云台

	while(1)
	{
		//手动模式
		if(control_mode == CONTROL_MANUAL_MODE)
		{
			//判断是否从自瞄切换过来
		if(NUC_MODE_FLAG == 1)
		{
			//将自瞄角度切换过来
			Aim_Yaw_Angle = Yaw_Angle_IMU;
			Aim_Pitch206_Angle = Pitch_Motor_206.Real_MechanicalAngle;
		}
		NUC_MODE_FLAG = 0;
		
							//按键控制Yaw轴不动
							if(DBUS.PC.Keyboard & KEY_SHIFT)
							{
								Aim_Yaw_Angle=Aim_Yaw_Angle;
							}
							else
							{
								Aim_Yaw_Angle += Control_Info.PTZ.Yaw;
							}
							//按键按住控制Pitch不动
							if((DBUS.PC.Keyboard & KEY_V))
							{
								Aim_Pitch206_Angle=Aim_Pitch206_Angle;
							}
							else
							{
								Aim_Pitch206_Angle += Control_Info.PTZ.Pitch;
							}
							
		////目标Yaw轴角度
		
		PID_Yaw_205_MechAngle.PIDout = Pid_Calc(&PID_Yaw_205_MechAngle, Yaw_Angle_IMU, Aim_Yaw_Angle);
		Yaw_Motor_205.Target_Speed   = Pid_Calc(&PID_Yaw_205_Speed,mpu_data.gz*0.1f, PID_Yaw_205_MechAngle.PIDout);
		
		
		////目标Pitch轴角度

		if(Aim_Pitch206_Angle > LeftPitch_Angle_Max)  Aim_Pitch206_Angle = LeftPitch_Angle_Max;
		if(Aim_Pitch206_Angle < LeftPitch_Angle_Min)  Aim_Pitch206_Angle = LeftPitch_Angle_Min;

			
			
		PID_Pitch_206_MechAngle.PIDout = Pid_Calc(&PID_Pitch_206_MechAngle, Pitch_Motor_206.Real_MechanicalAngle, Aim_Pitch206_Angle);
		Pitch_Motor_206.Target_Speed   = Pid_Calc(&PID_Pitch_206_Speed,mpu_data.gy*0.1f, PID_Pitch_206_MechAngle.PIDout);
		
		}
		//自瞄模式
		else if(control_mode == CONTROL_AUTO_MODE)
		{
		
		NUC_MODE_FLAG = 1;
		
		//判断NUC是否有数据，为0时保持当前角度，非0时进行角度控制
		if(Yaw_NUC)		Aim_Yaw_Angle = Yaw_Angle_IMU;
		
//		if(!(DBUS.PC.Keyboard & KEY_SHIFT))
		Aim_Yaw_Angle = Aim_Yaw_Angle - Yaw_NUC;
			


							
		PID_Yaw_205_NUC_Mech.PIDout = Pid_Calc(&PID_Yaw_205_NUC_Mech, Yaw_Angle_IMU, Aim_Yaw_Angle);
		Yaw_Motor_205.Target_Speed   = Pid_Calc(&PID_Yaw_205_NUC_Speed,mpu_data.gz*0.1f, PID_Yaw_205_NUC_Mech.PIDout);
			
		//判断NUC是否有数据，为0时保持当前角度，非0时进行角度控制				
		if(Pitch_NUC)		Aim_Pitch206_Angle = Pitch_Motor_206.Real_MechanicalAngle;
		
			
		Aim_Pitch206_Angle = Aim_Pitch206_Angle - Pitch_NUC;
			
		if(Aim_Pitch206_Angle > LeftPitch_Angle_Max)  Aim_Pitch206_Angle = LeftPitch_Angle_Max;
		if(Aim_Pitch206_Angle < LeftPitch_Angle_Min)  Aim_Pitch206_Angle = LeftPitch_Angle_Min;
		
		PID_Pitch_206_NUC_Mech.PIDout = Pid_Calc(&PID_Pitch_206_NUC_Mech, Pitch_Motor_206.Real_MechanicalAngle, Aim_Pitch206_Angle);	
		Pitch_Motor_206.Target_Speed   = Pid_Calc(&PID_Pitch_206_NUC_Speed,mpu_data.gy*0.1f, PID_Pitch_206_NUC_Mech.PIDout);
		
		}
		
		xEventGroupSetBits(EventGroupHandler,GIMBAL_SIGNAL);	//云台事件组置位
		xEventGroupSetBits(EventGroupHandler,ARMORY_SIGNAL);	//弹仓事件组置位

		vTaskDelayUntil( &xLastWakeTime,xFrequency );	//任务切换
	}
}