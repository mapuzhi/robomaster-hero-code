#include "led_task.h"

TaskHandle_t LED_Task_Handler;
void led_task(void *p_arg);

void led_task_create()
{
		xTaskCreate((TaskFunction_t )led_task,            //任务函数
							(const char*    )"led_task",          //任务名称
							(uint16_t       )LED_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )LED_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&LED_Task_Handler);   //任务句柄  
}

uint8_t led_run_flag = 0;
void led_task(void *p_arg)
{
	while(1)
	{

		{PEout(11)=!PEout(11);}
		vTaskDelay(250);
		if(DR16_Flash_Val > 0)
		PFout(14)=!PFout(14);
		
		vTaskDelay(250);
	
	if(led_run_flag == 0)
{
	led_run_flag = 1;
	//开场
	for(int i=1;i<=8;i++)
	{
		vTaskDelay(20);
		PGout(i)=!PGout(i);
	}
	for(int i=8;i>=1;i--)
	{
		vTaskDelay(20);
		PGout(i)=!PGout(i);
	}
		for(int i=1;i<=4;i++)
	{
		vTaskDelay(20);
		PGout(i)=!PGout(i);
		PGout(9-i)=!PGout(9-i);
	}
		for(int i=4;i>=1;i--)
	{
		vTaskDelay(20);
		PGout(i)=!PGout(i);
		PGout(9-i)=!PGout(9-i);
	}
	//结尾
	for(int i=1;i<=8;i++)
	{
		PGout(i)=!PGout(i);
	}
		vTaskDelay(20);
	for(int i=8;i>=1;i--)
	{
		PGout(i)=!PGout(i);
	}
		vTaskDelay(20);
	for(int i=1;i<=8;i++)
	{
		PGout(i)=!PGout(i);
	}
}
	}
}