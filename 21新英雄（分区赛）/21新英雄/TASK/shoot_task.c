#include "shoot_task.h"
TaskHandle_t SHOOT_Task_Handler;
void shoot_task(void *p_arg);
extern EventGroupHandle_t EventGroupHandler;

u8 Poke_Mouse_Flag=1;
u8 Stick_Delay_Flag=0;


void Fric_Control();
void Shoot_Bullet_Control();
void Dial_Bullet_Control();

void shoot_task_create()
{
		xTaskCreate((TaskFunction_t )shoot_task,            //任务函数
							(const char*    )"shoot_task",          //任务名称
							(uint16_t       )SHOOT_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )SHOOT_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&SHOOT_Task_Handler);   //任务句柄  
}

void shoot_task(void *p_arg)
{
		static portTickType xLastWakeTime;  
    const portTickType xFrequency = pdMS_TO_TICKS(5);  
    // 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
    xLastWakeTime = xTaskGetTickCount(); 
	
	while(1)
	{
		
		Fric_Control();	//摩擦轮控制
		
		if(shoot_mode == SHOOT_OPEN_MODE)
		Dial_Bullet_Control();	//拨弹盘控制
		else
		{
			Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, DBUS.RC.ch4);
		}
		
		
		Shoot_Bullet_Control();	//发射弹丸控制
		
		
			
		xEventGroupSetBits(EventGroupHandler,SHOOT_SIGNAL);	//发射事件组置位
		vTaskDelayUntil( &xLastWakeTime,xFrequency );  //任务切换
	}
}

short Poke_Aim_Angle;
short Poke_Use_Flag = 0;
float Shoot_Heat_Rate = 1;
short heat_ban_count0 = 0;
//发弹控制
void Shoot_Bullet_Control()
{
	 ////热量控制	 
	if(referee_mode == ONLINE_REFEREE_MODE)
	{ 
		switch (game_robot_state.robot_level)
		{
			case 1:
			{
				Shoot_Heat_Rate = RISK_HEAT_RATE_1;
			}break;
			
			case 2:
			{
				Shoot_Heat_Rate = RISK_HEAT_RATE_2;
			}break;
			
			case 3:
			{
				Shoot_Heat_Rate = RISK_HEAT_RATE_3;
			}break;
			
			default:
			{
			}	
		}
		
		if((power_heat_data.shooter_id1_42mm_cooling_heat<=game_robot_state.shooter_id1_42mm_cooling_limit)&&(game_robot_state.shooter_id1_42mm_cooling_limit-power_heat_data.shooter_id1_42mm_cooling_heat+game_robot_state.shooter_id1_42mm_cooling_rate*Shoot_Heat_Rate)>100)	
		{
			if(heat_ban_count0 <= 100) heat_ban_count0++;
			else  Shoot_heat_Aban = 0;
		}
		
		else
		{
			heat_ban_count0 = 0;
			Shoot_heat_Aban = 1;
		}
		
		if(DBUS.PC.Keyboard & KEY_CTRL)//ctrl键强开发射
		{
			Shoot_heat_Aban=0;
		}
		
	}
	

//			//拨杆
		  if((DBUS.RC.Switch_Left == RC_SW_DOWN)//左拨杆为下
		     &&(shoot_poke_count0 > 0)//左拨杆为上面或者中间
		     &&(shoot_mode == SHOOT_OPEN_MODE)//射击状态
		     &&(Shoot_heat_Aban==0))//冷却允许
		  {
				Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -2000);	
			}

			//左拨杆为上或者中间
		  if(DBUS.RC.Switch_Left == RC_SW_MID) 
			{
			shoot_poke_count0=100;	//180  
			}
			
			//鼠标左击
			if((DBUS.RC.Switch_Left != RC_SW_DOWN)//左拨杆不为下
		     &&(shoot_poke_count1>0)	//长按左键
		     &&(Shoot_Sta==1)//射击状态
		     &&(Shoot_heat_Aban==0))//冷却允许      
		  {
				Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -2000);	
		  }
			
			if((Left_Mouse_Press_Sta==10)&&(Poke_Mouse_Flag==1)&&(Stick_Delay_Flag==1))//左键长按 
		  {
		  shoot_poke_count1=100; //左键计时值填充，按下键后一段时间后才能再按
		  Poke_Mouse_Flag=0;	  
		  }
			
			if(Left_Mouse_Press_Sta==10)
			{
				Stick_Delay_Flag=0;
			}
			else
			{
				Stick_Delay_Flag=1;
			}
		
		
}
//拨弹盘正反转
short Poke_Up_derection = -1;
short poke_up_derection_count = 0;
short Poke_Reverse_Count = 0;
//拨弹盘控制
void Dial_Bullet_Control()
{
		if(LIMIT_STA == 0) //弹碰到限位开关，弹链满弹，此时拨弹盘停止转动
		{
			Poke_Motor_207.Target_Speed = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, 0);
		}
		//未触碰到限位开关时，不停正反转拨盘，直到弹链充满
		else
		{
			if(abs(Poke_Motor_207.Real_Speed) < 10)//速度低于10一段时间时，正反转
			{
				if(poke_up_derection_count<=10) poke_up_derection_count++;
				else 			
					{
						Poke_Up_derection=Poke_Up_derection*(-1);//一段时间后反转
						poke_up_derection_count=0;
					}
			}
			Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, Poke_Up_derection*1000);//750		
			
			
			//上弹时速度慢下来了
			if(((Poke_Motor_207.Real_Speed > -10) && (Poke_Motor_207.Real_Speed < 0)) || Poke_Reverse_Count != 0)
			{	//退弹
				if(poke_up_derection_count<=10) poke_up_derection_count++;
				else
				{
					Poke_Reverse_Count ++ ;
					poke_up_derection_count = 0;
				}
				if((Poke_Reverse_Count < 10) && (Poke_Reverse_Count > 0))
				{
					Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, 750);//750		
					Poke_Reverse_Count ++ ;
				}
				else
				{
					Poke_Reverse_Count = 0;
				}
			}
			//开始上弹
			else
			{
				Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -850);//750			
			}
		}
		
			if(DBUS.PC.Keyboard & KEY_R)	//按住R键，反转拨弹盘
			{
			Poke_Motor_207.Target_Speed  = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, 1000);
			}
			if(DBUS.PC.Keyboard & KEY_B)	//按住R键，反转拨弹盘
			{
			Poke_Motor_207.Target_Speed  = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, 0);
			}
}

short Shoot_Fric_Speed,Shoot_Fric_Speed_201,Shoot_Fric_Speed_202;	//发射摩擦轮速度

short Shoot_Fric_Speed_Open;	//摩擦轮开启时的速度

short shoot_speed_control = 5000;	//调试用

//摩擦轮控制，拨弹盘控制
void Fric_Control()
{
			//射速切换
			//20 0 0
			if(game_robot_state.shooter_id1_42mm_speed_limit == 10)	{Shoot_Fric_Speed_Open = 3620;}
			
			else if(game_robot_state.shooter_id1_42mm_speed_limit == 16)	{Shoot_Fric_Speed_Open = 4750;}
			
			//射击模式切换，开关摩擦轮
			if(shoot_mode == SHOOT_OPEN_MODE)	
			{
				Shoot_Fric_Speed = Shoot_Fric_Speed_Open;
				Shoot_Fric_Speed_201 = Shoot_Fric_Speed_Open ;
				Shoot_Fric_Speed_202 = Shoot_Fric_Speed_Open ;
			}
			else if(shoot_mode == SHOOT_CLOSE_MODE)	
			{	
				Shoot_Fric_Speed = 0;
				Shoot_Fric_Speed_201 = 0;
				Shoot_Fric_Speed_202 = 0;
			}
			
		//摩擦轮速度赋值
		Shoot_Motor_201.Target_Speed = Pid_Calc(&PID_Shoot_201_Left, Shoot_Motor_201.Real_Speed, -Shoot_Fric_Speed_201);  //201为负202为正，推出弹丸
		Shoot_Motor_202.Target_Speed = Pid_Calc(&PID_Shoot_202_Right, Shoot_Motor_202.Real_Speed, Shoot_Fric_Speed_202);
			
		if(DBUS.PC.Keyboard & KEY_R)	//按住R键，反转摩擦轮
		{
			Shoot_Motor_201.Target_Speed = Pid_Calc(&PID_Shoot_201_Left, Shoot_Motor_201.Real_Speed, 1000);  //201为负202为正，推出弹丸
			Shoot_Motor_202.Target_Speed = Pid_Calc(&PID_Shoot_202_Right, Shoot_Motor_202.Real_Speed, -1000);	
		}
}






