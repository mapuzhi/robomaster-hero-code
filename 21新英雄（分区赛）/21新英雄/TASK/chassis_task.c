#include "chassis_task.h"

TaskHandle_t CHASSIS_Task_Handler;
void chassis_task(void *p_arg);

#define ANGLE_TO_RAD 0.01745329251994329576923690768489f

short FL_Speed, FR_Speed, BR_Speed, BL_Speed;
float Power_Control_Rate = 1.0;
extern EventGroupHandle_t EventGroupHandler;

/*
*速度阈值调整
*/
float Speed_Wheel_Max(short speed1,short speed2,short speed3,short speed4)
{
	short Max_Speed=0;
	int  Max_Wheel_Speed = 5000;
	
	if(abs(speed1)>Max_Speed)
	Max_Speed=abs(speed1);
	if(abs(speed2)>Max_Speed)
	Max_Speed=abs(speed2);
		if(abs(speed3)>Max_Speed)
	Max_Speed=abs(speed3);	
		if(abs(speed4)>Max_Speed)
	Max_Speed=abs(speed4);
		
		if(Max_Speed>Max_Wheel_Speed)
		{
			return (Max_Wheel_Speed/Max_Speed);
		}
		else
		{return 1.0f;}
}

void chassis_task_create()
{
		xTaskCreate((TaskFunction_t )chassis_task,            //任务函数
							(const char*    )"chassis_task",          //任务名称
							(uint16_t       )CHASSIS_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )CHASSIS_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&CHASSIS_Task_Handler);   //任务句柄  
}
//陀螺反转
int rotate_reverse_flag = 1;
int rotate_reverse_flag_last = 1;

void chassis_task(void *p_arg)
{
	  static portTickType xLastWakeTime;  
    const portTickType xFrequency = pdMS_TO_TICKS(5);  
    // 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
    xLastWakeTime = xTaskGetTickCount(); 

	while(1)
	{
		
		int vx,vy,vw;	//x y 方向速度分量，w旋转速度
		float yaw_real_angle,use_yaw_angle;	//Yaw轴与底盘的真实夹角		Yaw轴与底盘的用户夹角（范围大于360°）
		yaw_real_angle=360.0f*(PTZ_Init_Value.Initial_Yaw_Angle-Yaw_Motor_205.Real_MechanicalAngle)/8192.0f;	//初始电机角度值与当前电机角度值之差
		if(yaw_real_angle < 0)	{use_yaw_angle=yaw_real_angle+360.0f;}	//计算用户夹角
		else	{use_yaw_angle=yaw_real_angle;}
		
		//跟随模式
		if(gimbal_mode == GIMBAL_FOLLOW_MODE)
		{
			rotate_reverse_flag_last = GIMBAL_FOLLOW_MODE;
			
			
			Control_Info.Chassis.Derection_Omega = ((Yaw_Motor_205.Real_MechanicalAngle - PTZ_Init_Value.Initial_Yaw_Angle) / 2.0f );	//底盘跟随速度
			if(abs(Control_Info.Chassis.Derection_Omega) < 10)  Control_Info.Chassis.Derection_Omega = 0;		//跟随速度死区
			
			if((DBUS.PC.Keyboard & KEY_V))
			{
				Control_Info.Chassis.Derection_Omega = 0;
			}
			
			//跟随速度限幅
			if(Control_Info.Chassis.Derection_Omega > 160){Control_Info.Chassis.Derection_Omega = 160;}
			else if(Control_Info.Chassis.Derection_Omega < -160){Control_Info.Chassis.Derection_Omega = -160;}
			
			//四个轮子的速度合成
			FL_Speed  =(-Control_Info.Chassis.Derection_X - Control_Info.Chassis.Derection_Y) - Control_Info.Chassis.Derection_Omega;
			FR_Speed =(-Control_Info.Chassis.Derection_X + Control_Info.Chassis.Derection_Y) - Control_Info.Chassis.Derection_Omega;
			BR_Speed  =( Control_Info.Chassis.Derection_X + Control_Info.Chassis.Derection_Y) - Control_Info.Chassis.Derection_Omega;
			BL_Speed   =( Control_Info.Chassis.Derection_X - Control_Info.Chassis.Derection_Y) - Control_Info.Chassis.Derection_Omega;
		}
		
		//大陀螺模式
		else if(gimbal_mode == GIMBAL_ROTATE_MODE)
		{
			//陀螺速度方向反转
			if(rotate_reverse_flag_last != gimbal_mode)
			{
//				rotate_reverse_flag = -rotate_reverse_flag;
			}
			rotate_reverse_flag_last = GIMBAL_ROTATE_MODE;
			
			
			//陀螺模式下速度分解合成
			vx = sin((double)(use_yaw_angle*ANGLE_TO_RAD))*Control_Info.Chassis.Derection_Y+cos((double)(use_yaw_angle*ANGLE_TO_RAD))*Control_Info.Chassis.Derection_X;
			vy = cos((double)(use_yaw_angle*ANGLE_TO_RAD))*Control_Info.Chassis.Derection_Y-sin((double)(use_yaw_angle*ANGLE_TO_RAD))*Control_Info.Chassis.Derection_X;
			
			
			if(Rotate_Flag == 1)
			{vw = rotate_reverse_flag*150;}//旋转速度
			if(Rotate_Flag == 2)
			{vw = rotate_reverse_flag*250;}//旋转速度	250	

			
			//陀螺指定转速
//			if(Chassis_Sta == SUPPER)
//			vw = rotate_reverse_flag*300;	
			
			//四个轮子的速度合成			
			FL_Speed  =(-vx - vy) - vw;
			FR_Speed =(-vx + vy) - vw;
			BR_Speed  =( vx + vy) - vw;
			BL_Speed   =( vx - vy) - vw;
		}
			
			//PID计算赋给电机电流值
			Chassis_Motor_201.Target_Speed = Pid_Calc(&PID_Chassis_201, Chassis_Motor_201.Real_Speed, FL_Speed);
			Chassis_Motor_202.Target_Speed = Pid_Calc(&PID_Chassis_202, Chassis_Motor_202.Real_Speed, FR_Speed);
			Chassis_Motor_203.Target_Speed = Pid_Calc(&PID_Chassis_203, Chassis_Motor_203.Real_Speed, BR_Speed);
			Chassis_Motor_204.Target_Speed = Pid_Calc(&PID_Chassis_204, Chassis_Motor_204.Real_Speed, BL_Speed);

		//裁判系统接入
		if(referee_mode == ONLINE_REFEREE_MODE)
		{
			if(Chassis_Sta == NORMAL)
			{
			//底盘缓冲低于60
			if((power_heat_data.chassis_power_buffer < 60))
			{ 
				//功率控制率
				Power_Control_Rate = (power_heat_data.chassis_power_buffer / 120.0f) * 1.0f;//78
				
				Chassis_Motor_201.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_202.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_203.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_204.Target_Speed *= Power_Control_Rate;		
			}
			}
		}
		//裁判系统掉线时
		else
		{
				//控制最大电流
				Power_Control_Rate = Speed_Wheel_Max(Chassis_Motor_201.Target_Speed,Chassis_Motor_202.Target_Speed,Chassis_Motor_203.Target_Speed,Chassis_Motor_204.Target_Speed);

				Chassis_Motor_201.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_202.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_203.Target_Speed *= Power_Control_Rate;
				Chassis_Motor_204.Target_Speed *= Power_Control_Rate;						
		}
		

		
		xEventGroupSetBits(EventGroupHandler,CHASSIS_SIGNAL);	//底盘事件组置位
		vTaskDelayUntil( &xLastWakeTime,xFrequency );	//任务切换
	}
}