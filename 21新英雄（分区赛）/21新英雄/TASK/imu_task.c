#include "imu_task.h"

TaskHandle_t IMU_Task_Handler;
void imu_task(void *p_arg);
extern EventGroupHandle_t EventGroupHandler;

void imu_task_create()
{
		xTaskCreate((TaskFunction_t )imu_task,            //任务函数
							(const char*    )"imu_task",          //任务名称
							(uint16_t       )IMU_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )IMU_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&IMU_Task_Handler);   //任务句柄  
}
void imu_task(void *p_arg)
{
		static portTickType xLastWakeTime;  
    const portTickType xFrequency = pdMS_TO_TICKS(1);  
    // 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
    xLastWakeTime = xTaskGetTickCount(); 
		kalman imu_kalman;                                                                                                                             
		kalmanCreate(&imu_kalman,0.0005,0.01);  

	while(1)
	{
		IMU_Get_Data();
		mpu_data.gz = KalmanFilter(&imu_kalman,mpu_data.gz);  
		IMU_AHRS_UPDATE();
		IMU_ATTITUDE_UPDATE();

	//发射拨弹盘时间控制（遥控器）
	 if(shoot_poke_count0<=-1) 
	 {
		 shoot_poke_count0=-1;
	 }
	 else shoot_poke_count0--;
	 
	//发射拨弹盘时间控制（鼠标）
	 if(shoot_poke_count1 <= -1)
	 {
	 shoot_poke_count1=-1;
	 Poke_Mouse_Flag=1;
	 }
	 else shoot_poke_count1--;
	
	 //遥控器信号衰减
	 DR16_Flash_Val--;
	 if(DR16_Flash_Val < 0)
	 {
		 DR16_Flash_Val = -1;
	 }

	//裁判系统接收信号衰减
	if(Referee_Receive_Flag <= -1)
	{Referee_Receive_Flag = -1;}
	else Referee_Receive_Flag --;

		
//	Yaw_Angle_IMU += mpu_data.gz * 0.0003;  //陀螺仪数据积分
		
	vTaskDelayUntil( &xLastWakeTime,xFrequency );	//任务切换  
		
	}
}