#include "swmode_task.h"
TaskHandle_t SWMODE_Task_Handler;
void swmode_task(void *p_arg);

chassis_speed_mode_e chassis_speed_mode ;
gimbal_mode_e gimbal_mode = GIMBAL_FOLLOW_MODE;
control_mode_e control_mode = CONTROL_MANUAL_MODE ;
shoot_mode_e shoot_mode = SHOOT_CLOSE_MODE;
referee_mode_e referee_mode = ONLINE_REFEREE_MODE;

void swmode_task_create()
{
		xTaskCreate((TaskFunction_t )swmode_task,            //任务函数
							(const char*    )"swmode_task",          //任务名称
							(uint16_t       )SWMODE_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )SWMODE_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&SWMODE_Task_Handler);   //任务句柄  
}

void swmode_task(void *p_arg)
{
	while(1)
	{
		//控制模式选择
		if((DBUS.RC.Switch_Right != RC_SW_MID)&&(Right_Mouse_Press_Sta != 10))	//右拨杆不为中间，且右键未按下时
		{
			control_mode = CONTROL_MANUAL_MODE;	//手动模式
		}
		else
		{
			control_mode = CONTROL_AUTO_MODE;	//自动模式
		}
		
		//云台模式选择
		if(Rotate_Flag != 0)	//右边拨杆为下，或者大陀螺按键被按下
		{
			gimbal_mode = GIMBAL_ROTATE_MODE;	//大陀螺模式
		}
		else
		{
			gimbal_mode = GIMBAL_FOLLOW_MODE;	//跟随模式
		}
		
		
		//发射控制
		////遥控器模式
		//左拨杆状态更改
		if(DBUS.RC.Switch_Left != Switch_Left_Last)
		{
			//左拨杆为上
			if(DBUS.RC.Switch_Left == RC_SW_UP)
			{
				if(shoot_mode == SHOOT_OPEN_MODE)
				{
					shoot_mode = SHOOT_CLOSE_MODE;
					Shoot_Sta = 0;
				}
				
				else if (shoot_mode == SHOOT_CLOSE_MODE)
		    {
					shoot_mode = SHOOT_OPEN_MODE;
					Shoot_Sta = 1;
			  }		
			}
		}	
	 Switch_Left_Last = DBUS.RC.Switch_Left;
	
			
		//摩擦轮开关
			if(DBUS.PC.Keyboard & KEY_X)
			{
				shoot_mode = SHOOT_OPEN_MODE;
				
				Send_Once_Flag = 0;//初始化标志，按下发射键重新置一下图形界面

				Shoot_Sta = 1;
			}
		
		 else if(DBUS.PC.Keyboard & KEY_Z)
			{
				shoot_mode = SHOOT_CLOSE_MODE;
				Shoot_Sta = 0;
			}
			
			//裁判系统连接模式
			if(Referee_Receive_Flag < 0)
			{
				referee_mode = OFFLINE_REFEREE_MODE;
			}
			
			else
			{
				referee_mode = ONLINE_REFEREE_MODE;
			}
		
			
		vTaskDelay(2);	//任务切换
	}
}