#ifndef __MPUREPORT_H
#define __MPUREPORT_H
#include "sys.h"
#include "main.h"
void MPU_REPORT();
void mpu6050_send_data(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz);
void usart_report_imu(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz,short roll,short pitch,short yaw);
#endif