#include "assist.h"
/*shuimujieming
*@title：int16_t类型数据的绝对值函数
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/

int16_t calculate_abs(int16_t i)
{
	if(i<=0)
	{
		i=-i;
	}
	return i;
}
/*shuimujieming
*@title：求解平方根倒数
*@description：1/Sqrt(x)
*@param 1：	
*@param 2：	
*@return:：	
*/

float inv_sqrt(float x)
{
    float xhalf = 0.5f*x;		 // get half of x
    int i = *(int*)&x;       // get bits for floating value 此处使用的数据类型位int，而源程序为long。注意！
    i = 0x5f3759df - (i>>1); // gives initial guess y0
    x = *(float*)&i;         // convert bits back to float
    x = x*(1.5f-xhalf*x*x);  // Newton step, repeating increases accuracy
	
    return x;
}
	
