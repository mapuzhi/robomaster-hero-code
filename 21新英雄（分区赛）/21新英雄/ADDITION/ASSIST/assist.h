#ifndef __ASSIST_H
#define __ASSIST_H
#include "sys.h"
#include "math.h"
int16_t calculate_abs(int16_t i);
float inv_sqrt(float x);
#endif
