#include "iwdg.h"

/*
*prer 4
*rlr 625
*tout 4*2^4 * 625/40 =	1000
*/
void IWDG_Init(u8 prer , u16 rlr)
{
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);//开启开门狗通道
	IWDG_SetPrescaler(prer);//看门狗设置预分频系数
	IWDG_SetReload(rlr);//看门狗设置重装载值
	IWDG_ReloadCounter();//喂狗
	IWDG_Enable();//使能看门狗
}

void IWDG_Feed(void)
{
	IWDG_ReloadCounter();//喂狗
}



