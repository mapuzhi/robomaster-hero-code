#include "nuc.h"
int8_t USARTR3_Info[7]={0,0,0,0,0,0,0};  //串口接收数据数组
int8_t USARTT3_Info[9]={0xFF,0,0,0,0,2,0,0,0xFE};   //串口发送数据数组//0为0xFF,1-2P轴，3-4Y轴，5装甲颜色，6弹速，7击打模式，8为0xFe

float Yaw_NUC=0,Pitch_NUC=0;
int16_t Send_P_NUC,Send_Y_NUC;
short Hit_Sentry = 0;   //1—打哨兵，0—不打哨兵，正常自瞄


/**
  * @brief  初始化USART3
  * @param  void
  * @retval void
  * @notes  USART3_RX -----PD9    USART3_TX-----PD8      DMA1_Channe4_Stream1/3
  */
void NUC_Init()
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	USART_InitTypeDef  USART_InitStruct;
	DMA_InitTypeDef    DMA_InitStruct;
	NVIC_InitTypeDef   NVIC_InitStruct;
	
	/*enabe clocks*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_DMA1, ENABLE);//此处可以用或运算，因为是控制位的
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	/*open the alternative function*/
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);
	
	/*Configure PB10,PB11 as GPIO_InitStruct1 input*/
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_9|GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStruct);
	
	USART_InitStruct.USART_BaudRate            = 115200;
	USART_InitStruct.USART_WordLength          = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits            = USART_StopBits_1;
	USART_InitStruct.USART_Parity              = USART_Parity_No;
	USART_InitStruct.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART3, &USART_InitStruct);
	
	USART_ITConfig(USART3, USART_IT_IDLE,ENABLE);//空闲中断
	
	USART_Cmd(USART3, ENABLE);		
	USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE);
		
	NVIC_InitStruct.NVIC_IRQChannel                   =USART3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority =1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority        =0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                =ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel                   =DMA1_Stream3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority =1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority        =0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                =ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
	
/* -------------- Configure DMA  接收---------------------------------------*/

	USART_DMACmd(USART3,USART_DMAReq_Rx,ENABLE);
	
	DMA_Cmd(DMA1_Stream1,DISABLE);
	DMA_DeInit(DMA1_Stream1);
	
	DMA_InitStruct.DMA_Channel						= DMA_Channel_4;                    //通道配置（通道5）
	DMA_InitStruct.DMA_BufferSize					= 7;                                //传输数据数目                
	DMA_InitStruct.DMA_DIR								= DMA_DIR_PeripheralToMemory;       //数据传输方向（外设——存储器）                   
	DMA_InitStruct.DMA_FIFOMode						= ENABLE;                           //使能FIFO模式
	DMA_InitStruct.DMA_FIFOThreshold			= DMA_FIFOThreshold_1QuarterFull;   //阈值1/4      
	DMA_InitStruct.DMA_Memory0BaseAddr		= (uint32_t)USARTR3_Info;           //存储器数据地址
	DMA_InitStruct.DMA_MemoryBurst				= DMA_MemoryBurst_Single;           //存储器突发配置
	DMA_InitStruct.DMA_MemoryDataSize			= DMA_MemoryDataSize_Byte;          //存储器数据格式（字节）
	DMA_InitStruct.DMA_MemoryInc					= DMA_MemoryInc_Enable;             //内存地址递增
	DMA_InitStruct.DMA_Mode								= DMA_Mode_Circular;                //是否循环发送（循环，普通模式只能接受一次）
	DMA_InitStruct.DMA_PeripheralBaseAddr	= (uint32_t) & (USART3->DR);        //外设地址 
	DMA_InitStruct.DMA_PeripheralBurst		= DMA_PeripheralBurst_Single;       //外设突发配置         
	DMA_InitStruct.DMA_PeripheralDataSize	= DMA_PeripheralDataSize_Byte;      //外设数据格式
	DMA_InitStruct.DMA_PeripheralInc			= DMA_PeripheralInc_Disable;        //外设地址不递增
	DMA_InitStruct.DMA_Priority						= DMA_Priority_VeryHigh;            //优先级非常高      
	DMA_Init(DMA1_Stream1, &DMA_InitStruct);
	DMA_Cmd(DMA1_Stream1, ENABLE);

///*********************************************DMA发送************************************/

	USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
	DMA_Cmd(DMA1_Stream3, DISABLE);
  DMA_DeInit(DMA1_Stream3);                //为DMA配置通道
	DMA_InitStruct.DMA_Channel            = DMA_Channel_4;
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&(	USART3->DR);    	//起始地址
	DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t)&USARTT3_Info;      	//存储变量
	DMA_InitStruct.DMA_DIR                = DMA_DIR_MemoryToPeripheral;   	//传输方向
	DMA_InitStruct.DMA_BufferSize         = 9; 															//缓冲区长度
	DMA_InitStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;   		//外设递增模式
	DMA_InitStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;         	//内存递增模式
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;  	//DMA访问时每次操作的数据长度
	DMA_InitStruct.DMA_MemoryDataSize     = DMA_PeripheralDataSize_Byte; 		//DMA访问时每次操作的数据长度
	DMA_InitStruct.DMA_Mode               = DMA_Mode_Normal;              	//传输模式：连续不断
	DMA_InitStruct.DMA_Priority           = DMA_Priority_VeryHigh;        	//优先级别
  DMA_InitStruct.DMA_FIFOMode			      = DMA_FIFOMode_Enable;
	DMA_InitStruct.DMA_FIFOThreshold      = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitStruct.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream3, &DMA_InitStruct);
	
	DMA_ITConfig(DMA1_Stream3,DMA_IT_TC,ENABLE);
	
	delay_ms(3);
}


/**
  * @brief  TX2发送与接收中断
  * @param  void
  * @retval void
  * @notes  串口6打印初始化    USART7_TX-PE8      USART7_RX -----PE7
  */
void DMA1_Stream3_IRQHandler(void)
{
	if(DMA_GetITStatus(DMA1_Stream3, DMA_IT_TCIF3) == SET)
	{
		DMA_Cmd(DMA1_Stream3, DISABLE);
		DMA_SetCurrDataCounter(DMA1_Stream3, 9); 
	}
	DMA_ClearITPendingBit(DMA1_Stream3, DMA_IT_TCIF3);
	DMA_ClearFlag(DMA1_Stream3, DMA_FLAG_TCIF3);	
}


//接收
void USART3_IRQHandler(void)
{
	char num;
	if(USART_GetITStatus(USART3, USART_IT_IDLE) == SET)
	{
	 num = USART3->SR;
	 num = USART3->DR;                        //清除串口空闲中断
	 DMA_Cmd(DMA1_Stream1, DISABLE); 
	 DMA_SetCurrDataCounter(DMA1_Stream1, 7); //重新设置接收数据长度
	 DMA_Cmd(DMA1_Stream1, ENABLE);
	 NUC_Decode();                            //接收中断里对数据进行解码
	}	
}




void NUC_Decode(void)
{
 if((USARTR3_Info[0]==(int8_t)0xFF) && (USARTR3_Info[6]==(int8_t)0xFE)) //首尾校验
 {
	 int16_t *Pitch_Receive = (int16_t *)(USARTR3_Info+1);
	 int16_t *Yaw_Receive   = (int16_t *)(USARTR3_Info+3);

	 Pitch_NUC = *Pitch_Receive/100.0f;
	 Yaw_NUC   = -*Yaw_Receive/100.0f;
 }
 else//首尾校验不成功则设为0
 {
	 Yaw_NUC=0;
	 Pitch_NUC=0;
 }
}




/**
  * @brief  向TX2发送数据
  * @param  void
  * @retval void
  * @notes  串口6打印初始化    USART7_TX-PE8      USART7_RX -----PE7
  */
void USART3_Send_NUC(void)
{
	int16_t *c=(int16_t *)(USARTT3_Info+1);//P轴数据地址
	int16_t *d=(int16_t *)(USARTT3_Info+3);//Y轴数据地址
	Send_P_NUC=(int16_t)(imu.pit*100);
	Send_Y_NUC=(int16_t)imu.yaw;
  c[0]=Send_P_NUC;
	d[0]=Send_Y_NUC;

	USARTT3_Info[5] = Arm_Color;   //1-蓝  0-红  2-默认无信号  （指的是自己的装甲颜色）
  USARTT3_Info[6] = (int)(real_shoot_data.bullet_speed*10.0f);  //1-打哨兵  0-不打哨兵
  USARTT3_Info[7] = Hit_Sentry;  //1-打哨兵  0-不打哨兵
	
	
	DMA_Cmd(DMA1_Stream3,ENABLE);
}










