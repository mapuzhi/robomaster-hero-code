//#ifndef __NUC_H
//#define __NUC_H
//#include "main.h"

//void NUC_Init(void);



//void NUC_Decode(void);


//void USART3_Send_NUC(void);

//extern float Yaw_NUC,Pitch_NUC;

//extern int16_t Send_P_NUC,Send_Y_NUC;

//extern short Hit_Sentry;

//#endif

#ifndef __NUC_H
#define __NUC_H



#include "main.h"
void NUC_Init();

typedef  struct
{
	uint8_t header;
	float pit_offset;
	float yaw_offset;
	uint8_t buff_flag;
	uint8_t tail;
}NUC_Info_TypeDef;

void NUC_Send_Data();
extern NUC_Info_TypeDef NUC_Data;

#endif

