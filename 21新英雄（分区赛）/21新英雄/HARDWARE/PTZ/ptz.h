#ifndef _PTZ_H
#define _PTZ_H

#include "main.h"

typedef struct
{
	short Critical_LeftPitch_Mechanical_Angle;
	short Critical_RightPitch_Mechanical_Angle;
	short Critical_Yaw_Mechanical_Angle;
   
	short Initial_LeftPitch_Angle;
	short Initial_RightPitch_Angle;	
	short Initial_Yaw_Angle;
   
  short Pitch_Angle_Max, Yaw_Angle_Max;
  short Pitch_Angle_Min, Yaw_Angle_Min;
	
	short Calibrated_Flag;
	
}PTZ_TypeDef;


extern PTZ_TypeDef  PTZ_Init_Value;

extern float Yaw_Angle_IMU;

extern float Aim_Yaw_Angle,Aim_Pitch206_Angle,Aim_Pitch207_Angle;

  //206,上下400
extern short	LeftPitch_Angle_Max ;
extern short	LeftPitch_Angle_Min ;

  //207
extern short RightPitch_Angle_Max ;
extern short RightPitch_Angle_Min ;

extern int8_t ptz_init_sec; //云台初始化成功


//extern float Yaw_Zero_Angle;


void PTZ_Init(void);
void PTZ_Control(void);
void PTZ_Angle_Value_Init(void);
void PTZ_Angle_Init(void);

#endif



