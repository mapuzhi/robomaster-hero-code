#include "ptz.h"

/***************************************数据定义区***************************************/

/*云台初始化数据结构体*/
PTZ_TypeDef PTZ_Init_Value;

short Angle_Init_Num;
short Current_Yaw_Angle;
short Current_LeftPitch_Angle,Current_RightPitch_Angle;

short YAW_INITIAL_ANGLE = 5793;
short PITCH_LEFT_INITIAL_ANGLE  = 6110;    //206

short PITCH_RIGHT_INITIAL_ANGLE = 3500;    //207


  //206,上下400
short	LeftPitch_Angle_Max = 6340;
short	LeftPitch_Angle_Min = 5470;


float Yaw_Angle_IMU;//陀螺仪Yaw轴角度

float Aim_Yaw_Angle,Aim_Pitch206_Angle,Aim_Pitch207_Angle;//目标Yaw，Pitch轴角度


int8_t ptz_init_sec = 0;   //云台初始化成功标志：1-成功；0-失败



/***************************************函数处理区***************************************/


/**
  * @brief 云台初始化函数	
  * @param  
  * @retval 
  * @notes  
  */
void PTZ_Init(void)
{
		
	PTZ_Angle_Value_Init();
	delay_ms(10);//不能少的延时，避免真实角度无法获取到
	
	
	PTZ_Angle_Init();	//云台初始位置调整
	
	ptz_init_sec = 1;     //云台初始化成功 （等于0是失败）
	////当前Yaw角度获取陀螺仪数据
	Aim_Yaw_Angle = Yaw_Angle_IMU;
	////当前Pitch角度等于初始化结束后的电机真实机械角
	Aim_Pitch206_Angle = Pitch_Motor_206.Real_MechanicalAngle;

}



/**
  * @brief 云台角度值初始化函数
  * @param  
  * @retval 
  * @notes  
  */
void PTZ_Angle_Value_Init(void)
{
	
	PTZ_Init_Value.Initial_Yaw_Angle        = YAW_INITIAL_ANGLE;
	
	PTZ_Init_Value.Initial_LeftPitch_Angle  = PITCH_LEFT_INITIAL_ANGLE; //206
	
	PTZ_Init_Value.Initial_RightPitch_Angle = PITCH_RIGHT_INITIAL_ANGLE; //207


	////yaw越界处理	
	if(PTZ_Init_Value.Initial_Yaw_Angle + 4000 > 8191)
	{
		PTZ_Init_Value.Critical_Yaw_Mechanical_Angle = PTZ_Init_Value.Initial_Yaw_Angle + 4000 - 8192;	
	}		
	else 
	{
		PTZ_Init_Value.Critical_Yaw_Mechanical_Angle = PTZ_Init_Value.Initial_Yaw_Angle + 4000;	
	}
	////yaw初始化角度
	if(PTZ_Init_Value.Initial_Yaw_Angle < PTZ_Init_Value.Critical_Yaw_Mechanical_Angle)  
	{ 
		PTZ_Init_Value.Initial_Yaw_Angle += 8192;
	}
	
	
	////pitch越界处理
	////206
	if(PTZ_Init_Value.Initial_LeftPitch_Angle + 4000 > 8191)
	{
		PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle = PTZ_Init_Value.Initial_LeftPitch_Angle + 4000 - 8192;
	}		
	else
	{
		PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle = PTZ_Init_Value.Initial_LeftPitch_Angle + 4000;	
	}
	
	
	
	
	////pitch初始化角度
	////206
	if (PTZ_Init_Value.Initial_LeftPitch_Angle < PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle)  
	{ 
		PTZ_Init_Value.Initial_LeftPitch_Angle += 8192;
	}

	if(LeftPitch_Angle_Max < PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle)
	{
		LeftPitch_Angle_Max += 8192;
	}
	
	if(LeftPitch_Angle_Min < PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle)
	{
		LeftPitch_Angle_Min += 8192;
	}
	

	
}


/**
  * @brief 云台角度初始化函数
  * @param  
  * @retval 
  * @notes  
 */
void PTZ_Angle_Init(void)
{
	Current_Yaw_Angle        = Yaw_Motor_205.Real_MechanicalAngle;
	
	Current_LeftPitch_Angle  = Pitch_Motor_206.Real_MechanicalAngle;
	
	

	for(Angle_Init_Num=0; Angle_Init_Num<800; Angle_Init_Num++)
  {
		////205
		if ( Current_Yaw_Angle < (PTZ_Init_Value.Initial_Yaw_Angle -7))
			Current_Yaw_Angle += 8;
    if ( Current_Yaw_Angle > (PTZ_Init_Value.Initial_Yaw_Angle +7))
			Current_Yaw_Angle -= 8;
		
		PID_Yaw_205_Position_Reg.PIDout = Pid_Calc(&PID_Yaw_205_Position_Reg, Yaw_Motor_205.Real_MechanicalAngle, Current_Yaw_Angle);		 
  	Yaw_Motor_205.Target_Speed      = Pid_Calc(&PID_Yaw_205_Speed_Reg, Yaw_Motor_205.Real_Speed, PID_Yaw_205_Position_Reg.PIDout);

		
		////206
		if ( Current_LeftPitch_Angle < (PTZ_Init_Value.Initial_LeftPitch_Angle -7))
			Current_LeftPitch_Angle += 8;
    if ( Current_LeftPitch_Angle > (PTZ_Init_Value.Initial_LeftPitch_Angle +7))
			Current_LeftPitch_Angle -= 8;
		
		PID_Pitch_206_Position_Reg.PIDout = Pid_Calc(&PID_Pitch_206_Position_Reg, Pitch_Motor_206.Real_MechanicalAngle, Current_LeftPitch_Angle); 
  	Pitch_Motor_206.Target_Speed      = Pid_Calc(&PID_Pitch_206_Speed_Reg, Pitch_Motor_206.Real_Speed, PID_Pitch_206_Position_Reg.PIDout);
	

    CAN2_TX_PTZ(); 
				
    CAN1_TX_Armory();
		
	}
	
}


