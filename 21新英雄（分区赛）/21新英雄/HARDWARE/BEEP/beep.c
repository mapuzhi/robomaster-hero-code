#include "beep.h"

void Beep_InitConfig(void)
{
	GPIO_InitTypeDef   				GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef 	TIM_TimeBaseStructure;
	TIM_OCInitTypeDef 				TIM_OCInitStructure;
	
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, ENABLE);
	GPIO_PinAFConfig(GPIOH, GPIO_PinSource6, GPIO_AF_TIM12);
	
	GPIO_InitStructure.GPIO_Pin		= GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType	= GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd	= GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_100MHz;
	GPIO_Init(GPIOH ,&GPIO_InitStructure);
	
	TIM_TimeBaseStructure.TIM_ClockDivision	= TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode		= TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period				= 500-1; 
	TIM_TimeBaseStructure.TIM_Prescaler			= 84*5-1;  
	TIM_TimeBaseInit(TIM12, &TIM_TimeBaseStructure);
	
	TIM_OCInitStructure.TIM_OCMode			= TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OCPolarity	= TIM_OCPolarity_High; 
	TIM_OCInitStructure.TIM_OutputState	= TIM_OutputState_Enable;
	TIM_OC1Init(TIM12, &TIM_OCInitStructure);
	
	TIM_OC1PreloadConfig(TIM12, TIM_OCPreload_Enable);//使能预装载寄存器
	TIM_ARRPreloadConfig(TIM12, ENABLE); // ARPE使能
	TIM_Cmd(TIM12, ENABLE);
 	
	BEEP_OFF;
	
}



