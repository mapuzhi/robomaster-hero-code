#include "general.h"

/***************************************数据定义区***************************************/
Control_Info_TypeDef  Control_Info;

/*底盘各方向速度*/
short Chassis_Speed_Y,Chassis_Speed_Y_Max, Chassis_Accereration_Y, Chassis_Speed_X,Chassis_Speed_X_Max, Chassis_Accereration_X;

//Sta为0时则鼠标未按下，对于左键来是未点击，对于右键来说是未长按
short Left_Mouse_Press_Sta  = 0;  //鼠标左键点击状态  //待检测状态：0，一次按下：1，一次松开：2，二次按下：3，……工作状态：10
short Left_Mouse_Press_Tim=0;

short Right_Mouse_Press_Sta = 0;  //鼠标右键点击状态
short Right_Mouse_Press_Tim = 0;

int8_t flag_A,flag_B;//测试用


short Left_Sta_Last = 0;       //上一次鼠标左键的状态

u32 TimeTicks = 0;
int Flip_Aim_Speed;



short TX_Cap=0, Cap_Last_Sta=CAP_CLOSE;
short shift_count1, shift_count2, shift_count3;  //加速
short cap_open_count, cap_close_count;
short cap_count0=0, cap_count1=0;

int16_t Chassis_Sta = NORMAL;
int16_t Supercap_Sta = CAP_CLOSE;


short Swing_Derection_Flag=-1;
short c_count, v_count; //扭腰
short f_count,g_count;//陀螺
	
short b_count,ctrl_count; //打哨兵或者自瞄

float Yaw_Quick_Val;
short Rotate_Flag;
short Rotate_Button_Flag = 0;

/***************************************函数处理区***************************************/

/**
  * @brief  定时器7配置
  * @param  void
  * @retval void
  * @notes  void
 */
void Tick_TIM7_Init (int arr)   //溢出时间为10-6 *arr s
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef         NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);  							//使能TIM7时钟
	
  TIM_TimeBaseInitStructure.TIM_Period 				= arr; 									//自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler			= 840-1 ;  							//定时器分频 1MHz
	TIM_TimeBaseInitStructure.TIM_CounterMode		= TIM_CounterMode_Up; 	//向上计数模式
	TIM_TimeBaseInitStructure.TIM_ClockDivision	= TIM_CKD_DIV1;
	
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseInitStructure);									//初始化TIM7
	
	TIM_ITConfig(TIM7,TIM_IT_Update, ENABLE); 													//允许定时器7更新中断
	TIM_Cmd(TIM7,ENABLE);																							 	//使能定时器7

	NVIC_InitStructure.NVIC_IRQChannel										=TIM7_IRQn; 	//定时器7中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority	=5; 				//抢占优先级0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority					=0; 				//子优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd									=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
}

/**
  * @brief  定时器7中断处理函数
  * @param  void
  * @retval void
  * @notes  void
 */
void TIM7_IRQHandler(void)
{
   if(TIM_GetITStatus(TIM7, TIM_IT_Update == SET))
   {

		if(abs(mpu_data.gz) < 30)		//云台飘
		{mpu_data.gz = 0;}
		Yaw_Angle_IMU += mpu_data.gz * 0.0003;  //陀螺仪数据积分
		
     TIM_ClearITPendingBit (TIM7, TIM_IT_Update);
   }
	 
}
/**
  * @brief 键盘按键处理
  * @param  
  * @retval 
  * @notes  
  */
void Key_Control(void)
{
	 ////底盘
		if(DBUS.PC.Keyboard & KEY_W)
		{
			Chassis_Speed_Y -= Chassis_Accereration_Y;
			if(Chassis_Speed_Y > Chassis_Speed_Y_Max)	 Chassis_Speed_Y = Chassis_Speed_Y_Max;
			if(Chassis_Speed_Y < -Chassis_Speed_Y_Max)	 Chassis_Speed_Y = -Chassis_Speed_Y_Max;
		}
		else if(DBUS.PC.Keyboard & KEY_S)
		{
			Chassis_Speed_Y += Chassis_Accereration_Y;
			if(Chassis_Speed_Y > Chassis_Speed_Y_Max)	 Chassis_Speed_Y = Chassis_Speed_Y_Max;
			if(Chassis_Speed_Y < -Chassis_Speed_Y_Max)	 Chassis_Speed_Y = -Chassis_Speed_Y_Max;
		}
		else
		{
			//未按键时逐渐停下来
//			if (Chassis_Speed_Y > 0) Chassis_Speed_Y -= Chassis_Accereration_Y;
//			if (Chassis_Speed_Y < 0) Chassis_Speed_Y += Chassis_Accereration_Y;
			//立马停下来
			Chassis_Speed_Y = 0;
		}
		
		
		if(DBUS.PC.Keyboard & KEY_A)
		{
			Chassis_Speed_X += Chassis_Accereration_X;
			if(Chassis_Speed_X > Chassis_Speed_X_Max)	 Chassis_Speed_X = Chassis_Speed_X_Max;
			if(Chassis_Speed_X < -Chassis_Speed_X_Max)	 Chassis_Speed_X = -Chassis_Speed_X_Max;

		}
		else if(DBUS.PC.Keyboard & KEY_D)
		{
			Chassis_Speed_X -= Chassis_Accereration_X;
			if(Chassis_Speed_X > Chassis_Speed_X_Max)	 Chassis_Speed_X = Chassis_Speed_X_Max;
			if(Chassis_Speed_X < -Chassis_Speed_X_Max)	 Chassis_Speed_X = -Chassis_Speed_X_Max;
		}
		else
		{
				//未按键时逐渐停下来
//			if (Chassis_Speed_X > 0) Chassis_Speed_X -= Chassis_Accereration_Y;
//			if (Chassis_Speed_X < 0) Chassis_Speed_X += Chassis_Accereration_Y;
			//立马停下来			
			Chassis_Speed_X = 0;
		}
		
		
		
		////左右转向	 
		if(DBUS.PC.Keyboard & KEY_Q)
		{
      if( ((DBUS.PC.Keyboard & KEY_W)==1) || ((DBUS.PC.Keyboard & KEY_S)==1) )
         Yaw_Quick_Val = +1.5;//0.4;//0.25;
      else
	       Yaw_Quick_Val = +2.0;//0.6;//0.5;
		}
		else if(DBUS.PC.Keyboard & KEY_E)
		{
      if( ((DBUS.PC.Keyboard & KEY_W)==1) || ((DBUS.PC.Keyboard & KEY_S)==1) )
  		  Yaw_Quick_Val = -1.5;//0.4;//-0.25;
		  else
	      Yaw_Quick_Val = -2.0;//0.6;//-0.5;
		}
		else
		{
			Yaw_Quick_Val = 0.0;
		}
		

		
// //底盘速度切换
		if(Chassis_Sta == SUPPER)
		{
			Chassis_Accereration_Y = CHASSIS_ACC_HIGH;
			Chassis_Speed_Y_Max        = CHASSIS_SPEED_HIGH;
			
			Chassis_Accereration_X = CHASSIS_ACC_MID;
			Chassis_Speed_X_Max        = CHASSIS_SPEED_MID;
		}		
		else if(Chassis_Sta == NORMAL)
		{
			Chassis_Accereration_Y = CHASSIS_ACC_MID;
			Chassis_Accereration_X = CHASSIS_ACC_MID;
		
		  if( ((DBUS.PC.Keyboard & KEY_Q) == 1) || ((DBUS.PC.Keyboard & KEY_E) == 1) )
		  {
			Chassis_Speed_Y_Max = CHASSIS_SPEED_LOW * 0.2f;
		  }
      else
			{				
			Chassis_Speed_Y_Max = CHASSIS_SPEED_LOW;
			}
			
			Chassis_Speed_X_Max = CHASSIS_SPEED_LOW * 0.8;

		}		
				
    if((DBUS.PC.Keyboard & KEY_C) && (DBUS.PC.Keyboard & KEY_CTRL))
		{
			c_count = 0;
			if(v_count < 5) v_count++;
			else
			{
				Rotate_Flag = 0;
			}
		}

    //小陀螺开关
		else  if(DBUS.PC.Keyboard & KEY_C)
		{
			v_count = 0;
			if(c_count < 5) c_count++;
			else
			{
				if(Rotate_Button_Flag == 0)
				{
					if(Rotate_Flag == 0)
					{Rotate_Flag = 1;}
					else
					{
						Rotate_Flag ++;
						if(Rotate_Flag > 2)
						{
							Rotate_Flag = 1;
						}
					}
				}
				Rotate_Button_Flag = 1;
			}
		}

		else
		{
			Rotate_Button_Flag = 0;
		}
		
		
    if(DBUS.PC.Keyboard & KEY_F)
		{
			g_count = 0;
			if(f_count < 5) f_count++;
			else
			{
				//未使用
			}
			
		}			
    else if(DBUS.PC.Keyboard & KEY_G)
		{
			f_count = 0;
			if(g_count < 5) g_count++;
			else
			{
				//未使用
			}
			
		}		

		
		
		////B-打哨兵，Ctrl-正常自瞄
    if( DBUS.PC.Keyboard & KEY_B )
		{
			ctrl_count = 0;
			if(b_count < 3) b_count++;
			else
			{
				Hit_Sentry = 1;
			}
		}
    else if( DBUS.PC.Keyboard & KEY_CTRL )
		{
			b_count = 0;
			if(ctrl_count < 3) ctrl_count++;
			else
			{
				Hit_Sentry = 0;
			}
		}
		
		
}




/**
  * @brief 鼠标按键控制
  * @param  
  * @retval 
  * @notes  
  */
void Mouse_Control(void)
{
		{
				//鼠标左按键检测
			if(Left_Mouse_Press_Sta==0)//左键为待检测状态
			{
				if(DBUS.PC.Press_Left)//按下左键
				{
					Left_Mouse_Press_Tim=PRESS_TIM_COUNT;//按下左键后，赋值Tim为20
					Left_Mouse_Press_Sta=1;//按下一次左键
					flag_A=0;
				}
			}
			
			else if(Left_Mouse_Press_Sta==10)
			{
				if(!DBUS.PC.Press_Left)//左键未被按下，长按了之后松开了
				{
					Left_Mouse_Press_Sta=0;//按键设为待检测
					flag_A=0;
					flag_B=1;
				}			
			}
			else
			{
				Left_Mouse_Press_Tim--;//如果之前按了一下，20--
				if(Left_Mouse_Press_Sta==1)
				{
					if(!DBUS.PC.Press_Left) Left_Mouse_Press_Sta=2;//按了之后松开了变为2，没松开则还是1
				}
				if(Left_Mouse_Press_Sta==2)//如果是按下之后松开了
				{
					if(DBUS.PC.Press_Left)//松开了又按下了
					{
						flag_B=3;
						Left_Mouse_Press_Sta=10;//设为长按
					}
				}
				
				
				if(Left_Mouse_Press_Tim==0)//按键时间到了
				{
					if(DBUS.PC.Press_Left)//按键时间到了，还是处于按下状态。即长按
					{
						Left_Mouse_Press_Sta=10; //按键状态置为长按的10
						flag_A=1;//A为1
					}
					else Left_Mouse_Press_Sta=0;//按键时间到了之后松开了就等下就变为检测状态
				}
			}
			
			}
		
		
	////鼠标右按键检测
	if(Right_Mouse_Press_Sta == 0)
	{
		if(DBUS.PC.Press_Right)
		{
			Right_Mouse_Press_Tim = PRESS_TIM_COUNT;
			Right_Mouse_Press_Sta = 1;
		}
	}
	else if(Right_Mouse_Press_Sta == 10)
	{
		if(!DBUS.PC.Press_Right)
		{
			Right_Mouse_Press_Sta = 0;
		}			
	}
	else
	{
		Right_Mouse_Press_Tim--;
		if(Right_Mouse_Press_Sta == 1)
		{
			if(!DBUS.PC.Press_Right) Right_Mouse_Press_Sta = 2;
		}
		if(Right_Mouse_Press_Sta == 2)
		{
			if(DBUS.PC.Press_Right)
			{
				Right_Mouse_Press_Sta = 10;
			}
		}
		if(Right_Mouse_Press_Tim == 0)
		{
			if(DBUS.PC.Press_Right)
			{
				Right_Mouse_Press_Sta = 10; 
			}
			else Right_Mouse_Press_Sta = 0;
		}
	}
		
		
		
//	//通过判断上一次为未按下状态并且当前为按下状态来确定是一次单击，下一次单击前必须松开
//	if(Left_Sta_Last==0)
//	{
//		if(DBUS.PC.Press_Left)
//		{
//			Left_Mouse_Press_Sta = 1;
//		}
//		else
//		{
//			Left_Mouse_Press_Sta = 0;		
//		}
//	}
//	else
//	{
//		Left_Mouse_Press_Sta = 0;		
//	}
//	Left_Mouse_Press_Sta = DBUS.PC.Press_Left;
//	
//	//按下为1，松开为0
//	if(DBUS.PC.Press_Right)
//	{
//		Right_Mouse_Press_Sta = 1;
//	}
//	else
//	{
//		Right_Mouse_Press_Sta = 0;
//	}

}












