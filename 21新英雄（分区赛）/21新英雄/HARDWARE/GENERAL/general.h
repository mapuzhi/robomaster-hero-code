#ifndef _GENERAL_H
#define _GENERAL_H
#include "main.h"

extern float Yaw_Quick_Val;
extern short Rotate_Flag;

void Tick_TIM7_Init (int arr);
extern short Chassis_Speed_Y,Chassis_Speed_Y_Max, Chassis_Accereration_Y, Chassis_Speed_X,Chassis_Speed_X_Max, Chassis_Accereration_X;
void Key_Control(void);
void Mouse_Control(void);

typedef struct
{
	struct
	{
		int16_t Derection_X;            
		int16_t Derection_Y;
		int16_t Derection_Omega;
	}Chassis;
	
	struct 
	{
		float Pitch;
		float Yaw;
	}PTZ;
	
	struct 
	{
    int64_t Aim_Flip;
	}Flip;
	
}Control_Info_TypeDef;


extern Control_Info_TypeDef  Control_Info;
extern short Left_Mouse_Press_Sta;
extern short Right_Mouse_Press_Sta;
extern int16_t Chassis_Sta;
extern int16_t Supercap_Sta;
extern short TX_Cap;
extern int16_t Chassis_Sta;




#endif


