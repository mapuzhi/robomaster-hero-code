#include "limit.h"
/**
  * @brief 限位开关初始化
  * @param  
  * @retval 
  * @notes  
  */
void Limit_Init(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IN;//普通输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;//推挽输出
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
	GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;//上拉
	GPIO_Init(GPIOF, &GPIO_InitStruct);//初始化GPIO
	
	//弹丸碰到是0,没碰到是1
}


