#ifndef __LED_H
#define __LED_H

#include "main.h"

#define  LED_RED_ON      GPIO_ResetBits(GPIOE, GPIO_Pin_11)
#define  LED_RED_OFF     GPIO_SetBits(GPIOE, GPIO_Pin_11)
#define  LED_RED_TOGGLE  GPIO_ToggleBits(GPIOE, GPIO_Pin_11)

#define  LED_GREEN_ON        GPIO_ResetBits(GPIOF, GPIO_Pin_14)
#define  LED_GREEN_OFF       GPIO_SetBits(GPIOF, GPIO_Pin_14)
#define  LED_GREEN_TOGGLE    GPIO_ToggleBits(GPIOF, GPIO_Pin_14)



//������
#define LED_RUN_Port GPIOG
#define LED_RUN_Pin_1 GPIO_Pin_1
#define LED_RUN_Pin_2 GPIO_Pin_2
#define LED_RUN_Pin_3 GPIO_Pin_3
#define LED_RUN_Pin_4 GPIO_Pin_4
#define LED_RUN_Pin_5 GPIO_Pin_5
#define LED_RUN_Pin_6 GPIO_Pin_6
#define LED_RUN_Pin_7 GPIO_Pin_7
#define LED_RUN_Pin_8 GPIO_Pin_8


void LED_InitConfig(void);

#endif
