#include "led.h"

/*
LED初始化配置
*/
void LED_InitConfig(void)
{
    GPIO_InitTypeDef  GPIO_InitStruct;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);

    GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_14;       //绿灯对应IO口
    GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_OUT;     //普通输出模式
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;     //推挽输出
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz; //100MHz
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;      //上拉
    GPIO_Init(GPIOF, &GPIO_InitStruct);             //初始化GPIOF

	
	
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);

    GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_11;       //红灯对应IO口
    GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_OUT;     //普通输出模式
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;     //推挽输出
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz; //100MHz
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;      //上拉
    GPIO_Init(GPIOE, &GPIO_InitStruct);             //初始化GPIOE

		//红灯绿灯默认灭灯
    LED_GREEN_OFF;
    LED_RED_OFF;
		
	  RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOG,ENABLE);
	
		GPIO_InitStruct.GPIO_Pin=LED_RUN_Pin_1|LED_RUN_Pin_2|LED_RUN_Pin_3|LED_RUN_Pin_4|LED_RUN_Pin_5|LED_RUN_Pin_6|LED_RUN_Pin_7|LED_RUN_Pin_8;
		GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
		GPIO_InitStruct.GPIO_OType=GPIO_OType_PP;
		GPIO_InitStruct.GPIO_PuPd=GPIO_PuPd_UP;//上拉，低电平有效
		GPIO_InitStruct.GPIO_Speed=GPIO_Speed_100MHz;
		GPIO_Init(LED_RUN_Port,&GPIO_InitStruct);
		
		GPIO_ResetBits(LED_RUN_Port,LED_RUN_Pin_1|LED_RUN_Pin_2|LED_RUN_Pin_3|LED_RUN_Pin_4|LED_RUN_Pin_5|LED_RUN_Pin_6|LED_RUN_Pin_7|LED_RUN_Pin_8);
		

}






