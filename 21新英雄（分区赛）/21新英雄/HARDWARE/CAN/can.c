#include "can.h"

/***************************************数据定义区***************************************/
CAN_Data_TypeDef  
                //CAN1
								//底盘电机
								Chassis_Motor_201,Chassis_Motor_202,Chassis_Motor_203,Chassis_Motor_204,
								//拨弹盘电机及翻盖电机
                Flip_Motor_206,Poke_Motor_207,
								//CAN2
								//摩擦轮电机
                Shoot_Motor_201,Shoot_Motor_202,
								//云台电机
                Yaw_Motor_205,Pitch_Motor_206,Pitch_Motor_207;
				
float Supper_Cup_Remain_Voltage = 0.0f;                 //超级电容剩余电压
short Cap_Count = 1;

extern u8 DanLian_ManDan_Flag;

unsigned char CAN1_Tx_Message_Flag = 0;
unsigned char CAN2_Tx_Message_Flag = 0;

short Flip_Angle_Last;
unsigned char Flip_Angle_Init_Flag=0;
short Flip_Angle_Number;
void Flip_Angle_Record(void);
/***************************************函数处理区***************************************/
/**
  * @brief  CAN1初始化
  * @param  void
  * @retval void
  * @notes  STM32F427II  CAN1_RX(PD0)    CAN1_TX(PD1)
  */
void CAN1_InitConfig(void)
{
	GPIO_InitTypeDef      GPIO_InitStruct;
	NVIC_InitTypeDef      NVIC_InitStruct;
	CAN_InitTypeDef       CAN_InitStruct;
	CAN_FilterInitTypeDef CAN_FilterInitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource0, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource1, GPIO_AF_CAN1);
	
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOD,&GPIO_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel                    = CAN1_RX0_IRQn;             //注意与中断向量表区别  refer to stm32f4xx.h file
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority  = 4;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority         = 0;//0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                 = ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
	
  NVIC_InitStruct.NVIC_IRQChannel                    = CAN1_TX_IRQn;              //注意与中断向量表区别  refer to stm32f4xx.h file
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority  = 4;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority         = 0;//1;
	NVIC_InitStruct.NVIC_IRQChannelCmd                 = ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
  
  CAN_DeInit(CAN1);
  CAN_StructInit(&CAN_InitStruct);
    
	CAN_InitStruct.CAN_Prescaler = 3;                                               //CAN BaudRate 42/(1+9+4)/3=1Mbps  //分频系数(Fdiv)为3+1
	CAN_InitStruct.CAN_Mode      = CAN_Mode_Normal;                                 //模式设置 
	CAN_InitStruct.CAN_SJW       = CAN_SJW_1tq;                                     //重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位 CAN_SJW_1tq~CAN_SJW_4tq
	CAN_InitStruct.CAN_BS1       = CAN_BS1_9tq;                                     //Tbs1范围CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStruct.CAN_BS2       = CAN_BS2_4tq;                                     //Tbs2范围CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStruct.CAN_TTCM      = DISABLE;                                         //非时间触发通信模式
	CAN_InitStruct.CAN_ABOM      = DISABLE;                                         //软件自动离线管理	  
	CAN_InitStruct.CAN_AWUM      = DISABLE;                                         //睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStruct.CAN_NART      = DISABLE;	                                        //禁止报文自动传送
	CAN_InitStruct.CAN_RFLM      = DISABLE;                                         //报文不锁定,新的覆盖旧的
	CAN_InitStruct.CAN_TXFP      = ENABLE;	                                        //优先级由报文标识符决定
	CAN_Init(CAN1,&CAN_InitStruct);
	
	CAN_FilterInitStruct.CAN_FilterIdHigh         = 0x0000;                         //32位ID
	CAN_FilterInitStruct.CAN_FilterIdLow          = 0x0000;                         //32位ID
	CAN_FilterInitStruct.CAN_FilterMaskIdHigh     = 0x0000;
	CAN_FilterInitStruct.CAN_FilterMaskIdLow      = 0x0000;
	CAN_FilterInitStruct.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;               //过滤器0关联到FIFO0;
	CAN_FilterInitStruct.CAN_FilterNumber         = 0;                              //过滤器0,范围0---13
	CAN_FilterInitStruct.CAN_FilterMode           = CAN_FilterMode_IdMask;
	CAN_FilterInitStruct.CAN_FilterScale          = CAN_FilterScale_32bit;
	CAN_FilterInitStruct.CAN_FilterActivation     = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStruct);
	
  CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);                                          //FIFO0消息挂号中断允许，打开接收中断
	CAN_ITConfig(CAN1,CAN_IT_TME,ENABLE);                                           //打开发送中断
}


/**
  * @brief  CAN2初始化
  * @param  void
  * @retval void
  * @notes  STM32F427II  CAN2_RX(PD0)    CAN2_TX(PD1)
  */
void CAN2_InitConfig(void)
{
	GPIO_InitTypeDef      GPIO_InitStruct;
	NVIC_InitTypeDef      NVIC_InitStruct;
	CAN_InitTypeDef       CAN_InitStruct;
	CAN_FilterInitTypeDef CAN_FilterInitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE); //先CAN1 后CAN2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_CAN2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_CAN2);
	
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_12 | GPIO_Pin_13;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel                    = CAN2_RX0_IRQn;             //注意与中断向量表区别  refer to stm32f4xx.h file
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority  = 4;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority         = 0;//0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                 = ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
	
  NVIC_InitStruct.NVIC_IRQChannel                    = CAN2_TX_IRQn;              //注意与中断向量表区别  refer to stm32f4xx.h file
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority  = 4;//1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority         = 0;//0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                 = ENABLE ;
	NVIC_Init(&NVIC_InitStruct);
  
  CAN_DeInit(CAN2);
  CAN_StructInit(&CAN_InitStruct);
    
	CAN_InitStruct.CAN_Prescaler = 3;                                               //CAN BaudRate 42/(1+9+4)/3=1Mbps  //分频系数(Fdiv)为3+1
	CAN_InitStruct.CAN_Mode      = CAN_Mode_Normal;                                 //模式设置 
	CAN_InitStruct.CAN_SJW       = CAN_SJW_1tq;                                     //重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位 CAN_SJW_1tq~CAN_SJW_4tq
	CAN_InitStruct.CAN_BS1       = CAN_BS1_9tq;                                     //Tbs1范围CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStruct.CAN_BS2       = CAN_BS2_4tq;                                     //Tbs2范围CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStruct.CAN_TTCM      = DISABLE;                                         //非时间触发通信模式
	CAN_InitStruct.CAN_ABOM      = DISABLE;                                         //软件自动离线管理	  
	CAN_InitStruct.CAN_AWUM      = DISABLE;                                         //睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStruct.CAN_NART      = DISABLE;	                                        //禁止报文自动传送
	CAN_InitStruct.CAN_RFLM      = DISABLE;                                         //报文不锁定,新的覆盖旧的
	CAN_InitStruct.CAN_TXFP      = ENABLE;	                                        //优先级由报文标识符决定
	CAN_Init(CAN2,&CAN_InitStruct);
	
	CAN_FilterInitStruct.CAN_FilterIdHigh         = 0x0000;                         //32位ID
	CAN_FilterInitStruct.CAN_FilterIdLow          = 0x0000;                         //32位ID
	CAN_FilterInitStruct.CAN_FilterMaskIdHigh     = 0x0000;
	CAN_FilterInitStruct.CAN_FilterMaskIdLow      = 0x0000;
	CAN_FilterInitStruct.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;               //过滤器0关联到FIFO0;
	CAN_FilterInitStruct.CAN_FilterNumber         = 14;                              //过滤器0,范围0---13
	CAN_FilterInitStruct.CAN_FilterMode           = CAN_FilterMode_IdMask;
	CAN_FilterInitStruct.CAN_FilterScale          = CAN_FilterScale_32bit;
	CAN_FilterInitStruct.CAN_FilterActivation     = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStruct);
	
  CAN_ITConfig(CAN2,CAN_IT_FMP0,ENABLE);                                          //FIFO0消息挂号中断允许，打开接收中断
	CAN_ITConfig(CAN2,CAN_IT_TME,ENABLE);                                           //打开发送中断
}

void CAN_InitConfig(void)
{
	CAN1_InitConfig();                             //CAN1初始化
	CAN2_InitConfig();                             //CAN2初始化
}
/**
  * @brief  CAN1底盘发送函数
  * @param  给定电流，电流值范围：-16384~+16384
  * @retval void
  * @notes  底盘数据发送（广播ID 0x200）
  */
void CAN1_TX_Chassis(void)
{
	CanTxMsg CAN1_Tx_Message;

	CAN1_Tx_Message.IDE = CAN_ID_STD;                                               //标准帧
	CAN1_Tx_Message.RTR = CAN_RTR_DATA;                                             //数据帧
	CAN1_Tx_Message.DLC = 0x08;                                                     //帧长度为8
	CAN1_Tx_Message.StdId = CAN_TRANSMIT_201to204_ID;                               //帧ID为传入参数的CAN_ID

	if(DR16_Flash_Val > 0)
	{
	CAN1_Tx_Message.Data[0] = (Chassis_Motor_201.Target_Speed>>8)&0xff;             //201接收电流高8位
	CAN1_Tx_Message.Data[1] = (Chassis_Motor_201.Target_Speed)&0xff;                 //201接收电流低8位
	CAN1_Tx_Message.Data[2] = (Chassis_Motor_202.Target_Speed>>8)&0xff;             //202接收电流高8位
	CAN1_Tx_Message.Data[3] = (Chassis_Motor_202.Target_Speed)&0xff;                //202接收电流低8位
	CAN1_Tx_Message.Data[4] = (Chassis_Motor_203.Target_Speed>>8)&0xff;             //203接收电流高8位
	CAN1_Tx_Message.Data[5] = (Chassis_Motor_203.Target_Speed)&0xff;                 //203接收电流低8位
	CAN1_Tx_Message.Data[6] = (Chassis_Motor_204.Target_Speed>>8)&0xff;             //204接收电流高8位
	CAN1_Tx_Message.Data[7] = (Chassis_Motor_204.Target_Speed)&0xff;                 //204接收电流低8位
	}
	else
	{
	CAN1_Tx_Message.Data[0] = 0;             //201接收电流高8位
	CAN1_Tx_Message.Data[1] = 0;                 //201接收电流低8位
	CAN1_Tx_Message.Data[2] = 0;             //202接收电流高8位
	CAN1_Tx_Message.Data[3] = 0;                //202接收电流低8位
	CAN1_Tx_Message.Data[4] = 0;             //203接收电流高8位
	CAN1_Tx_Message.Data[5] = 0;                 //203接收电流低8位
	CAN1_Tx_Message.Data[6] = 0;             //204接收电流高8位
	CAN1_Tx_Message.Data[7] = 0;                 //204接收电流低8位	
	}
	CAN1_Tx_Message_Flag = 0;
	CAN_Transmit(CAN1,&CAN1_Tx_Message);
	while(CAN1_Tx_Message_Flag == 0); 
}


/**
 * @Function : CAN1 弹仓以及Yaw云台
 * @Input    : void
 * @Output   : void
 * @Notes    : ID 0x200
 * @Copyright: Aword
 **/
void CAN1_TX_Armory(void)
{
	CanTxMsg CAN1_Tx_Message;

	CAN1_Tx_Message.IDE = CAN_ID_STD;                                               //标准帧
	CAN1_Tx_Message.RTR = CAN_RTR_DATA;                                             //数据帧
	CAN1_Tx_Message.DLC = 0x08;                                                     //帧长度为8
	CAN1_Tx_Message.StdId = CAN_TRANSMIT_205to208_ID;                               //帧ID为传入参数的CAN_ID

	if(DR16_Flash_Val > 0)
	{
	CAN1_Tx_Message.Data[0] = (Yaw_Motor_205.Target_Speed>>8)&0xff;             //205接收电流高8位
	CAN1_Tx_Message.Data[1] = (Yaw_Motor_205.Target_Speed)&0xff;                //205接收电流低8位
	CAN1_Tx_Message.Data[2] = (Flip_Motor_206.Target_Speed>>8)&0xff;             //206接收电流高8位
	CAN1_Tx_Message.Data[3] = (Flip_Motor_206.Target_Speed)&0xff;                //206接收电流低8位
	CAN1_Tx_Message.Data[4] = (Poke_Motor_207.Target_Speed>>8)&0xff;          
	CAN1_Tx_Message.Data[5] = (Poke_Motor_207.Target_Speed)&0xff;           
	CAN1_Tx_Message.Data[6] = 0x00;           
	CAN1_Tx_Message.Data[7] = 0x00;              
	}
	else
	{
	CAN1_Tx_Message.Data[0] = 0;             //205接收电流高8位
	CAN1_Tx_Message.Data[1] = 0;                //205接收电流低8位
	CAN1_Tx_Message.Data[2] = 0;             //206接收电流高8位
	CAN1_Tx_Message.Data[3] = 0;                //206接收电流低8位
	CAN1_Tx_Message.Data[4] = 0;          
	CAN1_Tx_Message.Data[5] = 0;           
	CAN1_Tx_Message.Data[6] = 0;           
	CAN1_Tx_Message.Data[7] = 0; 		
	}
	CAN1_Tx_Message_Flag = 0;
	CAN_Transmit(CAN1,&CAN1_Tx_Message);
	while(CAN1_Tx_Message_Flag == 0); 
}



void CAN1_TX_Cup(void)
{
	CanTxMsg CAN1_Tx_Message;

	CAN1_Tx_Message.IDE   = CAN_ID_STD;                                               //标准帧
	CAN1_Tx_Message.RTR   = CAN_RTR_DATA;                                             //数据帧
	CAN1_Tx_Message.DLC   = 0x08;                                                     //帧长度为8
	CAN1_Tx_Message.StdId = CAN_TR_CAP_ID;                               //帧ID为传入参数的CAN_ID
	
	
 if( TX_Cap==1) //放电
 {
	CAN1_Tx_Message.Data[0] = 1;                                        //205接收电流高8位
 }
 else if(TX_Cap==0)  //充电
 {
	CAN1_Tx_Message.Data[0] = 0;                                       //205接收电流高8位
 }


	CAN1_Tx_Message_Flag = 0;
	CAN_Transmit(CAN1,&CAN1_Tx_Message);

	while(CAN1_Tx_Message_Flag == 0); 
}



/**
	* @brief CAN2 云台发送函数
  * @param  
  * @retval 
  * @notes  
  */
void CAN2_TX_PTZ(void)
{
	CanTxMsg CAN2_Tx_Message;

	CAN2_Tx_Message.IDE = CAN_ID_STD;                                               //标准帧
	CAN2_Tx_Message.RTR = CAN_RTR_DATA;                                             //数据帧
	CAN2_Tx_Message.DLC = 0x08;                                                     //帧长度为8
	CAN2_Tx_Message.StdId = CAN_TRANSMIT_205to208_ID;                               //帧ID为传入参数的CAN_ID

	if(DR16_Flash_Val > 0)
	{
	CAN2_Tx_Message.Data[0] = 0;                                                  //205接收电流高8位
	CAN2_Tx_Message.Data[1] = 0;                                                  //205接收电流低8位
	CAN2_Tx_Message.Data[2] = (Pitch_Motor_206.Target_Speed>>8)&0xff;             //206接收电流高8位
	CAN2_Tx_Message.Data[3] = (Pitch_Motor_206.Target_Speed)&0xff;                //206接收电流低8位
	CAN2_Tx_Message.Data[4] = (Pitch_Motor_207.Target_Speed>>8)&0xff;             //207接收电流高8位
	CAN2_Tx_Message.Data[5] = (Pitch_Motor_207.Target_Speed)&0xff;                //207接收电流低8位
	CAN2_Tx_Message.Data[6] = 0x00;                                               //208接收电流高8位
	CAN2_Tx_Message.Data[7] = 0x00;                                               //208接收电流低8位
	}
	else
	{
	CAN2_Tx_Message.Data[0] = 0;                                                  //205接收电流高8位
	CAN2_Tx_Message.Data[1] = 0;                                                  //205接收电流低8位
	CAN2_Tx_Message.Data[2] = 0;             //206接收电流高8位
	CAN2_Tx_Message.Data[3] = 0;                //206接收电流低8位
	CAN2_Tx_Message.Data[4] = 0;             //207接收电流高8位
	CAN2_Tx_Message.Data[5] = 0;                //207接收电流低8位
	CAN2_Tx_Message.Data[6] = 0;                                               //208接收电流高8位
	CAN2_Tx_Message.Data[7] = 0;                                               //208接收电流低8位		
	}
	CAN2_Tx_Message_Flag = 0;
	CAN_Transmit(CAN2,&CAN2_Tx_Message);
	while(CAN2_Tx_Message_Flag == 0); 
}



/**
	* @brief  CAN2发射发送函数
  * @param  给定电流，电流值范围：-16384~+16384
  * @retval void
  * @notes  底盘数据发送（广播ID 0x200）
  */
void CAN2_TX_Shoot(void)
{
	CanTxMsg CAN2_Tx_Message;

	CAN2_Tx_Message.IDE = CAN_ID_STD;                                               //标准帧
	CAN2_Tx_Message.RTR = CAN_RTR_DATA;                                             //数据帧
	CAN2_Tx_Message.DLC = 0x08;                                                     //帧长度为8
	CAN2_Tx_Message.StdId = CAN_TRANSMIT_201to204_ID;                               //帧ID为传入参数的CAN_ID
	if(DR16_Flash_Val > 0)
	{
	CAN2_Tx_Message.Data[0] = (Shoot_Motor_201.Target_Speed>>8)&0xff;             //201接收电流高8位
	CAN2_Tx_Message.Data[1] = (Shoot_Motor_201.Target_Speed)&0xff;                //201接收电流低8位
	CAN2_Tx_Message.Data[2] = (Shoot_Motor_202.Target_Speed>>8)&0xff;             //202接收电流高8位
	CAN2_Tx_Message.Data[3] = (Shoot_Motor_202.Target_Speed)&0xff;                //202接收电流低8位
	CAN2_Tx_Message.Data[4] = 0;        
	CAN2_Tx_Message.Data[5] = 0;          
	CAN2_Tx_Message.Data[6] = 0;           
	CAN2_Tx_Message.Data[7] = 0;          
	}
	else
	{
	CAN2_Tx_Message.Data[0] = 0;             //201接收电流高8位
	CAN2_Tx_Message.Data[1] = 0;                //201接收电流低8位
	CAN2_Tx_Message.Data[2] = 0;             //202接收电流高8位
	CAN2_Tx_Message.Data[3] = 0;                //202接收电流低8位
	CAN2_Tx_Message.Data[4] = 0;        
	CAN2_Tx_Message.Data[5] = 0;          
	CAN2_Tx_Message.Data[6] = 0;           
	CAN2_Tx_Message.Data[7] = 0;     
	}
	CAN2_Tx_Message_Flag = 0;
	CAN_Transmit(CAN2,&CAN2_Tx_Message);
	while(CAN2_Tx_Message_Flag == 0); 
}




/**
  * @brief  CAN1发送中断函数
  * @param  void
  * @retval void
  * @notes  void
  */
void CAN1_TX_IRQHandler(void)
{  
  if (CAN_GetITStatus (CAN1,CAN_IT_TME)!=RESET)                                 //if transmit mailbox is empty     
	{
		CAN1_Tx_Message_Flag=1;
		CAN_ClearITPendingBit(CAN1,CAN_IT_TME);    
	}
}



/**
  * @brief  CAN2发送中断函数
  * @param  void
  * @retval void
  * @notes  void
  */
void CAN2_TX_IRQHandler(void)
{  
  if (CAN_GetITStatus (CAN2,CAN_IT_TME)!=RESET)                                 //if transmit mailbox is empty     
	{
		CAN2_Tx_Message_Flag=1;
		CAN_ClearITPendingBit(CAN2,CAN_IT_TME);    
	}
}




/**
  * @brief  CAN1接收中断函数
  * @param  void
  * @retval void
  * @notes  void
  */
void CAN1_RX0_IRQHandler(void)                                                  //中断向量表参考“startup_stm32f40_41xxx.s”
{   
	CanRxMsg CAN1_Rx_Message;	
	if (CAN_GetITStatus(CAN1,CAN_IT_FMP0)!= RESET)
	{
		CAN_Receive(CAN1,CAN_FIFO0,&CAN1_Rx_Message);
		if ( (CAN1_Rx_Message.IDE == CAN_Id_Standard) && (CAN1_Rx_Message.RTR == CAN_RTR_Data) && (CAN1_Rx_Message.DLC == 8) )//标准帧、数据帧、数据长度为8字节
		{
			switch (CAN1_Rx_Message.StdId)
			{
				//超级电容
				case CAN1_FEEDBACK_ID_SUPPER_CUP:                                                                     
				{
					Supper_Cup_Remain_Voltage=char_to_float(CAN1_Rx_Message.Data[0],CAN1_Rx_Message.Data[1],CAN1_Rx_Message.Data[2],CAN1_Rx_Message.Data[3]);   //超级电容

					Cap_Count=200;
				}break ;

				//底盘
				case CAN1_FEEDBACK_ID_CHASSIS_FRONTLEFT:                                                                      //底盘电机--前左-->0x201
				{
					Chassis_Motor_201.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);              //机械角
					Chassis_Motor_201.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);              //转速
				}break ;
				
				case CAN1_FEEDBACK_ID_CHASSIS_FRONTRIGHT:                                                                     //底盘电机--前右-->0x202
				{
					Chassis_Motor_202.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);              //机械角
					Chassis_Motor_202.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);              //转速
				}break ;
				
				case CAN1_FEEDBACK_ID_CHASSIS_BACKRIGHT:                                                                      //底盘电机--后右-->0x203
				{
					Chassis_Motor_203.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);              //机械角
					Chassis_Motor_203.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);              //转速
				}break ;
				
				case CAN1_FEEDBACK_ID_CHASSIS_BACKLEFT:                                                                       //底盘电机--后左-->0x204
				{
					Chassis_Motor_204.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);              //机械角
					Chassis_Motor_204.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);              //转速
				}break ;
								
				//弹仓
				case CAN1_FEEDBACK_ID_ARMORY_POKE_MOTOR:                                                                      //拨盘电机-->0x207
				{
					Poke_Motor_207.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);                 //机械角
					Poke_Motor_207.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);                 //转速	
					Poke_Motor_MechanicalAngle_Judge();
					
//					if(DanLian_ManDan_Flag == 0)
//					{
//						Poke_Motor_MechanicalAngle_Judge();
//					}
					
				}break ;
				
				case CAN1_FEEDBACK_ID_ARMORY_FLIP_MOTOR:                                                                      //翻盖电机-->0x206
				{
					Flip_Motor_206.Real_MechanicalAngle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);                 //机械角
					Flip_Motor_206.Real_Speed          =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);                 //转速
				}break ;
				
				//云台
				case CAN1_FEEDBACK_ID_PTZ_YAW:
				{
					Yaw_Motor_205.Origin_Mechanical_Angle=(CAN1_Rx_Message.Data[0]<<8)|(CAN1_Rx_Message.Data[1]);              //机械角
					Yaw_Motor_205.Real_Speed             =(CAN1_Rx_Message.Data[2]<<8)|(CAN1_Rx_Message.Data[3]);              //转速
					if (Yaw_Motor_205.Origin_Mechanical_Angle < PTZ_Init_Value.Critical_Yaw_Mechanical_Angle)
          {
            Yaw_Motor_205.Real_MechanicalAngle =Yaw_Motor_205.Origin_Mechanical_Angle+8192;
          }
					else Yaw_Motor_205.Real_MechanicalAngle =Yaw_Motor_205.Origin_Mechanical_Angle;
				}break;
				
				
				default:
				{
					break;
				}   
			}
		}
		CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);		
	}
}




/**
  * @brief  CAN2接收中断函数
  * @param  void
  * @retval void
  * @notes  void
  */
void CAN2_RX0_IRQHandler(void)                                                  //中断向量表参考“startup_stm32f40_41xxx.s”
{   
	CanRxMsg CAN2_Rx_Message;	
	if (CAN_GetITStatus(CAN2,CAN_IT_FMP0)!= RESET)
	{
		CAN_Receive(CAN2,CAN_FIFO0,&CAN2_Rx_Message);
		if ( (CAN2_Rx_Message.IDE == CAN_Id_Standard) && (CAN2_Rx_Message.RTR == CAN_RTR_Data) && (CAN2_Rx_Message.DLC == 8) )//标准帧、数据帧、数据长度为8字节
		{
			switch (CAN2_Rx_Message.StdId)
			{
				
				//云台
				//206
				case CAN2_FEEDBACK_ID_PTZ_LEFTPITCH:                                                                     //云台电机--pitch-->0x206
				{
					Pitch_Motor_206.Origin_Mechanical_Angle=(CAN2_Rx_Message.Data[0]<<8)|(CAN2_Rx_Message.Data[1]);           //机械角
					Pitch_Motor_206.Real_Speed             =(CAN2_Rx_Message.Data[2]<<8)|(CAN2_Rx_Message.Data[3]);					 //转速
					
					if(Pitch_Motor_206.Origin_Mechanical_Angle < PTZ_Init_Value.Critical_LeftPitch_Mechanical_Angle)
          {
            Pitch_Motor_206.Real_MechanicalAngle =Pitch_Motor_206.Origin_Mechanical_Angle+8192;
          }
					else 
						Pitch_Motor_206.Real_MechanicalAngle =Pitch_Motor_206.Origin_Mechanical_Angle;
				}break ;
				
				//207
				case CAN2_FEEDBACK_ID_PTZ_RIGHTPITCH:                                                                     //云台电机--pitch-->0x207
				{
					Pitch_Motor_207.Origin_Mechanical_Angle=(CAN2_Rx_Message.Data[0]<<8)|(CAN2_Rx_Message.Data[1]);           //机械角
					Pitch_Motor_207.Real_Speed             =(CAN2_Rx_Message.Data[2]<<8)|(CAN2_Rx_Message.Data[3]);              //转速
					
					if(Pitch_Motor_207.Origin_Mechanical_Angle < PTZ_Init_Value.Critical_RightPitch_Mechanical_Angle)
          {
            Pitch_Motor_207.Real_MechanicalAngle = Pitch_Motor_207.Origin_Mechanical_Angle+8192;
          }
					else 
						Pitch_Motor_207.Real_MechanicalAngle = Pitch_Motor_207.Origin_Mechanical_Angle;
								
				}break ;
				
				
				//发射
				case CAN2_FEEDBACK_ID_SHOOT_LEFT:                                                                      //发射电机-->0x201
				{
					Shoot_Motor_201.Real_MechanicalAngle=(CAN2_Rx_Message.Data[0]<<8)|(CAN2_Rx_Message.Data[1]);              //机械角
					Shoot_Motor_201.Real_Speed          =(CAN2_Rx_Message.Data[2]<<8)|(CAN2_Rx_Message.Data[3]);              //转速
				}break ;
				
				
				case CAN2_FEEDBACK_ID_SHOOT_RIGHT:                                                                     //发射电机-->0x202
				{
					Shoot_Motor_202.Real_MechanicalAngle=(CAN2_Rx_Message.Data[0]<<8)|(CAN2_Rx_Message.Data[1]);              //机械角
					Shoot_Motor_202.Real_Speed          =(CAN2_Rx_Message.Data[2]<<8)|(CAN2_Rx_Message.Data[3]);              //转速
				}break ;
				
				default:
				{
					break; 
				}
					
			}
		}
		CAN_ClearITPendingBit(CAN2, CAN_IT_FMP0);		
	}
}



////////  记圈数处理
int8_t Poke_LastAngle_Init=0;
short Poke_Last_Angle;
short Poke_Motor_Number=0;
int64_t Poke_Down_Use_Angle;

void Poke_Motor_MechanicalAngle_Judge(void)
{
	
	if(Poke_LastAngle_Init==0)
	{
		Poke_Last_Angle=Poke_Motor_207.Real_MechanicalAngle;
		Poke_LastAngle_Init=1;
	}
	
	else
	{
		if(Poke_Motor_207.Real_MechanicalAngle-Poke_Last_Angle>4000)
		{
			Poke_Motor_Number--;		
		}
		if(Poke_Motor_207.Real_MechanicalAngle-Poke_Last_Angle<-4000)
		{
			Poke_Motor_Number++;
		}
		Poke_Down_Use_Angle=Poke_Motor_207.Real_MechanicalAngle+8192*Poke_Motor_Number;
		
		Poke_Last_Angle=Poke_Motor_207.Real_MechanicalAngle;
  }
	
	
}





float char_to_float(uint8_t byte0,uint8_t byte1,uint8_t byte2,uint8_t byte3)
{
	union 
	{
		float f;
		char byte[4];
	}value;
	value.byte[0]=byte0;
	value.byte[1]=byte1;
	value.byte[2]=byte2;
	value.byte[3]=byte3;	
	
	return value.f;
}








