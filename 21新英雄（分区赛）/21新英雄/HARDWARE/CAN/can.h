#ifndef _CAN_H
#define _CAN_H
#include "main.h"

typedef struct
{
	short Real_Speed;
	short Real_MechanicalAngle;
	short Origin_Mechanical_Angle;
	short Target_Speed;
	short Target_Mechanical_Angle;
}CAN_Data_TypeDef;

extern  CAN_Data_TypeDef   
	                      //CAN1
                        Chassis_Motor_201,Chassis_Motor_202,Chassis_Motor_203,Chassis_Motor_204,//���̵��
                        Flip_Motor_206,//����
                        Poke_Motor_207,//������
												Supper_Cup_208,//��������
                        //CAN2
	                      Shoot_Motor_201,Shoot_Motor_202,//����Ħ����3508
                        Yaw_Motor_205,Pitch_Motor_206,Pitch_Motor_207;//��̨���
												
												

extern float Supper_Cup_Remain_Voltage;
extern short Cap_Count;


extern short Flip_Angle_Number;
extern short Flip_Angle_Last;

//CAN�߱�ʶ��
#define CAN_TRANSMIT_201to204_ID             0x200
#define CAN_TRANSMIT_205to208_ID             0x1FF 
#define CAN_TR_CAP_ID                        0x427

//����
#define CAN1_FEEDBACK_ID_CHASSIS_FRONTLEFT   0x201
#define CAN1_FEEDBACK_ID_CHASSIS_FRONTRIGHT  0x202
#define CAN1_FEEDBACK_ID_CHASSIS_BACKRIGHT   0x203
#define CAN1_FEEDBACK_ID_CHASSIS_BACKLEFT    0x204

//����
#define CAN1_FEEDBACK_ID_ARMORY_FLIP_MOTOR   0x206  
#define CAN1_FEEDBACK_ID_ARMORY_POKE_MOTOR   0x207 

//��������
#define CAN1_FEEDBACK_ID_SUPPER_CUP          0x300


//����
#define CAN2_FEEDBACK_ID_SHOOT_LEFT          0x201
#define CAN2_FEEDBACK_ID_SHOOT_RIGHT         0x202


//��̨
#define CAN1_FEEDBACK_ID_PTZ_YAW             0x205
#define CAN2_FEEDBACK_ID_PTZ_LEFTPITCH       0x206
#define CAN2_FEEDBACK_ID_PTZ_RIGHTPITCH      0x207


extern int64_t Poke_Down_Use_Angle;



void CAN1_InitConfig(void);
void CAN2_InitConfig(void);
void CAN_InitConfig(void);


void CAN1_TX_Chassis(void);
void CAN1_TX_Armory(void);
void CAN2_TX_PTZ(void);
void CAN2_TX_Shoot(void);
void CAN1_TX_Cup(void);

void Poke_Motor_MechanicalAngle_Judge(void);

	

float char_to_float(uint8_t byte0,uint8_t byte1,uint8_t byte2,uint8_t byte3);

#endif













