#include "tim.h"

uint32_t TIM_TICK_COUNT=0;//时钟计数值

/*shuimujieming
*@title：计时器时钟TIM6初始化
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/

void TIM6_Init(int arr)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef         NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);  		//使能TIM6时钟

  TIM_TimeBaseInitStructure.TIM_Period 				= arr; 									//自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler			=	84 -1; 								//定时器分频
	TIM_TimeBaseInitStructure.TIM_CounterMode		=	TIM_CounterMode_Up; 	//向上计数模式
	TIM_TimeBaseInitStructure.TIM_ClockDivision	=	TIM_CKD_DIV1; 
	
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseInitStructure);									//初始化TIM6
	
	TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE); 													//允许定时器6更新中断
	TIM_Cmd(TIM6,ENABLE);                                               //使能定时器6

	NVIC_InitStructure.NVIC_IRQChannel										=	TIM6_DAC_IRQn; 	//定时器6中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority	=	4; 					//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority					=	0;						//子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
	
}
/*shuimujieming
*@title：定时器中断处理函数
*@description：
*@param 1：	
*@param 2：	
*@return:：	
*/

void TIM6_DAC_IRQHandler()
{
	if(TIM_GetITStatus(TIM6,TIM_IT_Update) == SET)
	{
		TIM_TICK_COUNT++;					//不做溢出处理
		
		if(abs(mpu_data.gz) < 20)		//云台飘
		{mpu_data.gz = 0;}
		
		
		Yaw_Angle_IMU += mpu_data.gz * 0.0003;  //陀螺仪数据积分
	}
	TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
}




