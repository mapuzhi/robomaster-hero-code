 #include "Referee_System.h"
 
  RefereeInfo_TypeDef             RefereeInfor;
  ext_game_status_t                    game_information;	          //0x0001
  ext_game_result_t                   game_result_data;	       	    //0x0002
  ext_game_robot_HP_t	              game_robot_HP;             	    //0x0003
	ext_event_data_t               event_data;                  	    //0x0101
	ext_supply_projectile_action_t      supply_projectile_action;     //0x0102
	ext_referee_warning_t               referee_warning;              //0x0104
	ext_game_robot_status_t             game_robot_state;             //0x0201
	ext_power_heat_data_t 	        		power_heat_data;	        		//0x0202
	ext_game_robot_pos_t		        		game_robot_pos;	              //0x0203
	ext_buff_t                          buff_data;                    //0x0204
	aerial_robot_energy_t           		aerial_robot_energy;          //0x0205    
	ext_robot_hurt_t                    robot_hurt_data;              //0x0206
	ext_shoot_data_t                    real_shoot_data;              //0x0207
  ext_bullet_remaining_t              bullet_remaining;             //0x0208
	

uint8_t seq=0;
u8 Arm_Color=2;       //装甲颜色，；2-默认无信号   //0是蓝色，1是红色
 
//利用串口空闲中断+DMA接收不定长数据

#define bool int
#define FALSE 0
#define TRUE  1

#define JUDGE_MAX_LENGTH  200    //200

uint8_t JUDGE_RX_BUFF1[JUDGE_MAX_LENGTH];
uint8_t JUDGE_RX_BUFF2[JUDGE_MAX_LENGTH];

uint16_t JUDGE_BUFF1_RX_NUMBER=0;
uint16_t JUDGE_BUFF2_RX_NUMBER=0;


/*空闲BUFF枚举*/
typedef enum{
      BUFF_NO1=0,
      BUFF_NO2=1
}BUFF_NO;

BUFF_NO  Free_BUFF_NO;   //空闲BUFF

bool BUFF_OK;

DMA_InitTypeDef   dma;

short Referee_Receive_Flag = 0;
 
/***********************Function definition***********************/ 

float bit8TObit32(uint8_t *change_info)
{
	union
	{
    float f;
		char  byte[4];
	}u32val;
    u32val.byte[0]=change_info[0];
    u32val.byte[1]=change_info[1];
    u32val.byte[2]=change_info[2];
    u32val.byte[3]=change_info[3];
	return u32val.f;
} 

 char bit32TObit8(int index_need,int bit32)
{
	union
	{
    float  f;
		char  byte[4];
	}u32val;
    u32val.f = bit32;
	return u32val.byte[index_need];
}

int16_t bit8TObit16(uint8_t *change_info)
{
	union
	{
    int16_t f;
		char  byte[2];
	}u16val;
    u16val.byte[0]=change_info[0];
    u16val.byte[1]=change_info[1];
	return u16val.f;
}

 char bit16TObit8(int index_need,int bit16)
{
	union
	{
    float  f;
		char  byte[2];
	}u16val;
    u16val.f = bit16;
	return u16val.byte[index_need];
}




/**
 * @Function : RefereeInfo Decode
 * @Input    : void
 * @Output   : void
 * @Notes    : void
 * @Copyright: Aword
 **/
void RefereeInfo_Decode(uint8_t *UsartRx_Info)
{
	RefereeInfor.CmdID = bit8TObit16(&UsartRx_Info[5]);	//第六位开始为CMD命令码ID(大小为2byte)
	switch(RefereeInfor.CmdID)
	{
		case 0x0201:
		{
		  game_robot_state.robot_id                        =  UsartRx_Info[7];
			game_robot_state.robot_level                     =  UsartRx_Info[8];
			game_robot_state.remain_HP                       =  bit8TObit16(&UsartRx_Info[9]);
			game_robot_state.max_HP                          =  bit8TObit16(&UsartRx_Info[11]);
			game_robot_state.shooter_id1_17mm_cooling_rate      =  bit8TObit16(&UsartRx_Info[13]);
			game_robot_state.shooter_id1_17mm_cooling_limit     =  bit8TObit16(&UsartRx_Info[15]);
			game_robot_state.shooter_id1_17mm_speed_limit      =  bit8TObit16(&UsartRx_Info[17]);
			game_robot_state.shooter_id2_17mm_cooling_rate     =  bit8TObit16(&UsartRx_Info[19]);
			game_robot_state.shooter_id2_17mm_cooling_limit       =  bit8TObit16(&UsartRx_Info[21]);
			game_robot_state.shooter_id2_17mm_speed_limit      =  bit8TObit16(&UsartRx_Info[23]);
			game_robot_state.shooter_id1_42mm_cooling_rate      =  bit8TObit16(&UsartRx_Info[25]);
			game_robot_state.shooter_id1_42mm_cooling_limit      =  bit8TObit16(&UsartRx_Info[27]);
			game_robot_state.shooter_id1_42mm_speed_limit      =  bit8TObit16(&UsartRx_Info[29]);
			game_robot_state.chassis_power_limit      =  bit8TObit16(&UsartRx_Info[31]);

					
			if(game_robot_state.robot_id==1)  Arm_Color=1;//红英雄
			if(game_robot_state.robot_id==101) Arm_Color=0;//蓝英雄
			
		}break;
		case 0x0202:
		{
		  power_heat_data.chassis_volt               =  bit8TObit16(&UsartRx_Info[7]);
			power_heat_data.chassis_current            =  bit8TObit16(&UsartRx_Info[9]);
			power_heat_data.chassis_power              =  bit8TObit32(&UsartRx_Info[11]);//底盘功率
			power_heat_data.chassis_power_buffer       =  bit8TObit16(&UsartRx_Info[15]);//底盘功率缓冲
			power_heat_data.shooter_id1_17mm_cooling_heat              =  bit8TObit16(&UsartRx_Info[17]);
			power_heat_data.shooter_id2_17mm_cooling_heat              =  bit8TObit16(&UsartRx_Info[19]);			
			power_heat_data.shooter_id1_42mm_cooling_heat              =  bit8TObit16(&UsartRx_Info[21]);	//枪口热量	

		}break;
		case 0x0204:
		{
		  buff_data.power_rune_buff  =  UsartRx_Info[7];
		}break;
		case 0x0207:
		{
		  real_shoot_data.bullet_speed=bit8TObit32(&UsartRx_Info[10]);
		}break;
		case 0x0208:
		{
		  bullet_remaining.bullet_remaining_num_42mm=bit8TObit16(&UsartRx_Info[9]);
		}break;
		default:
		{
		}	
	
  }
}

/**
 * @Function : USART4InitConfig
 * @Input    : void
 * @Output   : void
 * @Notes    : USART4_TX-->PA0      USART4_RX-->PA1
 * @Copyright: Aword
 **/
void USART4_InitConfig(void)
{	
  GPIO_InitTypeDef   GPIO_InitStruct;
	USART_InitTypeDef  USART_InitStruct;
	NVIC_InitTypeDef   NVIC_InitStruct;
	
	/*enabe clocks*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4,ENABLE);
	
	/*open the alternative function*/
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource1,GPIO_AF_UART4);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_UART4);
	
	/*Configure PB10,PB11 as GPIO_InitStruct1 input*/
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	USART_InitStruct.USART_BaudRate            = 115200;
	USART_InitStruct.USART_WordLength          = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits            = USART_StopBits_1;
	USART_InitStruct.USART_Parity              = USART_Parity_No;
	USART_InitStruct.USART_Mode                = USART_Mode_Rx|USART_Mode_Tx;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(UART4,&USART_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel                    = UART4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority  = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority         = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd                 = ENABLE ;
	NVIC_Init(&NVIC_InitStruct);

  USART_Cmd(UART4,ENABLE);	
	USART_ITConfig(UART4,USART_IT_IDLE,ENABLE);
}	

/**
 * @Function : 裁判系统DMA初始化
 * @Input    : void
 * @Output   : void
 * @Notes    : USART4_TX-->PA0      USART4_RX-->PA1
 * @Copyright: Aword
 **/
void JudgeSys_DMA_InitConfig(void)
{
	
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1,ENABLE);
	
	DMA_DeInit(DMA1_Stream2);
	DMA_StructInit(&dma);
	
	dma.DMA_Channel             =DMA_Channel_4;             //通道5
	dma.DMA_PeripheralBaseAddr  =(uint32_t)&(UART4->DR);
	dma.DMA_Memory0BaseAddr     =(uint32_t)JUDGE_RX_BUFF1;
	dma.DMA_DIR                 =DMA_DIR_PeripheralToMemory;
	dma.DMA_BufferSize          =JUDGE_MAX_LENGTH;      //一次传输的数据量的大小
	dma.DMA_PeripheralInc       =DMA_PeripheralInc_Disable; 
	dma.DMA_MemoryInc           =DMA_MemoryInc_Enable;
	dma.DMA_PeripheralDataSize  =DMA_PeripheralDataSize_Byte;
	dma.DMA_MemoryDataSize      =DMA_MemoryDataSize_Byte;
	dma.DMA_Mode                =DMA_Mode_Circular;         //Mode应该选择Circular模式，Normal模式只能接收一次数据
	dma.DMA_Priority            =DMA_Priority_VeryHigh;     //DMA通道优先级
  dma.DMA_FIFOMode            =DMA_FIFOMode_Disable;      //不开FIFO
	dma.DMA_FIFOThreshold       =DMA_FIFOThreshold_Full;
	dma.DMA_MemoryBurst         =DMA_MemoryBurst_Single;    //外设突发传输配置
	dma.DMA_PeripheralBurst     =DMA_PeripheralBurst_Single;//外设突发传输配置
	DMA_Init(DMA1_Stream2,&dma);
	
	Free_BUFF_NO=BUFF_NO2;
	BUFF_OK=FALSE;
	
	DMA_Cmd(DMA1_Stream2,ENABLE);	    //使能DMA1_Stream2
}

/**
 * @Function : 裁判系统初始化
 * @Input    : void
 * @Output   : void
 * @Notes    : USART4_TX-->PD9      USART4_RX-->PD8
 * @Copyright: Aword
 **/
void JudgeSys_Init(void)
{
  USART4_InitConfig();                          //串口初始化
	JudgeSys_DMA_InitConfig();                    //DMA初始化
	USART_DMACmd(UART4,USART_DMAReq_Rx,ENABLE);  //开启串口DMA接收
}


/**
 * @Function : USART4 IRQHandler
 * @Input    : void
 * @Output   : void
 * @Notes    : USART4_TX-->PD9      USART4_RX-->PD8
 * @Copyright: Aword
 **/
void UART4_IRQHandler(void)    
{
	uint32_t temp=temp;	
	
	if(USART_GetITStatus(UART4,USART_IT_IDLE)!=RESET)
	{ 
		 Referee_Receive_Flag = 300;	//接收到裁判系统数据后给一定长度的信号
		 temp=UART4->SR;                //清除串口空闲中断标志位 
	 	 temp=UART4->DR;  
		 DMA_Cmd(DMA1_Stream2,DISABLE);  //失能DMA1

		if(Free_BUFF_NO==BUFF_NO1)
		 {
			 JUDGE_BUFF2_RX_NUMBER=JUDGE_MAX_LENGTH-DMA_GetCurrDataCounter(DMA1_Stream2);//获得DMA当前收到的字节数，因为DMA中断只有接收长度满时才会触发，所以要把剩余数据读出来
		   dma.DMA_Memory0BaseAddr     =(uint32_t)JUDGE_RX_BUFF1;
			 DMA_Init(DMA1_Stream2,&dma);
	     Free_BUFF_NO=BUFF_NO2;
		 }
     else
		 {
			 JUDGE_BUFF1_RX_NUMBER=JUDGE_MAX_LENGTH-DMA_GetCurrDataCounter(DMA1_Stream2);
		   dma.DMA_Memory0BaseAddr     =(uint32_t)JUDGE_RX_BUFF2;
			 DMA_Init(DMA1_Stream2,&dma);
	     Free_BUFF_NO=BUFF_NO1;
		 }	
		   
	   DMA_SetCurrDataCounter(DMA1_Stream2,JUDGE_MAX_LENGTH);      //重新设置接收数据长度
		 DMA_Cmd(DMA1_Stream2,ENABLE);	 
		 Judge_Data_Process(); 		 
	}
}

/**
 * @Function : 解读当前一帧数据WWWW
 * @Input    : void
 * @Output   : void
 * @Notes    : void
 * @Copyright: Aword
 **/
void Judge_Data_Process(void)
{
	u16 i=0;
	u16 RxLength = 0;

	if(Free_BUFF_NO==BUFF_NO1)
	{	
		while(JUDGE_RX_BUFF1[i]==0XA5)
		{
			RxLength=(JUDGE_RX_BUFF1[i+2]<<8)|JUDGE_RX_BUFF1[i+1]+9;//本帧数据长度
			if (Verify_CRC16_Check_Sum(&JUDGE_RX_BUFF1[i], RxLength ))  
			{
				RefereeInfo_Decode(&JUDGE_RX_BUFF1[i]);
			}
			JUDGE_RX_BUFF1[i]=0;
			i+=RxLength;//下帧数据起始位置
		}
	}
	else
	{
		while(JUDGE_RX_BUFF2[i]==0XA5)
		{ 
			RxLength=(JUDGE_RX_BUFF2[i+2]<<8)|JUDGE_RX_BUFF2[i+1]+9;//本帧数据长度
			if (Verify_CRC16_Check_Sum(&JUDGE_RX_BUFF2[i], RxLength ))  RefereeInfo_Decode(&JUDGE_RX_BUFF2[i]); 
			JUDGE_RX_BUFF1[i]=0;
			i+=RxLength;//下帧数据起始位置
		}
	}

	
}



/**
  * @brief     pack data to bottom device
  * @param[in] sof：framehearder
  * @param[in] cmd_id:  command id of data
  * @param[in] *p_data: pointer to the data to be sent
  * @param[in] len:     the data length
  */
void referee_data_pack_handle(uint8_t sof,uint16_t cmd_id, uint8_t *p_data, uint16_t len)
{
	unsigned char i=i;
	
	uint8_t tx_buff[MAX_SIZE];

	uint16_t frame_length = frameheader_len + cmd_len + len + crc_len;   //数据帧长度	

	memset(tx_buff,0,frame_length);  //存储数据的数组清零
	
	/*****帧头打包*****/
	tx_buff[0] = sof;//数据帧起始字节
	memcpy(&tx_buff[1],(uint8_t*)&len, sizeof(len));//数据帧中data的长度
	tx_buff[3] = seq;//包序号
	Append_CRC8_Check_Sum(tx_buff,frameheader_len);  //帧头校验CRC8

	/*****命令码打包*****/
	memcpy(&tx_buff[frameheader_len],(uint8_t*)&cmd_id, cmd_len);
	
	/*****数据打包*****/
	memcpy(&tx_buff[frameheader_len+cmd_len], p_data, len);
	Append_CRC16_Check_Sum(tx_buff,frame_length);  //一帧数据校验CRC16

	if (seq == 0xff) seq=0;
  else seq++;
	
	/*****数据上传*****/
	USART_ClearFlag(UART4,USART_FLAG_TC);
	for(i=0;i<frame_length;i++)
	{
	  USART_SendData(UART4,tx_buff[i]);
	  while (USART_GetFlagStatus(UART4,USART_FLAG_TC) == RESET); //等待之前的字符发送完成
	}
}

