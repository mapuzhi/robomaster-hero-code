#ifndef _REFEREE_SYSTEM_H
#define _REFEREE_SYSTEM_H

#include "main.h"

extern int16_t Maximum_Power;

#define MAX_SIZE          200    //上传数据寄存器最大的长度
#define frameheader_len  5       //帧头长度
#define cmd_len          2       //命令码长度
#define crc_len          2       //CRC16校验


//帧头格式
typedef struct
{                                       //字节偏移     大小     说明
	u8 SOF;                               //   0          1      数据帧起始字节，固定值为0XA5
	u16 DataLength;                       //   1          2      数据帧内Data长度 
	u8 Seq;                               //   3          1      包序号 
	u8 CRC8;                              //   4          1      帧头CRC校验
}FrameHeader_TypeDef;

//通信协议
typedef struct
{                                       //字节偏移     大小     说明
	FrameHeader_TypeDef FrameHeader;      //   0          5      帧头协议
	u16 CmdID;                            //   5          2      命令码ID
	u16 Date[50];                         //   7          n      数据
	u16 FrameTail;                        //   7+n        2      CRC16校验
}RefereeInfo_TypeDef;




#define GAME_STATUS_ID     			 	   = 0x0001		//比赛状态数据,1Hz
#define GAME_RESULT_ID       				 = 0x0002		//比赛结果数据
#define GAME_ROBOT_HP_ID     				 = 0x0003		//机器人血量数据，1Hz
//#define DART_STATUS_ID       				 = 0x0004		//飞镖发射状

#define EVENE_DATA_ID								 = 0x0101		//场地事件数据
#define SUPPLY_PROJECTILE_ACTION_ID	 = 0x0102		//补给站动作标识
#define SUPPLY_PROJECTILE_BOOKING_ID = 0x0103		//请求补给站补弹数据，由参赛队发送，上限 10Hz
#define REFEREE_WARNING_ID					 = 0X0104		//裁判警告信息
//#define DART_REMAINING_TIME_ID			 = 0x0105		//飞镖发射口倒计时

#define GAME_ROBO_STATUS_ID  				 = 0x0201		//比赛机器人状态
#define POWER_HEAT_DATE_ID   				 = 0x0202		//实时功率热量数据， 50Hz 周期发送
#define GAME_ROBOT_POS_ID   				 = 0x0203		//机器人位置数据， 10Hz 发送
#define BUFF_ID         						 = 0x0204		//机器人增益
#define AERIAL_ROBO_ENERGY_ID				 = 0x0205		//空中机器人能量状态
#define ROBOT_HURT_ID         			 = 0x0206		//伤害状态
#define SHOOT_DATA_ID   						 = 0x0207		//实时射击信息
#define BULLET_REMAINING_NUM_ID			 = 0x0208		//子弹剩余发射数(仅空中以及哨兵机器人)
#define RFID_STATUS_ID   						 = 0x0209		//机器人RFID状态
//#define DART_CLIENT_CMD_ID   				 = 0x020A		//飞镖机器人客户端指令数据

#define INTERACTIVE_DATA_ID   			 = 0x0301
	



////比赛状态数据：0x0001  发送频率：1Hz  发送范围：所有机器人
typedef __packed struct
{
  /* current race stage     //当前比赛阶段
   0 not start              //比赛未开始
   1 preparation stage      //准备阶段
   2 self-check stage       //自检阶段
   3 5 seconds count down   //5s倒计时
   4 fighting stage         //对战中
   5 result computing stage //比赛结算中
	*/
  uint8_t  game_type:4;       //比赛类型
  uint8_t  game_process:4;    //当前比赛阶段
  uint16_t stage_remain_time; //当前阶段剩余时间
	
	uint64_t SyncTimeStamp;     //机器人接收到该指令的精确 Unix 时间，当机载端收到有效的 NTP 服务器授时后生效
} ext_game_status_t;


////比赛结果数据：0x0002  发送频率：比赛结束后发送  发送范围：所有机器人
typedef __packed struct              
{
  uint8_t winner;                   //0-平局 ， 1-红方胜利 ， 2-蓝方胜利
} ext_game_result_t;


////机器人血量数据:0x0003  发送频率：1Hz  发送范围：所有机器人
typedef __packed struct          
{
	uint16_t red_1_robot_HP;  //红 1 英雄机器人血量
	uint16_t red_2_robot_HP;  //红 2 工程机器人血量
	uint16_t red_3_robot_HP;  //红 3 步兵机器人血量
	uint16_t red_4_robot_HP;  //红 4 步兵机器人血量
	uint16_t red_5_robot_HP;  //红 5 步兵机器人血量
	uint16_t red_7_robot_HP;  //红 7 哨兵机器人血量
	uint16_t red_outpost_HP;  //红方前哨站血量
	uint16_t red_base_HP;     //红方基地血量
	
	uint16_t blue_1_robot_HP; //蓝 1 英雄机器人血量
	uint16_t blue_2_robot_HP; //蓝 2 工程机器人血量
	uint16_t blue_3_robot_HP; //蓝 3 步兵机器人血量
	uint16_t blue_4_robot_HP; //蓝 4 步兵机器人血量
	uint16_t blue_5_robot_HP; //蓝 5 步兵机器人血量
	uint16_t blue_7_robot_HP; //蓝 7 哨兵机器人血量
	uint16_t blue_outpost_HP; //蓝方前哨站血量
	uint16_t blue_base_HP;    //蓝方基地血量
} ext_game_robot_HP_t;


////////////飞镖发射状态：0x0004。发送频率：飞镖发射后发送，发送范围：所有机器人。
////////typedef __packed struct
////////{
//////// uint8_t dart_belong;           //发射飞镖队伍 1：红方，2：蓝方
//////// uint16_t stage_remaining_time; //发射时的剩余比赛时间
////////} ext_dart_status_t;


////////////人工智能挑战赛加成与惩罚区状态：0x0005。发送频率：1Hz 周期发送，发送范围：所有机器人。
////////typedef __packed struct
////////{
//////// uint8_t F1_zone_status:1;
//////// uint8_t F1_zone_buff_debuff_status:3;
//////// uint8_t F2_zone_status:1;
//////// uint8_t F2_zone_buff_debuff_status:3; 
//////// uint8_t F3_zone_status:1;
//////// uint8_t F3_zone_buff_debuff_status:3; 
//////// uint8_t F4_zone_status:1;
//////// uint8_t F4_zone_buff_debuff_status:3; 
//////// uint8_t F5_zone_status:1;
//////// uint8_t F5_zone_buff_debuff_status:3; 
//////// uint8_t F6_zone_status:1;
//////// uint8_t F6_zone_buff_debuff_status:3;
//////// 
//////// uint16_t red1_bullet_left;
//////// uint16_t red2_bullet_left;
//////// uint16_t blue1_bullet_left;
//////// uint16_t blue2_bullet_left;
////////} ext_ICRA_buff_debuff_zone_status_t;


////场地事件数据：0x0101  发送频率：1Hz周期发送  发送范围：己方机器人
typedef __packed struct
{
	/*
		bit 0：己方补给站 1 号补血点占领状态 1 为已占领；
		bit 1：己方补给站 2 号补血点占领状态 1 为已占领；
		bit 2：己方补给站 3 号补血点占领状态 1 为已占领；
	
		bit 3-5：己方能量机关状态：	
			bit 3 为打击点占领状态，1 为占领；
			bit 4 为小能量机关激活状态，1 为已激活；
			bit 5 为大能量机关激活状态，1 为已激活；
	
		bit 6：己方 R2 环形高地占领状态 1 为已占领；
		bit 7：己方 R3 梯形高地占领状态 1 为已占领；
		bit 8：己方 R4 梯形高地占领状态 1 为已占领；
	
		bit 9：己方基地护盾状态：
			1 为基地有虚拟护盾血量；
			0 为基地无虚拟护盾血量；
	
		bit 10：己方前哨战状态：	
			1 为前哨战存活；	
			0 为前哨战被击毁；
			
		bit 10 -31: 保留	
	*/
  uint32_t event_type;           
} ext_event_data_t;


////补给站动作标识：0x0102  发送频率：动作触发后发送  发送范围：己方机器人
typedef __packed struct
{
  uint8_t supply_projectile_id;  //补给站口 ID
  uint8_t supply_robot_id;       //补弹机器人 ID
  uint8_t supply_projectile_step;//出弹口开闭状态
  uint8_t supply_projectile_num; //补弹数量
} ext_supply_projectile_action_t;


////请求补给站补子弹：0x0103 10HZ
typedef __packed struct
{
  uint8_t supply_projectile_id;
  uint8_t supply_robot_id;
  uint8_t supply_num;	
} ext_supply_projectile_booking_t;


////裁判系统警告信息：0x0104（cmd_id） 发送频率：警告发生后发送  发送范围：己方机器人
typedef __packed struct
{
  uint8_t level;         //警告等级
  uint8_t foul_robot_id; //犯规机器人 ID  	
} ext_referee_warning_t;


////////////飞镖发射口倒计时：0x0105（cmd_id ） 发送频率：1Hz周期发送  发送范围：己方机器人
////////typedef __packed struct
////////{
//////// uint8_t dart_remaining_time; //15s 倒计时
////////} ext_dart_remaining_time_t;


////比赛机器人状态：0x0201  发送频率：10Hz  发送范围：单一机器人
typedef __packed struct                    
{ 
	uint8_t  robot_id;                       //红方英雄：1 ; 蓝方英雄：11
	uint8_t  robot_level;	                   //机器人等级：   一二三级：1,2,3  
	uint16_t remain_HP;                      //剩余血量
	uint16_t max_HP;                         //上限血量
	
////////	uint16_t shooter_heat0_cooling_rate;     //机器人 17mm 枪口每秒冷却值
////////	uint16_t shooter_heat0_cooling_limit;    //机器人 17mm 枪口热量上限
	
	uint16_t shooter_id1_17mm_cooling_rate;  //机器人 1 号 17mm 枪口每秒冷却值
	uint16_t shooter_id1_17mm_cooling_limit; //机器人 1 号 17mm 枪口热量上限
	uint16_t shooter_id1_17mm_speed_limit;    //机器人 1 号 17mm 枪口上限速度 单位 m/s
	
	uint16_t shooter_id2_17mm_cooling_rate;  //机器人 2 号 17mm 枪口每秒冷却值
	uint16_t shooter_id2_17mm_cooling_limit; //机器人 2 号 17mm 枪口热量上限
	uint16_t shooter_id2_17mm_speed_limit;   //机器人 2 号 17mm 枪口上限速度 单位 m/s
	
	uint16_t shooter_id1_42mm_cooling_rate;  //机器人 42mm 枪口每秒冷却值
	uint16_t shooter_id1_42mm_cooling_limit; //机器人 42mm 枪口热量上限
	uint16_t shooter_id1_42mm_speed_limit;   //机器人 42mm 枪口上限速度 单位 m/s
	
////////	uint16_t shooter_heat1_cooling_rate;     //机器人 42mm 枪口每秒冷却值
////////	uint16_t shooter_heat1_cooling_limit;    //机器人 42mm 枪口热量上限
////////	
////////	uint8_t  shooter_heat0_speed_limit;      //机器人 17mm 枪口上限速度 单位 m/s
////////	uint8_t  shooter_heat1_speed_limit;      //机器人 42mm 枪口上限速度 单位 m/s
	
////////	uint8_t  max_chassis_power;              //机器人最大底盘， 单位 w
	
	uint16_t chassis_power_limit;            //机器人底盘功率限制上限
  uint8_t  mains_power_gimbal_output:1;    //gimbal 口输出： 1 为有 24V 输出，0 为无 24v 输出
  uint8_t  mains_power_chassis_output:1;   //chassis 口输出：1 为有 24V 输出，0 为无 24v 输出
  uint8_t  mains_power_shooter_output:1;   //shooter 口输出：1 为有 24V 输出，0 为无 24v 输出
} ext_game_robot_status_t;


////实时功率热量数据： 0x0202  发送频率：50Hz  发送范围：单一机器人
typedef __packed struct                        
{ 
  uint16_t chassis_volt;                       //底盘输出电压
  uint16_t chassis_current;                    //底盘输出电流
  float    chassis_power;                      //底盘输出功率
  uint16_t chassis_power_buffer;               //底盘缓冲功率（飞坡增加至250J）
	
	uint16_t shooter_id1_17mm_cooling_heat;      //1 号 17mm 枪口热量
	uint16_t shooter_id2_17mm_cooling_heat;      //2 号 17mm 枪口热量
	uint16_t shooter_id1_42mm_cooling_heat;      //42mm 枪口热量
	
////////  uint16_t shooter_heat0;                      //17mm 枪口热量
////////  uint16_t shooter_heat1;                      //42mm 枪口热量
////////	uint16_t mobile_shooter_heat2;               //机动 17 mm 枪口热量
} ext_power_heat_data_t;


////机器人位置：0x0203  发送频率：10Hz  发送范围：单一机器人
typedef __packed struct
{
  float x;
  float y;
  float z;
  float yaw; //位置枪口，单位度
} ext_game_robot_pos_t;


////机器人增益：0x0204  发送频率：1Hz 周期发送  发送范围：单一机器人
typedef __packed struct
{
	/*
	bit 0：机器人血量补血状态
	bit 1：枪口热量冷却加速
	bit 2：机器人防御加成
	bit 3：机器人攻击加成
	其他 bit 保留
	*/
  uint8_t power_rune_buff;

} ext_buff_t;


////空中机器人能量状态：0x0205  发送频率：10Hz  发送范围：单一机器人
typedef __packed struct
{
////////  uint16_t energy_point;  //积累的能量点
  uint8_t attack_time;    //可攻击时间，单位 s，30s 递减至 0
} aerial_robot_energy_t;


////伤害状态：0x0206  发送频率：伤害发生后发送  发送范围：单一机器人
typedef __packed struct                   
{
  uint8_t armor_id:4; //受打击的装甲ID：0~4
		/* 
		0-3bits: the attacked armor id:
		0x00:0 front
		0x01:1 left
		0x02:2 behind
		0x03:3 right
		others reserved
		*/
  uint8_t hurt_type:4;  //血量变化类型
		/* 
		4-7bits: blood volume change type 血量变化类型
		0x0: armor attacked        装甲伤害扣血                
		0x1: module offline        模块掉线扣血
		0x2: bullet over speed     超射速扣血
		0x3: bullet over frequency 超枪口热量扣血
		0x4: over power	           超底盘功率扣血
		0x5  armor coll	           装甲撞击扣血
		*/
} ext_robot_hurt_t;


////实时射击信息：0x0207  发送频率：射击后发送  发送范围：单一机器人
typedef __packed struct                 
{
  uint8_t bullet_type;  //子弹类型  1:17mm弹丸; 2:42mm弹丸
	
	uint8_t shooter_id;   //发射机构 ID： 1--1 号 17mm 发射机构;  2--2 号 17mm 发射机构;  3--42mm 发射机构
	
  uint8_t bullet_freq;  //子弹射频
  float   bullet_speed; //子弹射速   m/s
} ext_shoot_data_t;


////子弹剩余发射数目：0x0208 1HZ
typedef __packed struct                 
{
////////  uint16_t bullet_remaining_num; //子弹剩余发射数目
	
	uint16_t bullet_remaining_num_17mm;  //17mm 子弹剩余发射数目
	uint16_t bullet_remaining_num_42mm;  //42mm 子弹剩余发射数目
	uint16_t coin_remaining_num;         //剩余金币数量
} ext_bullet_remaining_t;


////机器人RFID状态：0x0209  发送频率：1Hz  发送范围：单一机器人
typedef __packed struct
{
	/*
	bit 0：基地增益点 RFID 状态；
	bit 1：高地增益点 RFID 状态；
	bit 2：能量机关激活点 RFID 状态；
	bit 3：飞坡增益点 RFID 状态；
	bit 4：前哨岗增益点 RFID 状态；
	bit 5：资源岛增益点 RFID 状态；
	bit 6：补血点增益点 RFID 状态；
	bit 7：工程机器人补血卡 RFID 状态；
	bit 8-31：保留
	RFID 状态不完全代表对应的增益或处罚状态，例如敌方已占领的高地增益点，不
	能获取对应的增益效果
	*/
 uint32_t rfid_status;
} ext_rfid_status_t;


////////////飞镖机器人客户端指令数据：0x020A  发送频率：10Hz  发送范围：单一机器人
////////typedef __packed struct
////////{
//////// uint8_t dart_launch_opening_status;
//////// uint8_t dart_attack_target;
//////// uint16_t target_change_time;
//////// uint8_t first_dart_speed;
//////// uint8_t second_dart_speed;
//////// uint8_t third_dart_speed;
//////// uint8_t fourth_dart_speed;
//////// uint16_t last_dart_launch_time;
//////// uint16_t operate_launch_cmd_time;
////////} ext_dart_client_cmd_t;


////////交互数据接收信息：0x0301
typedef __packed struct
{
 uint16_t data_cmd_id;   //数据段的内容 ID
 uint16_t sender_ID;     //发送者的 ID
 uint16_t receiver_ID;   //接收者的 ID
} ext_student_interactive_header_data_t;

////////typedef __packed struct
////////{                                       //字节偏移     大小     说明
////////	int16_t Data_Content;
////////	int16_t Sender_ID;
////////	int16_t Client_ID;
////////  float   Date1;                        //   0          4      
////////  float   Date2;                        //   4          4
////////  float   Date3;                        //   8          4
////////	uint8_t Mask;                         //   12         1
////////} ShowData_t;

#define  PowerHeatData_ID          0x0004//功率热量数据？代表什么？


extern  RefereeInfo_TypeDef              RefereeInfor;
extern  ext_game_status_t                game_information;	         //0x0001 比赛状态数据
extern  ext_game_result_t                game_result_data;	         //0x0002 比赛结果数据
extern  ext_game_robot_HP_t	             game_robot_HP;              //0x0003 比赛机器人血量数据
//extern  ext_dart_status_t								 dart_status;    						 //0x0004 飞镖发射状态

extern	ext_event_data_t                 event_data;                 //0x0101 场地事件数据
extern	ext_supply_projectile_action_t   supply_projectile_action;   //0x0102 场地补给站动作标识数据
extern	ext_supply_projectile_booking_t  supply_projectile_booking;  //0x0103 请求补给站补弹数据
extern  ext_referee_warning_t            referee_warning;            //0x0104 裁判警告数据
//extern  ext_dart_remaining_time_t				 dart_remaining_time;				 //0x0105 飞镖发射口倒计时

extern	ext_game_robot_status_t          game_robot_state;           //0x0201 机器人状态数据
extern	ext_power_heat_data_t 	         power_heat_data;	           //0x0202 实时功率热量数据
extern	ext_game_robot_pos_t		         game_robot_pos;	           //0x0203 机器人位置数据
extern	ext_buff_t                       buff_data;                  //0x0204 机器人增益数据
////////extern	ext_aerial_robot_energy_t        aerial_robot_energy;        //0x0205 空中机器人能量状态数据
extern	aerial_robot_energy_t            aerial_robot_energy;        //0x0205 空中机器人能量状态数据

extern	ext_robot_hurt_t                 robot_hurt_data;            //0x0206 伤害状态数据
extern	ext_shoot_data_t                 real_shoot_data;            //0x0207 实时射击数据
extern  ext_bullet_remaining_t           bullet_remaining;           //0x0208 弹丸剩余发射数，仅空中机器人，哨兵机器人以及 ICRA 机器人
extern  ext_rfid_status_t								 rfid_status;								 //0x0209 机器人 RFID 状态
//extern  ext_dart_client_cmd_t            dart_client_cmd;            //0x020A 飞镖机器人客户端指令数据

////////extern  ShowData_t                       ShowData;
extern  ext_student_interactive_header_data_t                       ShowData;

  
extern short Referee_Info_ReSec;
extern short referee_online_flag;
extern short usart4_toggle_flag; 
extern u8 Arm_Color;

float bit8TObit32(uint8_t *change_info);
char bit32TObit8(int index_need,int bit32);
int16_t bit8TObit16(uint8_t *change_info);
char bit16TObit8(int index_need,int bit16);

void RefereeInfo_Decode(uint8_t *UsartRx_Info);
void USART4_InitConfig(void);
void JudgeSys_DMA_InitConfig(void);
void JudgeSys_Init(void);
void Judge_Data_Process(void);
void Student_Date_Upload(void);


#endif




