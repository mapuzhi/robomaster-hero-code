#include "armory.h"

/***************************************数据定义区***************************************/
u8 DanLian_ManDan_Flag = 1;  //1-不满弹，0-满弹

u8 Poke_Flag = 1,Poke_Flag_v1 = 1,Initial_Poke_Flag = 1;

u8 Poke_Reverse_Flag = 0;

int32_t Aim_Angle,Initial_Poke_Angle;


int16_t shoot_poke_count = 1000;

//short poke_sta=0;        //0-停止，1-转动

short Poke_Flag_Count=0;

extern int Flip_Aim_Speed;
extern  u8 Poke_Down_State;

int16_t shoot_poke_count0, shoot_poke_count1;

int16_t Poke_KaDan1 = 1000;
int16_t poke_reverse_count = 1000;


extern float Aim_Pitch206_Angle;
u8 Reverse_rotation_flag = 1;
u8 shoot_poke_count1_flag = 1;

/***************************************函数处理区***************************************/


void Armory_Control(void)
{
	
  ////翻盖电机////
	////遥控器模式
////	if(DBUS.RC.Switch_Right == RC_SW_MID)
////	{
////		Flip_Aim_Speed = DBUS.RC.ch1/5;
////		Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, Flip_Aim_Speed);
////	}
////	
////	////键鼠模式
////	if(DBUS.RC.Switch_Right == RC_SW_DOWN)
////	{
////    if(DBUS.PC.Keyboard & KEY_G)
////		{
////			Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, 200);
////		}
////		
////    else if(DBUS.PC.Keyboard & KEY_F)
////		{
////			Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, -200);
////		}
////		
////		else
////		{
////			Flip_Motor_206.Target_Speed = 0;
////		}
////	}
	

		
	////拨盘电机////
  if((ptz_init_sec == 1) && (DanLian_ManDan_Flag == 1))
	{

		Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -1500);//750
		
		if( (abs(Poke_Motor_207.Real_Speed) < 200) )
		{
			Poke_KaDan1=1000;
			Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, 3500);
		}	
		
		if(LIMIT_STA == 0) //弹碰到限位开关，弹链满弹
		{
			Poke_Motor_207.Target_Speed = 0;
			DanLian_ManDan_Flag = 0;
		}
		
	}



	if(DanLian_ManDan_Flag == 0)
	{
		
		 if(Initial_Poke_Flag==1)    //开机只进行一次
			{
			 Initial_Poke_Flag=0;
			 Aim_Angle=Poke_Down_Use_Angle;
			 Initial_Poke_Angle=Poke_Down_Use_Angle;
			}	
		
		
		////遥控器模式
		if(DBUS.RC.Switch_Right != RC_SW_DOWN)
		{
				////拨盘转动一格控制
				if((shoot_poke_count <= 0) && (DBUS.RC.Switch_Left == RC_SW_DOWN) && (Shoot_Sta == 1) && (Shoot_heat_Aban == 0))
				{
					if(Poke_Flag == 1)
					{
						Initial_Poke_Angle = Poke_Down_Use_Angle;
						Aim_Angle -= 8192*19/6;     //转动一格
						Poke_Flag = 0;
						shoot_poke_count = 1000;
						
						Poke_Reverse_Flag = 1;				

					}
					else
					{
						PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
						Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
					}
										
				}
				
				////拨盘准备下一次!
				else if((shoot_poke_count > 0)||(DBUS.RC.Switch_Left != RC_SW_DOWN))
				{
					 Poke_Flag = 1;
										
					 PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle*0.1f, Aim_Angle*0.1f);
					 Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);						


					if((shoot_poke_count < 300) &&(Poke_Reverse_Flag == 1))
//					if(Poke_Reverse_Flag == 1)
					{
						if( (abs(PID_Poke_207_MechAngle.PIDout)>23000) && (abs(Poke_Motor_207.Real_Speed) < 50) )
						{
							Aim_Angle = Poke_Down_Use_Angle;
							Aim_Angle += 8192*19/6;
							PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle_Rev, Poke_Down_Use_Angle, Aim_Angle);
							Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed_Rev, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);	
							Poke_Reverse_Flag = 0;
							
						}
					}
					
				}


				else
				{
					Poke_Motor_207.Target_Speed=0;
				}	
		}
		
			
		

		////键鼠模式
		if( DBUS.RC.Switch_Right == RC_SW_DOWN )
		{
				////拨盘转动一格控制
				if((shoot_poke_count <= 0) && (DBUS.PC.Press_Left) && (Shoot_Sta == 1) && (Shoot_heat_Aban == 0) )
				{
					if(Poke_Flag == 1)
					{
						Initial_Poke_Angle = Poke_Down_Use_Angle;	
						Aim_Angle -= 8192*19/6;     //转动一格
						Poke_Flag = 0;
						shoot_poke_count = 1000;
					}
					else
					{
						PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
						Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
					}
				}
				
				////拨盘准备下一次!
				else if((shoot_poke_count > 0)||(!DBUS.PC.Press_Left) || (Shoot_Sta == 0))
				{
					 Poke_Flag = 1;
					
					 PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle*0.1f, Aim_Angle*0.1f);
					 Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
					
					
					if((shoot_poke_count < 300) &&(Poke_Reverse_Flag == 1))
//					if(Poke_Reverse_Flag == 1)
					{
						if( (abs(PID_Poke_207_MechAngle.PIDout)>23000) && (abs(Poke_Motor_207.Real_Speed) < 50) )
						{	//PID_Poke_207_MechAngle.PIDout最大6777.56，最小-15000
							Aim_Angle = Poke_Down_Use_Angle;
							Aim_Angle += 8192*19/6;
							PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle_Rev, Poke_Down_Use_Angle, Aim_Angle);
							Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed_Rev, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);	
							Poke_Reverse_Flag = 0;
							
						}
					}	
					
				}

				else
				{
					Poke_Motor_207.Target_Speed=0;
				}
		}

	}




//////		////用定时器来控制固定转动——没有角度环，只能靠重复测试不同的时间来控制转动角度，但是电机转动会有明显的卡转现象，然后下一次转动就会转很大角度，具体原因不明；提供的力很小，不足以把弹丸顶上去
//////	     if (DBUS.RC.Switch_Right != RC_SW_DOWN)
//////			 {
//////				 if( (DBUS.RC.Switch_Left == RC_SW_DOWN) && (shoot_poke_count0 > 0) && (Shoot_Sta == 1) )
//////				 {
//////					 Poke_Motor_207.Target_Speed = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -1000);
//////				 }
//////				 else
//////				 {
//////					 Poke_Motor_207.Target_Speed = 0;
//////				 }
//////				 
//////				 if( (DBUS.RC.Switch_Left == RC_SW_UP) || (DBUS.RC.Switch_Left == RC_SW_MID) )
//////				 {
//////					 shoot_poke_count0 = 60;
//////				 }
//////				 
//////			 }


}



//力大慢顶
//连续

//打完一颗蛋回转一点，再转回去





























//#include "armory.h"

///***************************************数据定义区***************************************/
//u8 DanLian_ManDan_Flag = 1;  //1-不满弹，0-满弹

//u8 Poke_Flag = 1,Poke_Flag_v1 = 1,Initial_Poke_Flag = 1;

//u8 Poke_Reverse_Flag = 0;

//int32_t Aim_Angle,Initial_Poke_Angle;


//int16_t shoot_poke_count = 1000;

////short poke_sta=0;        //0-停止，1-转动

//short Poke_Flag_Count=0;

//extern int Flip_Aim_Speed;
//extern  u8 Poke_Down_State;

//int16_t shoot_poke_count0, shoot_poke_count1;

//int16_t Poke_KaDan1 = 1000;
//int16_t poke_reverse_count = 1000;


//extern float Aim_Pitch206_Angle;
//u8 Reverse_rotation_flag = 1;
//u8 shoot_poke_count1_flag = 1;

///***************************************函数处理区***************************************/


//void Armory_Control(void)
//{
//	
//  ////翻盖电机////
//	////遥控器模式
//////	if(DBUS.RC.Switch_Right == RC_SW_MID)
//////	{
//////		Flip_Aim_Speed = DBUS.RC.ch1/5;
//////		Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, Flip_Aim_Speed);
//////	}
//////	
//////	////键鼠模式
//////	if(DBUS.RC.Switch_Right == RC_SW_DOWN)
//////	{
//////    if(DBUS.PC.Keyboard & KEY_G)
//////		{
//////			Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, 200);
//////		}
//////		
//////    else if(DBUS.PC.Keyboard & KEY_F)
//////		{
//////			Flip_Motor_206.Target_Speed = Pid_Calc(&PID_Flip_206_Speed, Flip_Motor_206.Real_Speed, -200);
//////		}
//////		
//////		else
//////		{
//////			Flip_Motor_206.Target_Speed = 0;
//////		}
//////	}
//	

//		
//	////拨盘电机////
//  if((ptz_init_sec == 1) && (DanLian_ManDan_Flag == 1))
//	{

//		Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -1500);//750
//		
//		if( (abs(Poke_Motor_207.Real_Speed) < 200) )
//		{
//			Poke_KaDan1=1000;
//			Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207, Poke_Motor_207.Real_Speed, 3500);
//		}	
//		
//		if(LIMIT_STA == 0) //弹碰到限位开关，弹链满弹
//		{
//			Poke_Motor_207.Target_Speed = 0;
//			DanLian_ManDan_Flag = 0;
//		}
//		
//	}



//	if(DanLian_ManDan_Flag == 0)
//	{
//		
//		 if(Initial_Poke_Flag==1)    //开机只进行一次
//			{
//			 Initial_Poke_Flag=0;
//			 Aim_Angle=Poke_Down_Use_Angle;
//			 Initial_Poke_Angle=Poke_Down_Use_Angle;
//			}	
//		
//		
//		////遥控器模式
//		if(DBUS.RC.Switch_Right != RC_SW_DOWN)
//		{
//				////拨盘转动一格控制
//				if((shoot_poke_count <= 0) && (DBUS.RC.Switch_Left == RC_SW_DOWN) && (Shoot_Sta == 1) && (Shoot_heat_Aban == 0))
//				{
//					if(Poke_Flag == 1)
//					{
//						Initial_Poke_Angle = Poke_Down_Use_Angle;
//						Aim_Angle -= 8192*19/6;     //转动一格
//						Poke_Flag = 0;
//						shoot_poke_count = 1000;
//						Poke_Reverse_Flag = 1;

//					}
////					else
////					{
////						PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
////						Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
////					}
//										
//				}
//				
//				////拨盘准备下一次!
//				else if((shoot_poke_count > 0)||(DBUS.RC.Switch_Left != RC_SW_DOWN))
//				{
//					 Poke_Flag = 1;			
//					
//					 PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
//					 Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
//					
//					
////					if((abs(PID_Poke_207_MechAngle.PIDout) > 15000) &&(Poke_Reverse_Flag == 1))
////					{
////						if(Poke_Motor_207.Real_Speed < 10)
////						{
////							Aim_Angle = Poke_Down_Use_Angle;
////							Aim_Angle += 8192*19/6;
////							PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
////							Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);	
////							Poke_Reverse_Flag = 0;
////							
////						}
////					}	
////			
//				}


////				else
////				{
////					Poke_Motor_207.Target_Speed=0;
////				}	
//				
//		}
//		
//			
//		

//		////键鼠模式
//		if( DBUS.RC.Switch_Right == RC_SW_DOWN )
//		{
//				////拨盘转动一格控制
//				if((shoot_poke_count <= 0) && (DBUS.PC.Press_Left) && (Shoot_Sta == 1) && (Shoot_heat_Aban == 0) )
//				{
//					if(Poke_Flag == 1)
//					{
//						Initial_Poke_Angle = Poke_Down_Use_Angle;	
//						Aim_Angle -= 8192*19/6;     //转动一格
//						Poke_Flag = 0;
//						shoot_poke_count = 1000;
////						Poke_Reverse_Flag = 1;
//					}
//					else
//					{
//						PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
//						Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);
//					}
//				}
//				
//				////拨盘准备下一次!
//				else if((shoot_poke_count > 0)||(!DBUS.PC.Press_Left) || (Shoot_Sta == 0))
//				{
//					 Poke_Flag = 1;
//					
//					 PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
//					 Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);	

////						if((shoot_poke_count<300) &&(Poke_Reverse_Flag == 1))
////						{
////							if(Poke_Motor_207.Real_Speed < 10)
////							{
////								Aim_Angle = Poke_Down_Use_Angle;
////								Aim_Angle += 8192*19/6;
////								PID_Poke_207_MechAngle.PIDout = Pid_Calc(&PID_Poke_207_MechAngle, Poke_Down_Use_Angle, Aim_Angle);
////								Poke_Motor_207.Target_Speed   = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, PID_Poke_207_MechAngle.PIDout);	
////								Poke_Reverse_Flag = 0;
////								
////							}
////						}	
//				
//				}

//				else
//				{
//					Poke_Motor_207.Target_Speed=0;
//				}
//		}

//	}




////////		////用定时器来控制固定转动——没有角度环，只能靠重复测试不同的时间来控制转动角度，但是电机转动会有明显的卡转现象，然后下一次转动就会转很大角度，具体原因不明；提供的力很小，不足以把弹丸顶上去
////////	     if (DBUS.RC.Switch_Right != RC_SW_DOWN)
////////			 {
////////				 if( (DBUS.RC.Switch_Left == RC_SW_DOWN) && (shoot_poke_count0 > 0) && (Shoot_Sta == 1) )
////////				 {
////////					 Poke_Motor_207.Target_Speed = Pid_Calc(&PID_Poke_207_Speed, Poke_Motor_207.Real_Speed, -1000);
////////				 }
////////				 else
////////				 {
////////					 Poke_Motor_207.Target_Speed = 0;
////////				 }
////////				 
////////				 if( (DBUS.RC.Switch_Left == RC_SW_UP) || (DBUS.RC.Switch_Left == RC_SW_MID) )
////////				 {
////////					 shoot_poke_count0 = 60;
////////				 }
////////				 
////////			 }


//}



////力大慢顶
////连续

////打完一颗蛋回转一点，再转回去























