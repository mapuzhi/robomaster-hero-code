#ifndef _PID_H
#define _PID_H
#include "main.h"

typedef struct
{
	float P;
	float I;
	float D;
	float CurrentError;
	float LastError;
	float ErrorIgnored;
	float Pout;
	float Iout;
	float Dout;
	float PIDout;
	float PIDOutCompensate;
	float PMax;	
	float IMax;
	float DMax;
	float PIDOutMax;
	float PIDOutLast;
	float Target_Speed_Last;
	float Speed_Ratio;
	float Acceleration;
	u8 Motor_Type;
}PID_TypeDef;

extern PID_TypeDef PID_Chassis_201,PID_Chassis_202,PID_Chassis_203,PID_Chassis_204;
extern PID_TypeDef PID_Yaw_205_Position_Reg,PID_Yaw_205_Speed_Reg,PID_Yaw_205_MechAngle,PID_Yaw_205_Speed;
extern PID_TypeDef PID_Pitch_206_Position_Reg,PID_Pitch_206_Speed_Reg,PID_Pitch_206_MechAngle,PID_Pitch_206_Speed;
extern PID_TypeDef PID_Pitch_207_Position_Reg,PID_Pitch_207_Speed_Reg,PID_Pitch_207_MechAngle,PID_Pitch_207_Speed;
extern PID_TypeDef PID_Yaw_205_NUC_Mech,PID_Yaw_205_NUC_Speed,PID_Pitch_206_NUC_Mech,PID_Pitch_206_NUC_Speed,PID_Pitch_207_NUC_Mech,PID_Pitch_207_NUC_Speed;
extern PID_TypeDef PID_Flip_206_MechAngle,PID_Flip_206_Speed;
extern PID_TypeDef PID_Poke_207,PID_Poke_207_MechAngle,PID_Poke_207_Speed,PID_Poke_207_MechAngle_Rev,PID_Poke_207_Speed_Rev;
extern PID_TypeDef PID_Shoot_201_Left,PID_Shoot_202_Right;

void PID_Init(void);
float Pid_Calc(PID_TypeDef *PID,float Current_Speed,float Target_Speed);

#endif

