#include "pid.h"
/***************************************数据定义区***************************************/
/**底盘电机*/
PID_TypeDef		PID_Chassis_201,PID_Chassis_202,PID_Chassis_203,PID_Chassis_204;
/*云台电机*/
PID_TypeDef		PID_Yaw_205_Position_Reg,PID_Yaw_205_Speed_Reg,PID_Yaw_205_MechAngle,PID_Yaw_205_Speed;
PID_TypeDef		PID_Pitch_206_Position_Reg,PID_Pitch_206_Speed_Reg,PID_Pitch_206_MechAngle,PID_Pitch_206_Speed;
PID_TypeDef		PID_Pitch_207_Position_Reg,PID_Pitch_207_Speed_Reg,PID_Pitch_207_MechAngle,PID_Pitch_207_Speed;
/*云台自瞄*/
PID_TypeDef		PID_Yaw_205_NUC_Mech,PID_Yaw_205_NUC_Speed,PID_Pitch_206_NUC_Mech,PID_Pitch_206_NUC_Speed,PID_Pitch_207_NUC_Mech,PID_Pitch_207_NUC_Speed;
/*拨弹盘及*/
PID_TypeDef		PID_Poke_207,PID_Poke_207_MechAngle,PID_Poke_207_Speed,PID_Poke_207_MechAngle_Rev,PID_Poke_207_Speed_Rev;
/*翻盖电机*/
PID_TypeDef   PID_Flip_206_MechAngle,PID_Flip_206_Speed;
/*摩擦轮电机*/
PID_TypeDef		PID_Shoot_201_Left, PID_Shoot_202_Right;


/***************************************函数处理区***************************************/
/**
  * @brief  PID参数初始化
  * @param  void
  * @retval void
  * @notes  void
  */
void PID_Init(void)
{
	
	{//底盘	
		
		PID_Chassis_201.P=200;
		PID_Chassis_201.I=0;
		PID_Chassis_201.D=0;
		PID_Chassis_201.Motor_Type = 2;
			
		PID_Chassis_202.P=200;
		PID_Chassis_202.I=0;
		PID_Chassis_202.D=0;
		PID_Chassis_201.Motor_Type = 2;
				
		PID_Chassis_203.P=200;
		PID_Chassis_203.I=0;
		PID_Chassis_203.D=0;
		PID_Chassis_201.Motor_Type = 2;
				
		PID_Chassis_204.P=200;
		PID_Chassis_204.I=0;
		PID_Chassis_204.D=0;
		PID_Chassis_201.Motor_Type = 2;
		
		
		/*0x201*/
		PID_Chassis_201.CurrentError=0;
		PID_Chassis_201.LastError=0;
		PID_Chassis_201.ErrorIgnored=2;
		PID_Chassis_201.Pout=0;
		PID_Chassis_201.Iout=0;
		PID_Chassis_201.Dout=0;
		PID_Chassis_201.PIDout=0;
		PID_Chassis_201.PIDOutCompensate=0;
		PID_Chassis_201.PMax=15000;
		PID_Chassis_201.IMax=3000;
		PID_Chassis_201.DMax=5000;
		PID_Chassis_201.PIDOutMax=16000;
		PID_Chassis_201.PIDOutLast=0;
		PID_Chassis_201.Target_Speed_Last=0;
		PID_Chassis_201.Speed_Ratio=CHASSIS_SPEED_RATIO;
		PID_Chassis_201.Acceleration=CHASSIS_ACCELARATION;
			
		/*0x202*/
		PID_Chassis_202.CurrentError=0;
		PID_Chassis_202.LastError=0;
		PID_Chassis_202.ErrorIgnored=2;
		PID_Chassis_202.Pout=0;
		PID_Chassis_202.Iout=0;
		PID_Chassis_202.Dout=0;
		PID_Chassis_202.PIDout=0;
		PID_Chassis_202.PIDOutCompensate=0;
		PID_Chassis_202.PMax=15000;
		PID_Chassis_202.IMax=3000;
		PID_Chassis_202.DMax=5000;
		PID_Chassis_202.PIDOutMax=16000;
		PID_Chassis_202.PIDOutLast=0;
		PID_Chassis_202.Target_Speed_Last=0;
		PID_Chassis_202.Speed_Ratio=CHASSIS_SPEED_RATIO;
		PID_Chassis_202.Acceleration=CHASSIS_ACCELARATION;

		/*0x203*/
		PID_Chassis_203.CurrentError=0;
		PID_Chassis_203.LastError=0;
		PID_Chassis_203.ErrorIgnored=2;
		PID_Chassis_203.Pout=0;
		PID_Chassis_203.Iout=0;
		PID_Chassis_203.Dout=0;
		PID_Chassis_203.PIDout=0;
		PID_Chassis_203.PIDOutCompensate=0;
		PID_Chassis_203.PMax=15000;
		PID_Chassis_203.IMax=3000;
		PID_Chassis_203.DMax=5000;
		PID_Chassis_203.PIDOutMax=16000;
		PID_Chassis_203.PIDOutLast=0;
		PID_Chassis_203.Target_Speed_Last=0;
		PID_Chassis_203.Speed_Ratio=CHASSIS_SPEED_RATIO;
		PID_Chassis_203.Acceleration=CHASSIS_ACCELARATION;
			
		/*0x204*/
		PID_Chassis_204.CurrentError=0;
		PID_Chassis_204.LastError=0;
		PID_Chassis_204.ErrorIgnored=2;
		PID_Chassis_204.Pout=0;
		PID_Chassis_204.Iout=0;
		PID_Chassis_204.Dout=0;
		PID_Chassis_204.PIDout=0;
		PID_Chassis_204.PIDOutCompensate=0;
		PID_Chassis_204.PMax=15000;
		PID_Chassis_204.IMax=3000;
		PID_Chassis_204.DMax=5000;
		PID_Chassis_204.PIDOutMax=16000;
		PID_Chassis_204.PIDOutLast=0;
		PID_Chassis_204.Target_Speed_Last=0;	
		PID_Chassis_204.Speed_Ratio=CHASSIS_SPEED_RATIO;
		PID_Chassis_204.Acceleration=CHASSIS_ACCELARATION;	
	}

	{//云台
		//初始 5 0 0 200 0 0
		//4 0 0 220 0 0
		//4 0 0 200 0 0
		/*0x205*/
		PID_Yaw_205_Position_Reg.P=1.2;//5;//5;
		PID_Yaw_205_Position_Reg.I=0;//0;//0;
		PID_Yaw_205_Position_Reg.D=0;//140;//0;
			
		PID_Yaw_205_Speed_Reg.P=12;//5;//20;
		PID_Yaw_205_Speed_Reg.I=0;//0;//0;
		PID_Yaw_205_Speed_Reg.D=0;//0;//3;
		
		PID_Yaw_205_MechAngle.P=5;//5;//8;
		PID_Yaw_205_MechAngle.I=0;//0;
		PID_Yaw_205_MechAngle.D=0;//1100//1000;
			
		PID_Yaw_205_Speed.P=200;//200;//150;
		PID_Yaw_205_Speed.I=0;//0;
		PID_Yaw_205_Speed.D=0;//15;
			
		//初始 1 0 0 100 0 20
		//1 0 0
		//150 0 20
		/*0x206*/
		PID_Pitch_206_Position_Reg.P=2;//5;
		PID_Pitch_206_Position_Reg.I=0;//0;
		PID_Pitch_206_Position_Reg.D=0;//35;
			
		PID_Pitch_206_Speed_Reg.P=10;//5;
		PID_Pitch_206_Speed_Reg.I=0;//0;
		PID_Pitch_206_Speed_Reg.D=0;//0;
		
			//1 0 0 150 0 20
			//1.2 0 0 200 0 20
			//1 0 0 220 0 20
			
		PID_Pitch_206_MechAngle.P=1;
		PID_Pitch_206_MechAngle.I=0;
		PID_Pitch_206_MechAngle.D=0;
			//78  0.12  18    2.2  0.60  6
		PID_Pitch_206_Speed.P=220;
		PID_Pitch_206_Speed.I=0;
		PID_Pitch_206_Speed.D=20;

			
		/*0x205*/
		PID_Yaw_205_Position_Reg.CurrentError=0;
		PID_Yaw_205_Position_Reg.LastError=0;
		PID_Yaw_205_Position_Reg.ErrorIgnored=2;
		PID_Yaw_205_Position_Reg.Pout=0;
		PID_Yaw_205_Position_Reg.Iout=0;
		PID_Yaw_205_Position_Reg.Dout=0;
		PID_Yaw_205_Position_Reg.PIDout=0;
		PID_Yaw_205_Position_Reg.PIDOutCompensate=0;
		PID_Yaw_205_Position_Reg.PMax=30000;
		PID_Yaw_205_Position_Reg.IMax=30000;
		PID_Yaw_205_Position_Reg.DMax=30000;
		PID_Yaw_205_Position_Reg.PIDOutMax=30000;
		PID_Yaw_205_Position_Reg.PIDOutLast=0;
		PID_Yaw_205_Position_Reg.Target_Speed_Last=0;
		PID_Yaw_205_Position_Reg.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_Position_Reg.Acceleration=PTZ_ACCELERATION;

		PID_Yaw_205_Speed_Reg.CurrentError=0;
		PID_Yaw_205_Speed_Reg.LastError=0;
		PID_Yaw_205_Speed_Reg.ErrorIgnored=2;
		PID_Yaw_205_Speed_Reg.Pout=0;
		PID_Yaw_205_Speed_Reg.Iout=0;
		PID_Yaw_205_Speed_Reg.Dout=0;
		PID_Yaw_205_Speed_Reg.PIDout=0;
		PID_Yaw_205_Speed_Reg.PIDOutCompensate=0;
		PID_Yaw_205_Speed_Reg.PMax=30000;
		PID_Yaw_205_Speed_Reg.IMax=30000;
		PID_Yaw_205_Speed_Reg.DMax=30000;
		PID_Yaw_205_Speed_Reg.PIDOutMax=30000;
		PID_Yaw_205_Speed_Reg.PIDOutLast=0;
		PID_Yaw_205_Speed_Reg.Target_Speed_Last=0;
		PID_Yaw_205_Speed_Reg.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_Speed_Reg.Acceleration=PTZ_ACCELERATION;
		

		PID_Yaw_205_MechAngle.CurrentError=0;
		PID_Yaw_205_MechAngle.LastError=0;
		PID_Yaw_205_MechAngle.ErrorIgnored=0;//2;
		PID_Yaw_205_MechAngle.Pout=0;
		PID_Yaw_205_MechAngle.Iout=0;
		PID_Yaw_205_MechAngle.Dout=0;
		PID_Yaw_205_MechAngle.PIDout=0;
		PID_Yaw_205_MechAngle.PIDOutCompensate=0;
		PID_Yaw_205_MechAngle.PMax=30000;
		PID_Yaw_205_MechAngle.IMax=30000;
		PID_Yaw_205_MechAngle.DMax=30000;
		PID_Yaw_205_MechAngle.PIDOutMax=30000;
		PID_Yaw_205_MechAngle.PIDOutLast=0;
		PID_Yaw_205_MechAngle.Target_Speed_Last=0;		
		PID_Yaw_205_MechAngle.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_MechAngle.Acceleration=PTZ_ACCELERATION;
		
		
		PID_Yaw_205_Speed.CurrentError=0;
		PID_Yaw_205_Speed.LastError=0;
		PID_Yaw_205_Speed.ErrorIgnored=0;//2;
		PID_Yaw_205_Speed.Pout=0;
		PID_Yaw_205_Speed.Iout=0;
		PID_Yaw_205_Speed.Dout=0;
		PID_Yaw_205_Speed.PIDout=0;
		PID_Yaw_205_Speed.PIDOutCompensate=0;
		PID_Yaw_205_Speed.PMax=30000;
		PID_Yaw_205_Speed.IMax=30000;
		PID_Yaw_205_Speed.DMax=30000;
		PID_Yaw_205_Speed.PIDOutMax=30000;
		PID_Yaw_205_Speed.PIDOutLast=0;
		PID_Yaw_205_Speed.Target_Speed_Last=0;
		PID_Yaw_205_Speed.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_Speed.Acceleration=PTZ_ACCELERATION;
	
		
		/*0x206*/
		PID_Pitch_206_Position_Reg.CurrentError=0;
		PID_Pitch_206_Position_Reg.LastError=0;
		PID_Pitch_206_Position_Reg.ErrorIgnored=2;
		PID_Pitch_206_Position_Reg.Pout=0;
		PID_Pitch_206_Position_Reg.Iout=0;
		PID_Pitch_206_Position_Reg.Dout=0;
		PID_Pitch_206_Position_Reg.PIDout=0;
		PID_Pitch_206_Position_Reg.PIDOutCompensate=0;
		PID_Pitch_206_Position_Reg.PMax=30000;
		PID_Pitch_206_Position_Reg.IMax=30000;
		PID_Pitch_206_Position_Reg.DMax=30000;
		PID_Pitch_206_Position_Reg.PIDOutMax=30000;
		PID_Pitch_206_Position_Reg.PIDOutLast=0;
		PID_Pitch_206_Position_Reg.Target_Speed_Last=0;
		PID_Pitch_206_Position_Reg.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_Position_Reg.Acceleration=PTZ_ACCELERATION; 

		PID_Pitch_206_Speed_Reg.CurrentError=0;
		PID_Pitch_206_Speed_Reg.LastError=0;
		PID_Pitch_206_Speed_Reg.ErrorIgnored=2;
		PID_Pitch_206_Speed_Reg.Pout=0;
		PID_Pitch_206_Speed_Reg.Iout=0;
		PID_Pitch_206_Speed_Reg.Dout=0;
		PID_Pitch_206_Speed_Reg.PIDout=0;
		PID_Pitch_206_Speed_Reg.PIDOutCompensate=0;
		PID_Pitch_206_Speed_Reg.PMax=30000;
		PID_Pitch_206_Speed_Reg.IMax=30000;
		PID_Pitch_206_Speed_Reg.DMax=30000;
		PID_Pitch_206_Speed_Reg.PIDOutMax=30000;
		PID_Pitch_206_Speed_Reg.PIDOutLast=0;
		PID_Pitch_206_Speed_Reg.Target_Speed_Last=0;
		PID_Pitch_206_Speed_Reg.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_Speed_Reg.Acceleration=PTZ_ACCELERATION;
		
		
		PID_Pitch_206_MechAngle.CurrentError=0;
		PID_Pitch_206_MechAngle.LastError=0;
		PID_Pitch_206_MechAngle.ErrorIgnored=0;//2;
		PID_Pitch_206_MechAngle.Pout=0;
		PID_Pitch_206_MechAngle.Iout=0;
		PID_Pitch_206_MechAngle.Dout=0;
		PID_Pitch_206_MechAngle.PIDout=0;
		PID_Pitch_206_MechAngle.PIDOutCompensate=0;
		PID_Pitch_206_MechAngle.PMax=30000;
		PID_Pitch_206_MechAngle.IMax=30000;
		PID_Pitch_206_MechAngle.DMax=30000;
		PID_Pitch_206_MechAngle.PIDOutMax=30000;
		PID_Pitch_206_MechAngle.PIDOutLast=0;
		PID_Pitch_206_MechAngle.Target_Speed_Last=0;		
		PID_Pitch_206_MechAngle.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_MechAngle.Acceleration=PTZ_ACCELERATION;

		PID_Pitch_206_Speed.CurrentError=0;
		PID_Pitch_206_Speed.LastError=0;
		PID_Pitch_206_Speed.ErrorIgnored=0;//2;
		PID_Pitch_206_Speed.Pout=0;
		PID_Pitch_206_Speed.Iout=0;
		PID_Pitch_206_Speed.Dout=0;
		PID_Pitch_206_Speed.PIDout=0;
		PID_Pitch_206_Speed.PIDOutCompensate=0;
		PID_Pitch_206_Speed.PMax=30000;
		PID_Pitch_206_Speed.IMax=30000;
		PID_Pitch_206_Speed.DMax=30000;
		PID_Pitch_206_Speed.PIDOutMax=30000;
		PID_Pitch_206_Speed.PIDOutLast=0;
		PID_Pitch_206_Speed.Target_Speed_Last=0;	
		PID_Pitch_206_Speed.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_Speed.Acceleration=PTZ_ACCELERATION;
		
	}
	
	{//自瞄
		
		/*0x205*/	
		PID_Yaw_205_NUC_Mech.P=5;//15;
		PID_Yaw_205_NUC_Mech.I=0;//0;
		PID_Yaw_205_NUC_Mech.D=0;//0;

		PID_Yaw_205_NUC_Speed.P=200;//90;
		PID_Yaw_205_NUC_Speed.I=0;//0;
		PID_Yaw_205_NUC_Speed.D=0;//0;
		
		/*0x206*/
		PID_Pitch_206_NUC_Mech.P=1.5;
		PID_Pitch_206_NUC_Mech.I=0;
		PID_Pitch_206_NUC_Mech.D=0;
	 
		PID_Pitch_206_NUC_Speed.P=220;
		PID_Pitch_206_NUC_Speed.I=0;
		PID_Pitch_206_NUC_Speed.D=20;
			

		
		/*0x205*/
		PID_Yaw_205_NUC_Mech.CurrentError=0;
		PID_Yaw_205_NUC_Mech.LastError=0;
		PID_Yaw_205_NUC_Mech.ErrorIgnored=0;
		PID_Yaw_205_NUC_Mech.Pout=0;
		PID_Yaw_205_NUC_Mech.Iout=0;
		PID_Yaw_205_NUC_Mech.Dout=0;
		PID_Yaw_205_NUC_Mech.PIDout=0;
		PID_Yaw_205_NUC_Mech.PIDOutCompensate=0;
		PID_Yaw_205_NUC_Mech.PMax=30000;
		PID_Yaw_205_NUC_Mech.IMax=30000;
		PID_Yaw_205_NUC_Mech.DMax=30000;
		PID_Yaw_205_NUC_Mech.PIDOutMax=30000;
		PID_Yaw_205_NUC_Mech.PIDOutLast=0;
		PID_Yaw_205_NUC_Mech.Target_Speed_Last=0;		
		PID_Yaw_205_NUC_Mech.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_NUC_Mech.Acceleration=PTZ_ACCELERATION;
		
		PID_Yaw_205_NUC_Speed.CurrentError=0;
		PID_Yaw_205_NUC_Speed.LastError=0;
		PID_Yaw_205_NUC_Speed.ErrorIgnored=0;
		PID_Yaw_205_NUC_Speed.Pout=0;
		PID_Yaw_205_NUC_Speed.Iout=0;
		PID_Yaw_205_NUC_Speed.Dout=0;
		PID_Yaw_205_NUC_Speed.PIDout=0;
		PID_Yaw_205_NUC_Speed.PIDOutCompensate=0;
		PID_Yaw_205_NUC_Speed.PMax=30000;
		PID_Yaw_205_NUC_Speed.IMax=30000;
		PID_Yaw_205_NUC_Speed.DMax=30000;
		PID_Yaw_205_NUC_Speed.PIDOutMax=30000;
		PID_Yaw_205_NUC_Speed.PIDOutLast=0;
		PID_Yaw_205_NUC_Speed.Target_Speed_Last=0;		
		PID_Yaw_205_NUC_Speed.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Yaw_205_NUC_Speed.Acceleration=PTZ_ACCELERATION;
		
		
		/*0x206*/
		PID_Pitch_206_NUC_Mech.CurrentError=0;
		PID_Pitch_206_NUC_Mech.LastError=0;
		PID_Pitch_206_NUC_Mech.ErrorIgnored=0;
		PID_Pitch_206_NUC_Mech.Pout=0;
		PID_Pitch_206_NUC_Mech.Iout=0;
		PID_Pitch_206_NUC_Mech.Dout=0;
		PID_Pitch_206_NUC_Mech.PIDout=0;
		PID_Pitch_206_NUC_Mech.PIDOutCompensate=0;
		PID_Pitch_206_NUC_Mech.PMax=30000;
		PID_Pitch_206_NUC_Mech.IMax=30000;
		PID_Pitch_206_NUC_Mech.DMax=30000;
		PID_Pitch_206_NUC_Mech.PIDOutMax=30000;
		PID_Pitch_206_NUC_Mech.PIDOutLast=0;
		PID_Pitch_206_NUC_Mech.Target_Speed_Last=0;		
		PID_Pitch_206_NUC_Mech.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_NUC_Mech.Acceleration=PTZ_ACCELERATION;

		
		PID_Pitch_206_NUC_Speed.CurrentError=0;
		PID_Pitch_206_NUC_Speed.LastError=0;
		PID_Pitch_206_NUC_Speed.ErrorIgnored=0;
		PID_Pitch_206_NUC_Speed.Pout=0;
		PID_Pitch_206_NUC_Speed.Iout=0;
		PID_Pitch_206_NUC_Speed.Dout=0;
		PID_Pitch_206_NUC_Speed.PIDout=0;
		PID_Pitch_206_NUC_Speed.PIDOutCompensate=0;
		PID_Pitch_206_NUC_Speed.PMax=30000;
		PID_Pitch_206_NUC_Speed.IMax=30000;
		PID_Pitch_206_NUC_Speed.DMax=30000;
		PID_Pitch_206_NUC_Speed.PIDOutMax=30000;
		PID_Pitch_206_NUC_Speed.PIDOutLast=0;
		PID_Pitch_206_NUC_Speed.Target_Speed_Last=0;		
		PID_Pitch_206_NUC_Speed.Speed_Ratio=PTZ_SPEED_RATIO;
		PID_Pitch_206_NUC_Speed.Acceleration=PTZ_ACCELERATION;
		
	} 
	
	
	{//拨弹盘电机
		
		{/*0x207*/
//		PID_Poke_207_MechAngle.P=11;//9;//5;//1;
//		PID_Poke_207_MechAngle.I=0;//0;//0;//0;
//		PID_Poke_207_MechAngle.D=280;//240;//0;//20;
//		PID_Poke_207_MechAngle.CurrentError=0;
//		PID_Poke_207_MechAngle.LastError=0;
//		PID_Poke_207_MechAngle.ErrorIgnored=2;
//		PID_Poke_207_MechAngle.Pout=0;
//		PID_Poke_207_MechAngle.Iout=0;
//		PID_Poke_207_MechAngle.Dout=0;
//		PID_Poke_207_MechAngle.PIDout=0;
//		PID_Poke_207_MechAngle.PIDOutCompensate=0;
//		PID_Poke_207_MechAngle.PMax=30000;
//		PID_Poke_207_MechAngle.IMax=30000;
//		PID_Poke_207_MechAngle.DMax=30000;
//		PID_Poke_207_MechAngle.PIDOutMax=30000;
//		PID_Poke_207_MechAngle.PIDOutLast=0;
//		PID_Poke_207_MechAngle.Target_Speed_Last=0;	
//		PID_Poke_207_MechAngle.Speed_Ratio=1;
//		PID_Poke_207_MechAngle.Acceleration=1;
		}
		
		PID_Poke_207_Speed.P=10;//5;//20;//20;
		PID_Poke_207_Speed.I=0;//0;//0;//0;
		PID_Poke_207_Speed.D=1;//0;//0;//1;
		PID_Poke_207_Speed.CurrentError=0;
		PID_Poke_207_Speed.LastError=0;
		PID_Poke_207_Speed.ErrorIgnored=2;
		PID_Poke_207_Speed.Pout=0;
		PID_Poke_207_Speed.Iout=0;
		PID_Poke_207_Speed.Dout=0;
		PID_Poke_207_Speed.PIDout=0;
		PID_Poke_207_Speed.PIDOutCompensate=0;
		PID_Poke_207_Speed.PMax=13000;
		PID_Poke_207_Speed.IMax=4000;
		PID_Poke_207_Speed.DMax=5000;
		PID_Poke_207_Speed.PIDOutMax=16000;
		PID_Poke_207_Speed.PIDOutLast=0;
		PID_Poke_207_Speed.Target_Speed_Last=0;
		PID_Poke_207_Speed.Speed_Ratio=1;
		PID_Poke_207_Speed.Acceleration=3000;

	}
 
	{//摩擦轮电机
		
		//10 0 1
		
		/*0x201*/
		PID_Shoot_201_Left.P=10;
		PID_Shoot_201_Left.I=0;
		PID_Shoot_201_Left.D=1;
		PID_Shoot_201_Left.CurrentError=0;
		PID_Shoot_201_Left.LastError=0;
		PID_Shoot_201_Left.ErrorIgnored=0;
		PID_Shoot_201_Left.Pout=0;
		PID_Shoot_201_Left.Iout=0;
		PID_Shoot_201_Left.Dout=0;
		PID_Shoot_201_Left.PIDout=0;
		PID_Shoot_201_Left.PIDOutCompensate=0;
		PID_Shoot_201_Left.PMax=10000;
		PID_Shoot_201_Left.IMax=4000;
		PID_Shoot_201_Left.DMax=3000;
		PID_Shoot_201_Left.PIDOutMax=12000;
		PID_Shoot_201_Left.PIDOutLast=0;
		PID_Shoot_201_Left.Target_Speed_Last=0;
		PID_Shoot_201_Left.Speed_Ratio=1;
		PID_Shoot_201_Left.Acceleration=200;//CHASSIS_ACCELARATION;
		//5050
		//8 0 1 9000 4000 5000 16000
		
		
		
		//5000
		//8 0 0.8 9000 4000 3000 12000
		
		/*2021526*/
		//5000
		//10 0 0.8 9000 4000 3000 12000
		
		
		/*0x202*/
		PID_Shoot_202_Right.P=10;	
		PID_Shoot_202_Right.I=0;
		PID_Shoot_202_Right.D=1;
		PID_Shoot_202_Right.CurrentError=0;
		PID_Shoot_202_Right.LastError=0;
		PID_Shoot_202_Right.ErrorIgnored=0;
		PID_Shoot_202_Right.Pout=0;
		PID_Shoot_202_Right.Iout=0;
		PID_Shoot_202_Right.Dout=0;
		PID_Shoot_202_Right.PIDout=0;
		PID_Shoot_202_Right.PIDOutCompensate=0;
		PID_Shoot_202_Right.PMax=10000;
		PID_Shoot_202_Right.IMax=4000;
		PID_Shoot_202_Right.DMax=3000;
		PID_Shoot_202_Right.PIDOutMax=12000;
		PID_Shoot_202_Right.PIDOutLast=0;
		PID_Shoot_202_Right.Target_Speed_Last=0;
		PID_Shoot_202_Right.Speed_Ratio=1;
		PID_Shoot_202_Right.Acceleration=200;//CHASSIS_ACCELARATION;
		
		// 10m射速稳定
{
//				/*0x201*/
//		PID_Shoot_201_Left.P=20;
//		PID_Shoot_201_Left.I=0;
//		PID_Shoot_201_Left.D=0;
//		PID_Shoot_201_Left.CurrentError=0;
//		PID_Shoot_201_Left.LastError=0;
//		PID_Shoot_201_Left.ErrorIgnored=2;
//		PID_Shoot_201_Left.Pout=0;
//		PID_Shoot_201_Left.Iout=0;
//		PID_Shoot_201_Left.Dout=0;
//		PID_Shoot_201_Left.PIDout=0;
//		PID_Shoot_201_Left.PIDOutCompensate=0;
//		PID_Shoot_201_Left.PMax=12000;
//		PID_Shoot_201_Left.IMax=4000;
//		PID_Shoot_201_Left.DMax=5000;
//		PID_Shoot_201_Left.PIDOutMax=12000;
//		PID_Shoot_201_Left.PIDOutLast=0;
//		PID_Shoot_201_Left.Target_Speed_Last=0;
//		PID_Shoot_201_Left.Speed_Ratio=1;
//		PID_Shoot_201_Left.Acceleration=200;//CHASSIS_ACCELARATION;
//		
//		/*0x202*/
//		PID_Shoot_202_Right.P=20;
//		PID_Shoot_202_Right.I=0;
//		PID_Shoot_202_Right.D=0;
//		PID_Shoot_202_Right.CurrentError=0;
//		PID_Shoot_202_Right.LastError=0;
//		PID_Shoot_202_Right.ErrorIgnored=2;
//		PID_Shoot_202_Right.Pout=0;
//		PID_Shoot_202_Right.Iout=0;
//		PID_Shoot_202_Right.Dout=0;
//		PID_Shoot_202_Right.PIDout=0;
//		PID_Shoot_202_Right.PIDOutCompensate=0;
//		PID_Shoot_202_Right.PMax=12000;
//		PID_Shoot_202_Right.IMax=4000;
//		PID_Shoot_202_Right.DMax=5000;
//		PID_Shoot_202_Right.PIDOutMax=12000;
//		PID_Shoot_202_Right.PIDOutLast=0;
//		PID_Shoot_202_Right.Target_Speed_Last=0;
//		PID_Shoot_202_Right.Speed_Ratio=1;
//		PID_Shoot_202_Right.Acceleration=200;//CHASSIS_ACCELARATION;
}
}	

}



/**
  * @brief  PID计算
  * @param  void
  * @retval void
  * @notes  void  
*/
float Pid_Calc(PID_TypeDef *PID, float Current_Speed, float Target_Speed)
{   	
	Current_Speed = Current_Speed / PID->Speed_Ratio;

	PID->Target_Speed_Last = Target_Speed;

	PID->CurrentError = Target_Speed - Current_Speed;
	if (fabs(PID->CurrentError) < PID->ErrorIgnored )  PID->CurrentError = 0;
	
	//P  
	PID->Pout = PID->P * PID->CurrentError;
	if(PID->Pout >  PID->PMax)  PID->Pout =  PID->PMax;
	if(PID->Pout < -PID->PMax)  PID->Pout = -PID->PMax;
	
	//I
	PID->Iout += PID->I * PID->CurrentError;
	if(PID->Iout >  PID->IMax) PID->Iout =  PID->IMax;
	if(PID->Iout < -PID->IMax) PID->Iout = -PID->IMax;
	
	//D 
	PID->Dout = PID->D * ( PID->CurrentError - PID->LastError);
	if(PID->Dout >  PID->DMax) PID->Dout =  PID->DMax;
	if(PID->Dout < -PID->DMax) PID->Dout = -PID->DMax;
	
	//PID
	PID->PIDout = PID->Pout + PID->Iout + PID->Dout + PID->PIDOutCompensate;

	if(PID->Motor_Type == 2)
	{
		if ( (fabs( PID->PIDout - PID->PIDOutLast) > PID->Acceleration)  && (fabs(Current_Speed) !=0) )    
		{
			PID->PIDout =( PID->PIDout > PID->PIDOutLast) ?  (PID->PIDOutLast + PID->Acceleration) : (PID->PIDOutLast - PID->Acceleration);
		}
	}	
	
	if(PID->PIDout >  PID->PIDOutMax) PID->PIDout =  PID->PIDOutMax;
	if(PID->PIDout < -PID->PIDOutMax) PID->PIDout = -PID->PIDOutMax;

  PID->PIDOutLast = PID->PIDout;
	PID->LastError  = PID->CurrentError;
	return PID->PIDout;    
}


