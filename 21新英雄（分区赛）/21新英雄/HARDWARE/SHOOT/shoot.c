#include "shoot.h"

/***************************************数据定义区***************************************/
u8 Shoot_Sta=0;//发射状态，1为发射，0为不发射
u8 Switch_Left_Last;//上一次左拨杆的状态
short Shoot_Speed;//发射速度
int Switch_Left_Flag=0;


short Shoot_heat_Aban = 0;
short heat_Aban_count1 = 0, heat_Aban_count0 = 0;
short Risk_Heat;
float Risk_Heat_Rate;

short Poke_Turn=0; //0-没有卡弹，1-卡弹

short SHOOT_OPEN_SPEED;
/***************************************函数处理区***************************************/
/**
  * @brief  发射控制函数
  * @param  void
  * @retval void  
  * @notes  void
  */ 
void Shoot_Control(void)
{
	
	if(game_robot_state.shooter_id1_42mm_speed_limit == 10)
	{
		SHOOT_OPEN_SPEED = BAOFA_SPEED;
	}
	else if(game_robot_state.shooter_id1_42mm_speed_limit == 16)
	{
		SHOOT_OPEN_SPEED = SHESU_SPEED;
	}
		
	
	if(Switch_Left_Flag == 0)
	{
		Switch_Left_Last = DBUS.RC.Switch_Left;
	}
	Switch_Left_Flag = 1;
	

	
	////遥控器模式
  if (DBUS.RC.Switch_Right != RC_SW_DOWN)
	{
		//左拨杆状态更改
		if(DBUS.RC.Switch_Left != Switch_Left_Last)
		{
			//左拨杆为上
			if(DBUS.RC.Switch_Left == RC_SW_UP)
			{
					
				if(Shoot_Speed == SHOOT_OPEN_SPEED)
				{
					Shoot_Speed = SHOOT_STOP_SPEED;
					Shoot_Sta = 0;

				}
				
				else if (Shoot_Speed == SHOOT_STOP_SPEED)
		    {
					Shoot_Speed = SHOOT_OPEN_SPEED;
					Shoot_Sta = 1;
			  }
				
			}
			
		}
		
	 Switch_Left_Last = DBUS.RC.Switch_Left;

	}
	


   ////键鼠模式
//	if (DBUS.RC.Switch_Right == RC_SW_DOWN) 
  {
		 
		 if(DBUS.PC.Keyboard & KEY_X)
			{
				Shoot_Speed = SHOOT_OPEN_SPEED;
				Shoot_Sta = 1;
			}
		
		 if(DBUS.PC.Keyboard & KEY_Z)
			{
				Shoot_Speed = SHOOT_STOP_SPEED;
				Shoot_Sta = 0;
			}

			
	}
	

	//激光开关
	 if(ptz_init_sec == 1)
	 {
		 LASER_ON;
	 }


  ////热量控制	 
	if(JUDGE_STA == 1)
	{ 
		switch (game_robot_state.robot_level)
		{
			case 1:
			{
				Risk_Heat = RISK_HEAT_1;
				Risk_Heat_Rate = RISK_HEAT_RATE_1;
			}break;
			
			case 2:
			{
				Risk_Heat = RISK_HEAT_2;
				Risk_Heat_Rate = RISK_HEAT_RATE_2;
			}break;
			
			case 3:
			{
				Risk_Heat = RISK_HEAT_3;
				Risk_Heat_Rate = RISK_HEAT_RATE_3;
			}break;
			
			default:
			{
			}	
		}
		
		if((power_heat_data.shooter_id1_42mm_cooling_heat<=game_robot_state.shooter_id1_42mm_cooling_limit)&&(game_robot_state.shooter_id1_42mm_cooling_limit-power_heat_data.shooter_id1_42mm_cooling_heat+game_robot_state.shooter_id1_42mm_cooling_rate*Risk_Heat_Rate)>100)	
		{
			heat_Aban_count1 = 0;
			if(heat_Aban_count0 <= 100) heat_Aban_count0++;
			else  Shoot_heat_Aban = 0;
		}
		
		else
		{
			heat_Aban_count0 = 0;
			Shoot_heat_Aban = 1;
		}
		
		if(DBUS.PC.Keyboard & KEY_CTRL)//ctrl键强开发射
		{
			Shoot_heat_Aban=0;
		}
		
	}
	

		////摩擦轮和摩擦轮拨盘反转标志
    if(DBUS.PC.Keyboard & KEY_R)//按住R键，
	 {
			 Poke_Turn = 1;
	 }	
	 
   else
	 {
			 Poke_Turn = 0;		 
	 }    


}



