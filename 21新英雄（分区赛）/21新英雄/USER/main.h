#ifndef _MAIN_H
#define _MAIN_H

#include "stm32f4xx.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


#define START_TASK_PRIO		1
#define START_STK_SIZE		256

#define LED_TASK_PRIO		2
#define LED_STK_SIZE		128

#define GRAPHIC_TASK_PRIO		3
#define GRAPHIC_STK_SIZE		512

#define RC_TASK_PRIO		4
#define RC_STK_SIZE		512

#define SUPER_TASK_PRIO		5
#define SUPER_STK_SIZE		512

#define SWMODE_TASK_PRIO		6
#define SWMODE_STK_SIZE		512

#define SHOOT_TASK_PRIO		7
#define SHOOT_STK_SIZE		512

#define CAN_SEND_TASK_PRIO		17
#define CAN_SEND_STK_SIZE		512

#define CHASSIS_TASK_PRIO		18
#define CHASSIS_STK_SIZE		512

#define GIMBAL_TASK_PRIO		19
#define GIMBAL_STK_SIZE		512

#define IMU_TASK_PRIO		20
#define IMU_STK_SIZE		512








#define GIMBAL_SIGNAL   ( 1 << 0 )
#define CHASSIS_SIGNAL ( 1 << 1 )
#define ARMORY_SIGNAL     ( 1 << 2 )
#define SHOOT_SIGNAL     ( 1 << 3 )
#define SUPER_SIGNAL     ( 1 << 4 )

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#include "led_task.h"
#include "chassis_task.h"
#include "rc_task.h"
#include "gimbal_task.h"
#include "swmode_task.h"
#include "shoot_task.h"
#include "can_send_task.h"
#include "graphic_task.h"
#include "imu_task.h"
#include "super_task.h"




#include "sys.h"
#include "delay.h"
#include "led.h"
#include "can.h"
#include "pid.h"
#include "usart.h"
#include "DR16.h"
#include "ptz.h"
#include "shoot.h"
#include "Limit.h"
#include "general.h"
#include "Laser.h"
#include "Armory.h"
#include "tim.h"
#include "imu.h"
#include "nuc.h"
#include "beep.h"
#include "Referee_System.h"
#include "crc.h"
#include "iwdg.h"
#include "custom_ui.h"



//电机电流值
//3508 -16384——16384
//6020 -30000——30000
//2006 -10000——10000
//摩擦轮速度
#define  BAOFA_SPEED                      4400//射速大概在9左右
#define  SHESU_SPEED                      5200//6000//射速大概在13.4~15.8之间
//#define  SHOOT_OPEN_SPEED                      5200//高尔夫射速大概在14
#define  SHOOT_STOP_SPEED                      0


#define  PRESS_TIM_COUNT                20             //按键检测间隔时间


#define CHASSIS_SPEED_RATIO 									 19
#define CHASSIS_ACCELARATION 									 5000//2000


#define PTZ_SPEED_RATIO                        1
#define PTZ_ACCELERATION                     30000

#define SHOOT_SPEED_RATIO
#define SHOOT_ACCELERATION		

#define FRIC_SPEED_RATIO 									 1
#define FRIC_ACCELARATION 									20000

#define FLIP_CLOSE_ANGLE 			1000
#define FLIP_OPEN_ANGLE 			1000


#define CHASSIS_ACC_HIGH                       15
#define CHASSIS_ACC_MID                        3
#define CHASSIS_ACC_LOW                        2


#define CHASSIS_SPEED_HIGH                     450
#define CHASSIS_SPEED_MID                      350
#define CHASSIS_SPEED_LOW                      230	


#define JUDGE_STA                              1         //0-不接入裁判系统 ， 1-接入裁判系统   
#define DR_DECENT_TIM                          50         //遥控器信号衰减时间

#define RISK_HEAT_1                            200
#define RISK_HEAT_2                            350
#define RISK_HEAT_3                            500

#define RISK_HEAT_RATE_1                       0.05
#define RISK_HEAT_RATE_2                       0.1
#define RISK_HEAT_RATE_3                       0.15



#define FOLLOW_STA       0
//#define GET_GOFE_STA     1 
#define SWING_STA        2
#define ROTATE_STA       3
//#define AOTU_STA_GOLF    3
//#define GOFE_IS_FINE     4
#define NORMAL           5
#define SUPPER           6
#define CAP_OPEN         7
#define CAP_CLOSE        8
#define CAP_MOVE         9

#define SWING_ANGLE_VAL  300


//屏幕宽长1920x1080
#define SCREEN_WIDTH 1080
#define SCREEN_LENGTH 1920
//自定义图形绘制需要


#endif


