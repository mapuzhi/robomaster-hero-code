#include "main.h"
TaskHandle_t StartTask_Handler;
void start_task(void *pvParameters);
void start_task_create();

EventGroupHandle_t EventGroupHandler;


void BSP_Init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);//中断配置分组
	
	delay_init(168);                               //延时初始化

	TIM6_Init(1000);															 //初始化基础定时器

	Beep_InitConfig();														 //初始化蜂鸣器
	
	BEEP_ON;
	
	USART6_InitConfig();													 //串口6打印初始化	115200
	
	LED_InitConfig();															 //初始化LED
	
	DR16_InitConfig();														 //初始化遥控器
	
	CAN1_InitConfig();														 //初始化CAN1
	
	CAN2_InitConfig();														 //初始化CAN2
	
	PID_Init();																		 //初始化PID
	
	IMU_Init();																		 //初始化IMU	
		
	JudgeSys_Init();															 //初始化裁判系统

	NUC_Init();																		 //初始化NUC
	
	Laser_InitConfig();														 //初始化激光
	
	Limit_Init();																	 //初始化限位开关

	IWDG_Init(4 , 625);														 //初始化看门狗复位

}
//创建任务并且开启调度
void start_task_create()
{
		xTaskCreate((TaskFunction_t )start_task,            //任务函数
							(const char*    )"start_task",          //任务名称
							(uint16_t       )START_STK_SIZE,        //任务堆栈大小
							(void*          )NULL,                  //传递给任务函数的参数
							(UBaseType_t    )START_TASK_PRIO,       //任务优先级
							(TaskHandle_t*  )&StartTask_Handler);   //任务句柄              
		
	  vTaskStartScheduler();          //开启任务调度			
}

void start_task(void *pvParameters)
{
	
	EventGroupHandler = xEventGroupCreate();	//创建事件组

	while(1)
	{
	
	taskENTER_CRITICAL();	//进入临界区

	led_task_create();		//创建LED任务
		
	shoot_task_create(); //创建发射控制任务
		
	chassis_task_create();	//创建底盘任务
	
	imu_task_create();	//创建陀螺仪任务
	
	gimbal_task_create();	//创建云台控制任务
	
	rc_task_create();	//创建遥控器数据处理任务

	swmode_task_create(); //创建模式选择任务
	
	super_task_create();	//创建超级电容任务
		
	can_send_task_create();	//创建CAN发送任务

	graphic_task_create();	//创建图像界面任务
	
	BEEP_OFF;
		
	vTaskDelete(StartTask_Handler);	//删除开始任务

	taskEXIT_CRITICAL();	//退出临界区
	
	}
	
}

int main(void)
{
	BSP_Init();																	//初始化全部设备
	
	start_task_create();													//创建开始任务
	
	while(1);																			//任务运行中
}


